botpress = typeof botpress === "object" ? botpress : {}; botpress["botpress-platform-webchat"] =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/lite-modules/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _index = __webpack_require__(1);
	
	var _index2 = _interopRequireDefault(_index);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var FullscreenChat = function (_React$Component) {
	  _inherits(FullscreenChat, _React$Component);
	
	  function FullscreenChat(props) {
	    _classCallCheck(this, FullscreenChat);
	
	    return _possibleConstructorReturn(this, (FullscreenChat.__proto__ || Object.getPrototypeOf(FullscreenChat)).call(this, props));
	  }
	
	  _createClass(FullscreenChat, [{
	    key: 'render',
	    value: function render() {
	      return React.createElement(_index2.default, _extends({ fullscreen: false }, this.props));
	    }
	  }]);
	
	  return FullscreenChat;
	}(React.Component);
	
	exports.default = FullscreenChat;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _add_milliseconds = __webpack_require__(4);
	
	var _add_milliseconds2 = _interopRequireDefault(_add_milliseconds);
	
	var _is_before = __webpack_require__(7);
	
	var _is_before2 = _interopRequireDefault(_is_before);
	
	var _convo = __webpack_require__(8);
	
	var _convo2 = _interopRequireDefault(_convo);
	
	var _side = __webpack_require__(23);
	
	var _side2 = _interopRequireDefault(_side);
	
	var _style = __webpack_require__(19);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	/* global: window */
	
	// import { Emoji } from 'emoji-mart'
	
	if (!window.location.origin) {
	  window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
	}
	
	var BOT_HOSTNAME = window.location.origin;
	var ANIM_DURATION = 300;
	
	var MIN_TIME_BETWEEN_SOUNDS = 10000; // 10 seconds
	
	var Web = function (_React$Component) {
	  _inherits(Web, _React$Component);
	
	  function Web(props) {
	    _classCallCheck(this, Web);
	
	    var _this = _possibleConstructorReturn(this, (Web.__proto__ || Object.getPrototypeOf(Web)).call(this, props));
	
	    var options = window.botpressChatOptions || {};
	
	    _this.state = {
	      view: null,
	      textToSend: '',
	      loading: true,
	      played: false,
	      opened: false,
	      conversations: null,
	      currentConversation: null,
	      currentConversationId: null,
	      unreadCount: 0,
	      isButtonHidden: options.hideWidget
	    };
	
	    _this.handleIframeApi = _this.handleIframeApi.bind(_this);
	    return _this;
	  }
	
	  _createClass(Web, [{
	    key: 'componentWillMount',
	    value: function componentWillMount() {
	      this.setupSocket();
	    }
	  }, {
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      var _this2 = this;
	
	      this.setUserId().then(this.fetchData.bind(this)).then(function () {
	        _this2.handleSwitchView('widget');
	        if (!_this2.state.isButtonHidden) _this2.showConvoPopUp();
	
	        _this2.setState({ loading: false });
	      });
	
	      window.addEventListener('message', this.handleIframeApi);
	
	      this.props.bp.axios.interceptors.request.use(function (config) {
	        if (/\/api\/botpress-platform-webchat\//i.test(config.url)) {
	          var prefix = config.url.indexOf('?') > 0 ? '&' : '?';
	          config.url += prefix + '__ts=' + new Date().getTime();
	        }
	        return config;
	      }, function (error) {
	        return Promise.reject(error);
	      });
	    }
	  }, {
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      window.removeEventListener('message', this.handleIframeApi);
	    }
	  }, {
	    key: 'handleIframeApi',
	    value: function handleIframeApi(_ref) {
	      var data = _ref.data;
	
	      if (data === 'show') {
	        this.handleSwitchView('side');
	      } else if (data === 'hide') {
	        this.handleSwitchView('widget');
	      }
	    }
	  }, {
	    key: 'setUserId',
	    value: function setUserId() {
	      var _this3 = this;
	
	      return new Promise(function (resolve, reject) {
	
	        var interval = setInterval(function () {
	          if (window.__BP_VISITOR_ID) {
	            clearInterval(interval);
	            _this3.userId = window.__BP_VISITOR_ID;
	            resolve();
	          }
	        }, 250);
	
	        setTimeout(function () {
	          clearInterval(interval);
	          reject();
	        }, 300000);
	      });
	    }
	  }, {
	    key: 'showConvoPopUp',
	    value: function showConvoPopUp() {
	      var _this4 = this;
	
	      if (this.state.config.welcomeMsgEnable) {
	        setTimeout(function () {
	          if (!_this4.state.opened) {
	            _this4.handleSwitchView('convo');
	          }
	        }, this.state.config.welcomeMsgDelay || 5000);
	      }
	    }
	  }, {
	    key: 'handleSwitchView',
	    value: function handleSwitchView(view) {
	      var _this5 = this;
	
	      if (view === 'side' && this.state.view !== 'side') {
	        this.setState({
	          opened: true,
	          unreadCount: 0,
	          convoTransition: 'fadeOut',
	          widgetTransition: 'fadeOut'
	        });
	
	        setTimeout(function () {
	          _this5.setState({
	            sideTransition: 'fadeIn',
	            view: view
	          });
	        }, ANIM_DURATION + 10);
	      }
	
	      if (view === 'convo') {
	        setTimeout(function () {
	          _this5.setState({
	            convoTransition: 'fadeIn',
	            view: view
	          });
	        }, ANIM_DURATION);
	      }
	
	      if (view === 'widget') {
	        this.setState({
	          convoTransition: 'fadeOut',
	          sideTransition: 'fadeOut'
	        });
	
	        if (!this.state.view || this.state.view === 'side') {
	          setTimeout(function () {
	            _this5.setState({
	              widgetTransition: 'fadeIn',
	              view: view
	            });
	          }, ANIM_DURATION);
	        }
	      }
	
	      setTimeout(function () {
	        _this5.setState({
	          view: view
	        });
	      }, ANIM_DURATION);
	
	      setTimeout(function () {
	        _this5.setState({
	          widgetTransition: null,
	          convoTransition: null,
	          sideTransition: _this5.state.sideTransition === 'fadeIn' ? 'fadeIn' : null
	        });
	      }, ANIM_DURATION * 2.1);
	    }
	  }, {
	    key: 'handleButtonClicked',
	    value: function handleButtonClicked() {
	      if (this.state.view === 'convo') {
	        this.handleSwitchView('widget');
	      } else {
	        this.handleSwitchView('side');
	      }
	    }
	  }, {
	    key: 'setupSocket',
	    value: function setupSocket() {
	      // Connect the Botpress's Web Socket to the server
	      if (this.props.bp && this.props.bp.events) {
	        this.props.bp.events.setup();
	      }
	
	      this.props.bp.events.on('guest.webchat.message', this.handleNewMessage.bind(this));
	      this.props.bp.events.on('guest.webchat.typing', this.handleBotTyping.bind(this));
	    }
	  }, {
	    key: 'fetchData',
	    value: function fetchData() {
	      return this.fetchConfig().then(this.fetchConversations.bind(this)).then(this.fetchCurrentConversation.bind(this));
	    }
	  }, {
	    key: 'fetchConversations',
	    value: function fetchConversations() {
	      var _this6 = this;
	
	      var axios = this.props.bp.axios;
	      var userId = this.userId;
	      var url = BOT_HOSTNAME + '/api/botpress-platform-webchat/conversations/' + userId;
	
	      return axios.get(url).then(function (_ref2) {
	        var data = _ref2.data;
	
	        _this6.setState({
	          conversations: data
	        });
	      });
	    }
	  }, {
	    key: 'fetchCurrentConversation',
	    value: function (_fetchCurrentConversation) {
	      function fetchCurrentConversation(_x) {
	        return _fetchCurrentConversation.apply(this, arguments);
	      }
	
	      fetchCurrentConversation.toString = function () {
	        return _fetchCurrentConversation.toString();
	      };
	
	      return fetchCurrentConversation;
	    }(function (convoId) {
	      var _this7 = this;
	
	      var axios = this.props.bp.axios;
	      var userId = this.userId;
	
	      var conversationIdToFetch = convoId || this.state.currentConversationId;
	      if (this.state.conversations.length > 0 && !conversationIdToFetch) {
	        conversationIdToFetch = this.state.conversations[0].id;
	        this.setState({ currentConversationId: conversationIdToFetch });
	      }
	
	      var url = BOT_HOSTNAME + '/api/botpress-platform-webchat/conversations/' + userId + '/' + conversationIdToFetch;
	
	      return axios.get(url).then(function (_ref3) {
	        var data = _ref3.data;
	
	        // Possible race condition if the current conversation changed while fetching
	        if (_this7.state.currentConversationId !== conversationIdToFetch) {
	          // In which case we simply restart fetching
	          return fetchCurrentConversation();
	        }
	
	        _this7.setState({ currentConversation: data });
	      });
	    })
	  }, {
	    key: 'fetchConfig',
	    value: function fetchConfig() {
	      var _this8 = this;
	
	      return this.props.bp.axios.get('/api/botpress-platform-webchat/config').then(function (_ref4) {
	        var data = _ref4.data;
	
	        _this8.setState({
	          config: data
	        });
	      });
	    }
	  }, {
	    key: 'handleNewMessage',
	    value: function handleNewMessage(event) {
	      this.safeUpdateCurrentConvo(event.conversationId, true, function (convo) {
	        return Object.assign({}, convo, {
	          messages: [].concat(_toConsumableArray(convo.messages), [event]),
	          typingUntil: event.userId ? convo.typingUntil : null
	        });
	      });
	    }
	  }, {
	    key: 'handleBotTyping',
	    value: function handleBotTyping(event) {
	      this.safeUpdateCurrentConvo(event.conversationId, false, function (convo) {
	        return Object.assign({}, convo, {
	          typingUntil: (0, _add_milliseconds2.default)(new Date(), event.timeInMs)
	        });
	      });
	
	      setTimeout(this.expireTyping.bind(this), event.timeInMs + 50);
	    }
	  }, {
	    key: 'expireTyping',
	    value: function expireTyping() {
	      var currentTypingUntil = this.state.currentConversation && this.state.currentConversation.typingUntil;
	
	      var timerExpired = currentTypingUntil && (0, _is_before2.default)(new Date(currentTypingUntil), new Date());
	      if (timerExpired) {
	        this.safeUpdateCurrentConvo(this.state.currentConversationId, false, function (convo) {
	          return Object.assign({}, convo, { typingUntil: null });
	        });
	      }
	    }
	  }, {
	    key: 'safeUpdateCurrentConvo',
	    value: function safeUpdateCurrentConvo(convoId, addToUnread, updater) {
	      // there's no conversation to update or our convo changed
	      if (!this.state.currentConversation || this.state.currentConversationId !== convoId) {
	
	        this.fetchConversations().then(this.fetchCurrentConversation.bind(this));
	
	        return;
	      }
	
	      // there's no focus on the actual conversation
	      if (document.hasFocus && !document.hasFocus() || this.state.view !== 'side') {
	        this.playSound();
	
	        if (addToUnread) {
	          this.increaseUnreadCount();
	        }
	      }
	
	      this.handleResetUnreadCount();
	
	      var newConvo = updater && updater(this.state.currentConversation);
	
	      if (newConvo) {
	        this.setState({ currentConversation: newConvo });
	      }
	    }
	  }, {
	    key: 'playSound',
	    value: function playSound() {
	      var _this9 = this;
	
	      if (!this.state.played && this.state.view !== 'convo') {
	        // TODO: Remove this condition (view !== 'convo') and fix transition sounds
	        var audio = new Audio('/api/botpress-platform-webchat/static/notification.mp3');
	        audio.play();
	
	        this.setState({
	          played: true
	        });
	
	        setTimeout(function () {
	          _this9.setState({
	            played: false
	          });
	        }, MIN_TIME_BETWEEN_SOUNDS);
	      }
	    }
	  }, {
	    key: 'increaseUnreadCount',
	    value: function increaseUnreadCount() {
	      this.setState({
	        unreadCount: this.state.unreadCount + 1
	      });
	    }
	  }, {
	    key: 'handleResetUnreadCount',
	    value: function handleResetUnreadCount() {
	      if (document.hasFocus && document.hasFocus() && this.state.view === 'side') {
	        this.setState({
	          unreadCount: 0
	        });
	      }
	    }
	  }, {
	    key: 'handleSendMessage',
	    value: function handleSendMessage() {
	      var _this10 = this;
	
	      var userId = window.__BP_VISITOR_ID;
	      var url = BOT_HOSTNAME + '/api/botpress-platform-webchat/messages/' + userId;
	      var config = { params: { conversationId: this.state.currentConversationId } };
	
	      return this.handleSendData({ type: 'text', text: this.state.textToSend }).then(function () {
	        _this10.handleSwitchView('side');
	        _this10.setState({ textToSend: '' });
	      });
	    }
	  }, {
	    key: 'handleTextChanged',
	    value: function handleTextChanged(event) {
	      this.setState({
	        textToSend: event.target.value
	      });
	    }
	  }, {
	    key: 'handleAddEmoji',
	    value: function handleAddEmoji(emoji, event) {
	      this.setState({
	        textToSend: this.state.textToSend + emoji.native + ' '
	      });
	    }
	  }, {
	    key: 'handleSendQuickReply',
	    value: function handleSendQuickReply(title, payload) {
	      return this.handleSendData({
	        type: 'quick_reply',
	        text: title,
	        data: { payload: payload }
	      });
	    }
	  }, {
	    key: 'handleSendForm',
	    value: function handleSendForm(fields, formId, repr) {
	      return this.handleSendData({
	        type: 'form',
	        formId: formId,
	        text: repr,
	        data: fields
	      });
	    }
	  }, {
	    key: 'handleLoginPrompt',
	    value: function handleLoginPrompt(username, password) {
	      return this.handleSendData({
	        type: 'login_prompt',
	        text: 'Provided login information',
	        data: { username: username, password: password }
	      });
	    }
	  }, {
	    key: 'handleFileUploadSend',
	    value: function handleFileUploadSend(title, payload, file) {
	      var userId = window.__BP_VISITOR_ID;
	      var url = BOT_HOSTNAME + '/api/botpress-platform-webchat/messages/' + userId + '/files';
	      var config = { params: { conversationId: this.state.currentConversationId } };
	
	      var data = new FormData();
	      data.append('file', file);
	
	      return this.props.bp.axios.post(url, data, config).then();
	    }
	  }, {
	    key: 'handleSendData',
	    value: function handleSendData(data) {
	      var userId = window.__BP_VISITOR_ID;
	      var url = BOT_HOSTNAME + '/api/botpress-platform-webchat/messages/' + userId;
	      var config = { params: { conversationId: this.state.currentConversationId } };
	      return this.props.bp.axios.post(url, data, config).then();
	    }
	  }, {
	    key: 'handleSwitchConvo',
	    value: function handleSwitchConvo(convoId) {
	      this.setState({
	        currentConversation: null,
	        currentConversationId: convoId
	      });
	
	      this.fetchCurrentConversation(convoId);
	    }
	  }, {
	    key: 'handleClosePanel',
	    value: function handleClosePanel() {
	      this.handleSwitchView('widget');
	    }
	  }, {
	    key: 'renderOpenIcon',
	    value: function renderOpenIcon() {
	      return _react2.default.createElement(
	        'svg',
	        { width: '20', height: '20', viewBox: '0 0 20 20', xmlns: 'http://www.w3.org/2000/svg' },
	        _react2.default.createElement('path', { d: 'M4.583 14.894l-3.256 3.78c-.7.813-1.26.598-1.25-.46a10689.413 10689.413 0 0 1 .035-4.775V4.816a3.89 3.89 0 0 1 3.88-3.89h12.064a3.885 3.885 0 0 1 3.882 3.89v6.185a3.89 3.89 0 0 1-3.882 3.89H4.583z', fill: '#FFF', 'fill-rule': 'evenodd' })
	      );
	    }
	  }, {
	    key: 'renderCloseIcon',
	    value: function renderCloseIcon() {
	      return _react2.default.createElement(
	        'svg',
	        { width: '17', height: '17', viewBox: '0 0 17 17', xmlns: 'http://www.w3.org/2000/svg' },
	        _react2.default.createElement('path', { d: 'M16.726 15.402c.365.366.365.96 0 1.324-.178.178-.416.274-.663.274-.246 0-.484-.096-.663-.274L8.323 9.648h.353L1.6 16.726c-.177.178-.416.274-.663.274-.246 0-.484-.096-.663-.274-.365-.365-.365-.958 0-1.324L7.35 8.324v.35L.275 1.6C-.09 1.233-.09.64.274.274c.367-.365.96-.365 1.326 0l7.076 7.078h-.353L15.4.274c.366-.365.96-.365 1.326 0 .365.366.365.958 0 1.324L9.65 8.675v-.35l7.076 7.077z', fill: '#FFF', 'fill-rule': 'evenodd' })
	      );
	    }
	  }, {
	    key: 'renderUncountMessages',
	    value: function renderUncountMessages() {
	      return _react2.default.createElement(
	        'span',
	        { className: _style2.default.unread },
	        this.state.unreadCount
	      );
	    }
	  }, {
	    key: 'renderButton',
	    value: function renderButton() {
	      if (this.state.isButtonHidden) return null;
	      return _react2.default.createElement(
	        'button',
	        {
	          className: _style2.default[this.state.widgetTransition],
	          onClick: this.handleButtonClicked.bind(this),
	          style: { backgroundColor: this.state.config.foregroundColor } },
	        _react2.default.createElement(
	          'i',
	          null,
	          this.state.view === 'convo' ? this.renderCloseIcon() : this.renderOpenIcon()
	        ),
	        this.state.unreadCount > 0 ? this.renderUncountMessages() : null
	      );
	    }
	  }, {
	    key: 'renderWidget',
	    value: function renderWidget() {
	      return _react2.default.createElement(
	        'div',
	        { className: (0, _classnames2.default)(_style2.default['container']) },
	        _react2.default.createElement(
	          'div',
	          { className: (0, _classnames2.default)(_style2.default['widget-container']) },
	          _react2.default.createElement(
	            'span',
	            null,
	            this.state.view === 'convo' ? _react2.default.createElement(_convo2.default, {
	              transition: this.state.convoTransition,
	              change: this.handleTextChanged.bind(this),
	              send: this.handleSendMessage.bind(this),
	              config: this.state.config,
	              text: this.state.textToSend }) : null,
	            this.renderButton()
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderSide',
	    value: function renderSide() {
	      return _react2.default.createElement(_side2.default, {
	        config: this.state.config,
	        text: this.state.textToSend,
	
	        fullscreen: this.props.fullscreen,
	        transition: !this.props.fullscreen ? this.state.sideTransition : null,
	        unreadCount: this.state.unreadCount,
	
	        currentConversation: this.state.currentConversation,
	        conversations: this.state.conversations,
	
	        addEmojiToText: this.handleAddEmoji.bind(this),
	
	        onClose: !this.props.fullscreen ? this.handleClosePanel.bind(this) : null,
	        onSwitchConvo: this.handleSwitchConvo.bind(this),
	        onTextSend: this.handleSendMessage.bind(this),
	        onTextChanged: this.handleTextChanged.bind(this),
	        onQuickReplySend: this.handleSendQuickReply.bind(this),
	        onFormSend: this.handleSendForm.bind(this),
	        onFileUploadSend: this.handleFileUploadSend.bind(this),
	        onLoginPromptSend: this.handleLoginPrompt.bind(this) });
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      if (this.state.loading || !this.state.view) {
	        return null;
	      }
	
	      window.parent && window.parent.postMessage({ type: 'setClass', value: 'bp-widget-web bp-widget-' + this.state.view }, '*');
	
	      var view = this.state.view !== 'side' && !this.props.fullscreen ? this.renderWidget() : this.renderSide();
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.web, onFocus: this.handleResetUnreadCount.bind(this) },
	        view
	      );
	    }
	  }]);
	
	  return Web;
	}(_react2.default.Component);
	
	exports.default = Web;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = React;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	  Copyright (c) 2016 Jed Watson.
	  Licensed under the MIT License (MIT), see
	  http://jedwatson.github.io/classnames
	*/
	/* global define */
	
	(function () {
		'use strict';
	
		var hasOwn = {}.hasOwnProperty;
	
		function classNames () {
			var classes = [];
	
			for (var i = 0; i < arguments.length; i++) {
				var arg = arguments[i];
				if (!arg) continue;
	
				var argType = typeof arg;
	
				if (argType === 'string' || argType === 'number') {
					classes.push(arg);
				} else if (Array.isArray(arg)) {
					classes.push(classNames.apply(null, arg));
				} else if (argType === 'object') {
					for (var key in arg) {
						if (hasOwn.call(arg, key) && arg[key]) {
							classes.push(key);
						}
					}
				}
			}
	
			return classes.join(' ');
		}
	
		if (typeof module !== 'undefined' && module.exports) {
			module.exports = classNames;
		} else if (true) {
			// register as 'classnames', consistent with npm package name
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function () {
				return classNames;
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else {
			window.classNames = classNames;
		}
	}());


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Millisecond Helpers
	 * @summary Add the specified number of milliseconds to the given date.
	 *
	 * @description
	 * Add the specified number of milliseconds to the given date.
	 *
	 * @param {Date|String|Number} date - the date to be changed
	 * @param {Number} amount - the amount of milliseconds to be added
	 * @returns {Date} the new date with the milliseconds added
	 *
	 * @example
	 * // Add 750 milliseconds to 10 July 2014 12:45:30.000:
	 * var result = addMilliseconds(new Date(2014, 6, 10, 12, 45, 30, 0), 750)
	 * //=> Thu Jul 10 2014 12:45:30.750
	 */
	function addMilliseconds (dirtyDate, dirtyAmount) {
	  var timestamp = parse(dirtyDate).getTime()
	  var amount = Number(dirtyAmount)
	  return new Date(timestamp + amount)
	}
	
	module.exports = addMilliseconds


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	var isDate = __webpack_require__(6)
	
	var MILLISECONDS_IN_HOUR = 3600000
	var MILLISECONDS_IN_MINUTE = 60000
	var DEFAULT_ADDITIONAL_DIGITS = 2
	
	var parseTokenDateTimeDelimeter = /[T ]/
	var parseTokenPlainTime = /:/
	
	// year tokens
	var parseTokenYY = /^(\d{2})$/
	var parseTokensYYY = [
	  /^([+-]\d{2})$/, // 0 additional digits
	  /^([+-]\d{3})$/, // 1 additional digit
	  /^([+-]\d{4})$/ // 2 additional digits
	]
	
	var parseTokenYYYY = /^(\d{4})/
	var parseTokensYYYYY = [
	  /^([+-]\d{4})/, // 0 additional digits
	  /^([+-]\d{5})/, // 1 additional digit
	  /^([+-]\d{6})/ // 2 additional digits
	]
	
	// date tokens
	var parseTokenMM = /^-(\d{2})$/
	var parseTokenDDD = /^-?(\d{3})$/
	var parseTokenMMDD = /^-?(\d{2})-?(\d{2})$/
	var parseTokenWww = /^-?W(\d{2})$/
	var parseTokenWwwD = /^-?W(\d{2})-?(\d{1})$/
	
	// time tokens
	var parseTokenHH = /^(\d{2}([.,]\d*)?)$/
	var parseTokenHHMM = /^(\d{2}):?(\d{2}([.,]\d*)?)$/
	var parseTokenHHMMSS = /^(\d{2}):?(\d{2}):?(\d{2}([.,]\d*)?)$/
	
	// timezone tokens
	var parseTokenTimezone = /([Z+-].*)$/
	var parseTokenTimezoneZ = /^(Z)$/
	var parseTokenTimezoneHH = /^([+-])(\d{2})$/
	var parseTokenTimezoneHHMM = /^([+-])(\d{2}):?(\d{2})$/
	
	/**
	 * @category Common Helpers
	 * @summary Convert the given argument to an instance of Date.
	 *
	 * @description
	 * Convert the given argument to an instance of Date.
	 *
	 * If the argument is an instance of Date, the function returns its clone.
	 *
	 * If the argument is a number, it is treated as a timestamp.
	 *
	 * If an argument is a string, the function tries to parse it.
	 * Function accepts complete ISO 8601 formats as well as partial implementations.
	 * ISO 8601: http://en.wikipedia.org/wiki/ISO_8601
	 *
	 * If all above fails, the function passes the given argument to Date constructor.
	 *
	 * @param {Date|String|Number} argument - the value to convert
	 * @param {Object} [options] - the object with options
	 * @param {0 | 1 | 2} [options.additionalDigits=2] - the additional number of digits in the extended year format
	 * @returns {Date} the parsed date in the local time zone
	 *
	 * @example
	 * // Convert string '2014-02-11T11:30:30' to date:
	 * var result = parse('2014-02-11T11:30:30')
	 * //=> Tue Feb 11 2014 11:30:30
	 *
	 * @example
	 * // Parse string '+02014101',
	 * // if the additional number of digits in the extended year format is 1:
	 * var result = parse('+02014101', {additionalDigits: 1})
	 * //=> Fri Apr 11 2014 00:00:00
	 */
	function parse (argument, dirtyOptions) {
	  if (isDate(argument)) {
	    // Prevent the date to lose the milliseconds when passed to new Date() in IE10
	    return new Date(argument.getTime())
	  } else if (typeof argument !== 'string') {
	    return new Date(argument)
	  }
	
	  var options = dirtyOptions || {}
	  var additionalDigits = options.additionalDigits
	  if (additionalDigits == null) {
	    additionalDigits = DEFAULT_ADDITIONAL_DIGITS
	  } else {
	    additionalDigits = Number(additionalDigits)
	  }
	
	  var dateStrings = splitDateString(argument)
	
	  var parseYearResult = parseYear(dateStrings.date, additionalDigits)
	  var year = parseYearResult.year
	  var restDateString = parseYearResult.restDateString
	
	  var date = parseDate(restDateString, year)
	
	  if (date) {
	    var timestamp = date.getTime()
	    var time = 0
	    var offset
	
	    if (dateStrings.time) {
	      time = parseTime(dateStrings.time)
	    }
	
	    if (dateStrings.timezone) {
	      offset = parseTimezone(dateStrings.timezone)
	    } else {
	      // get offset accurate to hour in timezones that change offset
	      offset = new Date(timestamp + time).getTimezoneOffset()
	      offset = new Date(timestamp + time + offset * MILLISECONDS_IN_MINUTE).getTimezoneOffset()
	    }
	
	    return new Date(timestamp + time + offset * MILLISECONDS_IN_MINUTE)
	  } else {
	    return new Date(argument)
	  }
	}
	
	function splitDateString (dateString) {
	  var dateStrings = {}
	  var array = dateString.split(parseTokenDateTimeDelimeter)
	  var timeString
	
	  if (parseTokenPlainTime.test(array[0])) {
	    dateStrings.date = null
	    timeString = array[0]
	  } else {
	    dateStrings.date = array[0]
	    timeString = array[1]
	  }
	
	  if (timeString) {
	    var token = parseTokenTimezone.exec(timeString)
	    if (token) {
	      dateStrings.time = timeString.replace(token[1], '')
	      dateStrings.timezone = token[1]
	    } else {
	      dateStrings.time = timeString
	    }
	  }
	
	  return dateStrings
	}
	
	function parseYear (dateString, additionalDigits) {
	  var parseTokenYYY = parseTokensYYY[additionalDigits]
	  var parseTokenYYYYY = parseTokensYYYYY[additionalDigits]
	
	  var token
	
	  // YYYY or ±YYYYY
	  token = parseTokenYYYY.exec(dateString) || parseTokenYYYYY.exec(dateString)
	  if (token) {
	    var yearString = token[1]
	    return {
	      year: parseInt(yearString, 10),
	      restDateString: dateString.slice(yearString.length)
	    }
	  }
	
	  // YY or ±YYY
	  token = parseTokenYY.exec(dateString) || parseTokenYYY.exec(dateString)
	  if (token) {
	    var centuryString = token[1]
	    return {
	      year: parseInt(centuryString, 10) * 100,
	      restDateString: dateString.slice(centuryString.length)
	    }
	  }
	
	  // Invalid ISO-formatted year
	  return {
	    year: null
	  }
	}
	
	function parseDate (dateString, year) {
	  // Invalid ISO-formatted year
	  if (year === null) {
	    return null
	  }
	
	  var token
	  var date
	  var month
	  var week
	
	  // YYYY
	  if (dateString.length === 0) {
	    date = new Date(0)
	    date.setUTCFullYear(year)
	    return date
	  }
	
	  // YYYY-MM
	  token = parseTokenMM.exec(dateString)
	  if (token) {
	    date = new Date(0)
	    month = parseInt(token[1], 10) - 1
	    date.setUTCFullYear(year, month)
	    return date
	  }
	
	  // YYYY-DDD or YYYYDDD
	  token = parseTokenDDD.exec(dateString)
	  if (token) {
	    date = new Date(0)
	    var dayOfYear = parseInt(token[1], 10)
	    date.setUTCFullYear(year, 0, dayOfYear)
	    return date
	  }
	
	  // YYYY-MM-DD or YYYYMMDD
	  token = parseTokenMMDD.exec(dateString)
	  if (token) {
	    date = new Date(0)
	    month = parseInt(token[1], 10) - 1
	    var day = parseInt(token[2], 10)
	    date.setUTCFullYear(year, month, day)
	    return date
	  }
	
	  // YYYY-Www or YYYYWww
	  token = parseTokenWww.exec(dateString)
	  if (token) {
	    week = parseInt(token[1], 10) - 1
	    return dayOfISOYear(year, week)
	  }
	
	  // YYYY-Www-D or YYYYWwwD
	  token = parseTokenWwwD.exec(dateString)
	  if (token) {
	    week = parseInt(token[1], 10) - 1
	    var dayOfWeek = parseInt(token[2], 10) - 1
	    return dayOfISOYear(year, week, dayOfWeek)
	  }
	
	  // Invalid ISO-formatted date
	  return null
	}
	
	function parseTime (timeString) {
	  var token
	  var hours
	  var minutes
	
	  // hh
	  token = parseTokenHH.exec(timeString)
	  if (token) {
	    hours = parseFloat(token[1].replace(',', '.'))
	    return (hours % 24) * MILLISECONDS_IN_HOUR
	  }
	
	  // hh:mm or hhmm
	  token = parseTokenHHMM.exec(timeString)
	  if (token) {
	    hours = parseInt(token[1], 10)
	    minutes = parseFloat(token[2].replace(',', '.'))
	    return (hours % 24) * MILLISECONDS_IN_HOUR +
	      minutes * MILLISECONDS_IN_MINUTE
	  }
	
	  // hh:mm:ss or hhmmss
	  token = parseTokenHHMMSS.exec(timeString)
	  if (token) {
	    hours = parseInt(token[1], 10)
	    minutes = parseInt(token[2], 10)
	    var seconds = parseFloat(token[3].replace(',', '.'))
	    return (hours % 24) * MILLISECONDS_IN_HOUR +
	      minutes * MILLISECONDS_IN_MINUTE +
	      seconds * 1000
	  }
	
	  // Invalid ISO-formatted time
	  return null
	}
	
	function parseTimezone (timezoneString) {
	  var token
	  var absoluteOffset
	
	  // Z
	  token = parseTokenTimezoneZ.exec(timezoneString)
	  if (token) {
	    return 0
	  }
	
	  // ±hh
	  token = parseTokenTimezoneHH.exec(timezoneString)
	  if (token) {
	    absoluteOffset = parseInt(token[2], 10) * 60
	    return (token[1] === '+') ? -absoluteOffset : absoluteOffset
	  }
	
	  // ±hh:mm or ±hhmm
	  token = parseTokenTimezoneHHMM.exec(timezoneString)
	  if (token) {
	    absoluteOffset = parseInt(token[2], 10) * 60 + parseInt(token[3], 10)
	    return (token[1] === '+') ? -absoluteOffset : absoluteOffset
	  }
	
	  return 0
	}
	
	function dayOfISOYear (isoYear, week, day) {
	  week = week || 0
	  day = day || 0
	  var date = new Date(0)
	  date.setUTCFullYear(isoYear, 0, 4)
	  var fourthOfJanuaryDay = date.getUTCDay() || 7
	  var diff = week * 7 + day + 1 - fourthOfJanuaryDay
	  date.setUTCDate(date.getUTCDate() + diff)
	  return date
	}
	
	module.exports = parse


/***/ }),
/* 6 */
/***/ (function(module, exports) {

	/**
	 * @category Common Helpers
	 * @summary Is the given argument an instance of Date?
	 *
	 * @description
	 * Is the given argument an instance of Date?
	 *
	 * @param {*} argument - the argument to check
	 * @returns {Boolean} the given argument is an instance of Date
	 *
	 * @example
	 * // Is 'mayonnaise' a Date?
	 * var result = isDate('mayonnaise')
	 * //=> false
	 */
	function isDate (argument) {
	  return argument instanceof Date
	}
	
	module.exports = isDate


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Common Helpers
	 * @summary Is the first date before the second one?
	 *
	 * @description
	 * Is the first date before the second one?
	 *
	 * @param {Date|String|Number} date - the date that should be before the other one to return true
	 * @param {Date|String|Number} dateToCompare - the date to compare with
	 * @returns {Boolean} the first date is before the second date
	 *
	 * @example
	 * // Is 10 July 1989 before 11 February 1987?
	 * var result = isBefore(new Date(1989, 6, 10), new Date(1987, 1, 11))
	 * //=> false
	 */
	function isBefore (dirtyDate, dirtyDateToCompare) {
	  var date = parse(dirtyDate)
	  var dateToCompare = parse(dirtyDateToCompare)
	  return date.getTime() < dateToCompare.getTime()
	}
	
	module.exports = isBefore


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactDom = __webpack_require__(9);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _send = __webpack_require__(10);
	
	var _send2 = _interopRequireDefault(_send);
	
	var _input = __webpack_require__(15);
	
	var _input2 = _interopRequireDefault(_input);
	
	var _bot_avatar = __webpack_require__(18);
	
	var _bot_avatar2 = _interopRequireDefault(_bot_avatar);
	
	var _style = __webpack_require__(21);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var DEFAULT_NAME = 'Bot';
	var DEFAULT_WELCOME_MESSAGE = 'Hello!\n  Curious about our offer?\n  It will be a pleasure to help you getting started.\n';
	
	var Convo = function (_React$Component) {
	  _inherits(Convo, _React$Component);
	
	  function Convo(props) {
	    _classCallCheck(this, Convo);
	
	    var _this = _possibleConstructorReturn(this, (Convo.__proto__ || Object.getPrototypeOf(Convo)).call(this, props));
	
	    _this.state = {
	      text: ''
	    };
	    return _this;
	  }
	
	  _createClass(Convo, [{
	    key: 'handleTextChanged',
	    value: function handleTextChanged(event) {
	      this.setState({
	        text: event.target.value
	      });
	    }
	  }, {
	    key: 'renderSendButton',
	    value: function renderSendButton() {
	      return _react2.default.createElement(
	        'span',
	        null,
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default['enter-prompt'] },
	          _react2.default.createElement(
	            'a',
	            null,
	            _react2.default.createElement(
	              'i',
	              { 'class': 'flex' },
	              _react2.default.createElement(
	                'svg',
	                { width: '13', height: '13', viewBox: '0 0 13 13', xmlns: 'http://www.w3.org/2000/svg' },
	                _react2.default.createElement('path', { d: 'M12.975.38c.014.043.02.087.024.132v.06c-.004.048-.014.095-.03.14-.006.017-.007.032-.014.046L7.252 12.692c-.09.19-.28.308-.49.308-.216-.002-.406-.127-.493-.32l-.537-3.41C5.56 8.18 4.55 7.1 3.478 6.86l-3.2-.72c-.18-.1-.287-.293-.277-.5.012-.206.138-.39.328-.47L12.248.04 12.3.026c.05-.015.098-.025.148-.026.02 0 .038 0 .058.003.046.004.09.013.132.028l.055.02c.056.027.11.06.154.107.053.053.085.11.11.168.008.018.013.036.018.055z', 'fill-rule': 'evenodd' })
	              )
	            ),
	            _react2.default.createElement(
	              'span',
	              null,
	              'Send Message'
	            )
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderPromoElement',
	    value: function renderPromoElement() {
	      return null;
	    }
	  }, {
	    key: 'renderName',
	    value: function renderName() {
	      var name = this.props.config.botName || DEFAULT_NAME;
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.name,
	          style: {
	            color: this.props.config.textColorOnForeground
	          } },
	        _react2.default.createElement(
	          'div',
	          null,
	          _react2.default.createElement(
	            'span',
	            null,
	            name
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderAvatar',
	    value: function renderAvatar() {
	      var content = _react2.default.createElement(_bot_avatar2.default, { foregroundColor: this.props.config.foregroundColor });
	
	      if (this.props.config.botAvatarUrl) {
	        content = _react2.default.createElement('div', { className: _style2.default.picture, style: { backgroundImage: 'url(' + this.props.config.botAvatarUrl + ')' } });
	      }
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.avatar },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.square },
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.circle,
	              style: {
	                borderColor: this.props.config.foregroundColor,
	                color: this.props.config.foregroundColor
	              } },
	            content
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderWelcomeMessage',
	    value: function renderWelcomeMessage() {
	      var message = this.props.config.welcomeMsgText || DEFAULT_WELCOME_MESSAGE;
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.paragraph },
	        message
	      );
	    }
	  }, {
	    key: 'renderHeader',
	    value: function renderHeader() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.header,
	          style: {
	            color: this.props.config.textColorOnForeground,
	            backgroundColor: this.props.config.foregroundColor
	          } },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.line },
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.title },
	            this.renderAvatar(),
	            this.renderName()
	          )
	        ),
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.text },
	          this.renderWelcomeMessage()
	        )
	      );
	    }
	  }, {
	    key: 'renderComposer',
	    value: function renderComposer() {
	      return _react2.default.createElement(
	        'div',
	        { className: (0, _classnames2.default)(_style2.default.composer) },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default['flex-column'] },
	          _react2.default.createElement(_input2.default, {
	            send: this.props.send,
	            change: this.props.change,
	            placeholder: 'Type your message...',
	            text: this.props.text,
	            config: this.props.config }),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.bottom },
	            _react2.default.createElement(_send2.default, {
	              text: this.props.text,
	              send: this.props.send,
	              config: this.props.config })
	          )
	        )
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var classNames = (0, _classnames2.default)(_style2.default.internal, _style2.default[this.props.transition]);
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.external },
	        _react2.default.createElement(
	          'div',
	          { className: classNames,
	            style: {
	              color: this.props.config.textColorOnBackground,
	              backgroundColor: this.props.config.backgroundColor
	            } },
	          this.renderHeader(),
	          this.renderComposer()
	        )
	      );
	    }
	  }]);
	
	  return Convo;
	}(_react2.default.Component);
	
	exports.default = Convo;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	module.exports = ReactDOM;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _style = __webpack_require__(11);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Send = function (_Component) {
	  _inherits(Send, _Component);
	
	  function Send(props) {
	    _classCallCheck(this, Send);
	
	    return _possibleConstructorReturn(this, (Send.__proto__ || Object.getPrototypeOf(Send)).call(this, props));
	  }
	
	  _createClass(Send, [{
	    key: 'renderSendButton',
	    value: function renderSendButton() {
	      return _react2.default.createElement(
	        'span',
	        null,
	        _react2.default.createElement(
	          'div',
	          {
	            className: _style2.default['enter-prompt'],
	            onClick: this.props.send },
	          _react2.default.createElement(
	            'a',
	            { style: { color: this.props.config.foregroundColor } },
	            _react2.default.createElement(
	              'i',
	              { 'class': 'flex' },
	              _react2.default.createElement(
	                'svg',
	                { width: '13', height: '13', viewBox: '0 0 13 13', xmlns: 'http://www.w3.org/2000/svg' },
	                _react2.default.createElement('path', { d: 'M12.975.38c.014.043.02.087.024.132v.06c-.004.048-.014.095-.03.14-.006.017-.007.032-.014.046L7.252 12.692c-.09.19-.28.308-.49.308-.216-.002-.406-.127-.493-.32l-.537-3.41C5.56 8.18 4.55 7.1 3.478 6.86l-3.2-.72c-.18-.1-.287-.293-.277-.5.012-.206.138-.39.328-.47L12.248.04 12.3.026c.05-.015.098-.025.148-.026.02 0 .038 0 .058.003.046.004.09.013.132.028l.055.02c.056.027.11.06.154.107.053.053.085.11.11.168.008.018.013.036.018.055z', 'fill-rule': 'evenodd' })
	              )
	            ),
	            _react2.default.createElement(
	              'span',
	              null,
	              'Send Message'
	            )
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderPromoElement',
	    value: function renderPromoElement() {
	      return null;
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.footer },
	        this.props.text === '' ? this.renderPromoElement() : this.renderSendButton()
	      );
	    }
	  }]);
	
	  return Send;
	}(_react.Component);
	
	exports.default = Send;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(12);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, ".botpress-platform-webchat__style__footer___36Ycj {\n  -webkit-box-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  position: relative;\n  height: 20px;\n  font-size: 12px;\n  color: #9a9a9a; }\n  .botpress-platform-webchat__style__footer___36Ycj span .botpress-platform-webchat__style__flex-minimal___2d_b3 {\n    height: 20px;\n    position: relative;\n    -webkit-box-pack: end;\n    -ms-flex-pack: end;\n    justify-content: flex-end;\n    display: flex; }\n    .botpress-platform-webchat__style__footer___36Ycj span .botpress-platform-webchat__style__flex-minimal___2d_b3 .botpress-platform-webchat__style__element___cUAaK {\n      display: flex;\n      align-items: center; }\n      .botpress-platform-webchat__style__footer___36Ycj span .botpress-platform-webchat__style__flex-minimal___2d_b3 .botpress-platform-webchat__style__element___cUAaK span i {\n        vertical-align: middle;\n        display: inline;\n        margin: -3px 2px;\n        font-style: italic;\n        text-align: center;\n        padding: 0; }\n      .botpress-platform-webchat__style__footer___36Ycj span .botpress-platform-webchat__style__flex-minimal___2d_b3 .botpress-platform-webchat__style__element___cUAaK span a {\n        font-weight: 500;\n        cursor: pointer;\n        text-decoration: none; }\n  .botpress-platform-webchat__style__footer___36Ycj .botpress-platform-webchat__style__enter-prompt___2A1iw {\n    position: absolute;\n    top: 0;\n    right: 0;\n    height: 20px; }\n    .botpress-platform-webchat__style__footer___36Ycj .botpress-platform-webchat__style__enter-prompt___2A1iw a {\n      display: flex;\n      font-weight: 500;\n      font-size: 14px;\n      cursor: pointer;\n      text-decoration: none; }\n      .botpress-platform-webchat__style__footer___36Ycj .botpress-platform-webchat__style__enter-prompt___2A1iw a i {\n        vertical-align: middle;\n        display: inline;\n        margin: 2px 6px;\n        text-align: center;\n        padding: 0; }\n        .botpress-platform-webchat__style__footer___36Ycj .botpress-platform-webchat__style__enter-prompt___2A1iw a i svg {\n          fill: currentColor; }\n", ""]);
	
	// exports
	exports.locals = {
		"footer": "botpress-platform-webchat__style__footer___36Ycj",
		"flex-minimal": "botpress-platform-webchat__style__flex-minimal___2d_b3",
		"element": "botpress-platform-webchat__style__element___cUAaK",
		"enter-prompt": "botpress-platform-webchat__style__enter-prompt___2A1iw"
	};

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _style = __webpack_require__(16);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Send = function (_Component) {
	  _inherits(Send, _Component);
	
	  function Send(props) {
	    _classCallCheck(this, Send);
	
	    return _possibleConstructorReturn(this, (Send.__proto__ || Object.getPrototypeOf(Send)).call(this, props));
	  }
	
	  _createClass(Send, [{
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      this.textInput.focus();
	    }
	  }, {
	    key: 'handleFocus',
	    value: function handleFocus(value) {
	      if (this.props.focused) {
	        this.props.focused(value);
	      }
	    }
	  }, {
	    key: 'handleKeyPress',
	    value: function handleKeyPress(e) {
	      if (e.key === 'Enter') {
	        this.props.send();
	        e.preventDefault();
	      }
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;
	
	      return _react2.default.createElement(
	        'div',
	        { tabIndex: '-1', className: _style2.default.input },
	        _react2.default.createElement('textarea', { tabindex: '1',
	          ref: function ref(input) {
	            _this2.textInput = input;
	          },
	          onBlur: function onBlur() {
	            return _this2.handleFocus(false);
	          },
	          onFocus: function onFocus() {
	            return _this2.handleFocus(true);
	          },
	          placeholder: this.props.placeholder,
	          onChange: this.props.change,
	          value: this.props.text,
	          onKeyPress: this.handleKeyPress.bind(this),
	          style: {
	            color: this.props.config.textColorOnBackground
	          } })
	      );
	    }
	  }]);
	
	  return Send;
	}(_react.Component);
	
	exports.default = Send;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(17);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, ".botpress-platform-webchat__style__input___3yaCn {\n  outline: none; }\n  .botpress-platform-webchat__style__input___3yaCn textarea {\n    font: inherit;\n    background-color: transparent;\n    min-height: 32px;\n    max-height: 96px;\n    margin-bottom: 0;\n    font-size: 14px;\n    padding: 8px;\n    width: 100%;\n    border: none;\n    resize: none;\n    line-height: 1.2; }\n", ""]);
	
	// exports
	exports.locals = {
		"input": "botpress-platform-webchat__style__input___3yaCn"
	};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _style = __webpack_require__(19);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var BotAvatar = function BotAvatar(props) {
	
	  var color = props.foregroundColor || '#A029D3';
	
	  return React.createElement(
	    'i',
	    null,
	    React.createElement(
	      'svg',
	      { viewBox: '0 0 254 252', xmlns: 'http://www.w3.org/2000/svg' },
	      React.createElement(
	        'g',
	        { fill: 'none', 'fill-rule': 'evenodd' },
	        React.createElement('path', { d: 'M127 252c69.036 0 125-55.964 125-125S196.036 2 127 2 2 57.964 2 127s55.964 125 125 125z', className: _style2.default.color, fill: color }),
	        React.createElement('path', { d: 'M175.217 244.042c45.946-18.958 78.283-64.196 78.283-116.986 0-69.864-56.636-126.5-126.5-126.5S.5 57.192.5 127.056c0 52.898 32.469 98.212 78.564 117.101 5.747-18.068 21.627-31.698 41.055-34.382v-9.935h-71.37c-4.652 0-8.402-3.714-8.402-8.296V95.308c0-4.575 3.758-8.297 8.393-8.297h64.464V73.472h13.66V50.136c-5.305-1.859-9.106-6.874-9.106-12.77 0-7.477 6.116-13.539 13.66-13.539 7.545 0 13.661 6.062 13.661 13.54 0 5.895-3.801 10.91-9.107 12.769v23.336h13.66v13.54h59.895c4.64 0 8.41 3.714 8.41 8.296v96.236c0 4.574-3.762 8.296-8.402 8.296h-74.46v10.064c18.988 2.96 34.451 16.4 40.142 34.138z', fill: '#FFF', opacity: '.85' }),
	        React.createElement('rect', { fill: '#FFF', x: '64.365', y: '114.515', width: '130.488', height: '40.691', rx: '10' }),
	        React.createElement('ellipse', { className: _style2.default.color, fill: color, opacity: '.5', cx: '92.283', cy: '135.674', rx: '14.78', ry: '14.649' }),
	        React.createElement('ellipse', { className: _style2.default.color, fill: color, opacity: '.5', cx: '162.897', cy: '135.674', rx: '14.78', ry: '14.649' }),
	        React.createElement('ellipse', { fill: '#000', opacity: '.6', cx: '92.283', cy: '135.674', rx: '14.78', ry: '14.649' }),
	        React.createElement('ellipse', { fill: '#000', opacity: '.6', cx: '162.897', cy: '135.674', rx: '14.78', ry: '14.649' })
	      )
	    )
	  );
	};
	
	exports.default = BotAvatar;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(20);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	exports.push([module.id, "@import url(https://fonts.googleapis.com/css?family=Lato);", ""]);
	
	// module
	exports.push([module.id, "body {\n  margin: 0 !important;\n  font-family: 'Lato', sans-serif;\n  overflow: hidden; }\n\n.botpress-platform-webchat__style__web___KyLc_ button {\n  float: right;\n  fill: white;\n  cursor: pointer;\n  outline: none;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  z-index: 1;\n  width: 52px;\n  height: 52px;\n  cursor: pointer;\n  border-radius: 5px;\n  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.4);\n  overflow: hidden;\n  padding: 0;\n  border: none;\n  background-clip: padding-box;\n  transition: width 1s, height 1s; }\n  .botpress-platform-webchat__style__web___KyLc_ button i {\n    transition: opacity .3s ease;\n    opacity: 1;\n    fill: inherit;\n    stroke: inherit;\n    width: 100%;\n    padding: 0;\n    line-height: 0; }\n  .botpress-platform-webchat__style__web___KyLc_ button .botpress-platform-webchat__style__unread___23aWv {\n    display: block;\n    position: absolute;\n    right: 2px;\n    bottom: 54px;\n    width: 20px !important;\n    height: 20px !important;\n    border-radius: 50%;\n    line-height: 20px;\n    color: #fff;\n    background-color: #ff5d5d;\n    box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.4); }\n\n.botpress-platform-webchat__style__container___LtHgy {\n  overflow: hidden; }\n  .botpress-platform-webchat__style__container___LtHgy .botpress-platform-webchat__style__widget-container___3GITr {\n    display: flex;\n    justify-content: flex-end;\n    align-items: flex-end;\n    padding: 12px 12px 12px 16px;\n    flex-direction: column; }\n    .botpress-platform-webchat__style__container___LtHgy .botpress-platform-webchat__style__widget-container___3GITr * {\n      box-sizing: border-box;\n      outline: none; }\n    .botpress-platform-webchat__style__container___LtHgy .botpress-platform-webchat__style__widget-container___3GITr span {\n      flex: 1 0 auto;\n      width: 100%; }\n\n.botpress-platform-webchat__style__fadeIn___2KKLx {\n  -webkit-animation-name: botpress-platform-webchat__style__zoomIn___3fpS3;\n  animation-name: botpress-platform-webchat__style__zoomIn___3fpS3;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n@-webkit-keyframes botpress-platform-webchat__style__zoomIn___3fpS3 {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n@keyframes botpress-platform-webchat__style__zoomIn___3fpS3 {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n.botpress-platform-webchat__style__fadeOut___HgrS8 {\n  -webkit-animation-name: botpress-platform-webchat__style__zoomOut___2YZKl;\n  animation-name: botpress-platform-webchat__style__zoomOut___2YZKl;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n@-webkit-keyframes botpress-platform-webchat__style__zoomOut___2YZKl {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  100% {\n    opacity: 0; } }\n\n@keyframes botpress-platform-webchat__style__zoomOut___2YZKl {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  100% {\n    opacity: 0; } }\n", ""]);
	
	// exports
	exports.locals = {
		"web": "botpress-platform-webchat__style__web___KyLc_",
		"unread": "botpress-platform-webchat__style__unread___23aWv",
		"container": "botpress-platform-webchat__style__container___LtHgy",
		"widget-container": "botpress-platform-webchat__style__widget-container___3GITr",
		"fadeIn": "botpress-platform-webchat__style__fadeIn___2KKLx",
		"zoomIn": "botpress-platform-webchat__style__zoomIn___3fpS3",
		"fadeOut": "botpress-platform-webchat__style__fadeOut___HgrS8",
		"zoomOut": "botpress-platform-webchat__style__zoomOut___2YZKl"
	};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(22);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, ".botpress-platform-webchat__style__external___2svzf {\n  -webkit-box-flex: 1;\n  -ms-flex: 1 0 auto;\n  flex: 1 0 auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  margin: 32px 0 16px 16px;\n  position: relative;\n  height: 100%; }\n  .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy {\n    -webkit-box-flex: 1;\n    -ms-flex: 1 0 auto;\n    flex: 1 0 auto;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n    -ms-flex-direction: column;\n    flex-direction: column;\n    box-shadow: 0 4px 23px 0 rgba(0, 0, 0, 0.09);\n    border-radius: 5px; }\n    .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr {\n      -webkit-box-flex: 1;\n      -ms-flex: 1 0 auto;\n      flex: 1 0 auto;\n      border-top-left-radius: inherit;\n      border-top-right-radius: inherit; }\n      .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n        justify-content: space-between;\n        -webkit-box-align: center;\n        -ms-flex-align: center;\n        align-items: center;\n        font-weight: 500;\n        margin-bottom: 20px;\n        margin-top: 12px;\n        padding-left: 18px;\n        padding-right: 18px; }\n        .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n          -webkit-box-align: center;\n          -ms-flex-align: center;\n          align-items: center;\n          font-size: 18px; }\n          .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__avatar___1HWW- {\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-orient: horizontal;\n            -webkit-box-direction: reverse;\n            -ms-flex-direction: row-reverse;\n            flex-direction: row-reverse;\n            -webkit-box-align: end;\n            -ms-flex-align: end;\n            align-items: flex-end;\n            height: 1.4em; }\n            .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__avatar___1HWW- .botpress-platform-webchat__style__square___3VkAO {\n              -webkit-transform: translate(-10px, 12px);\n              transform: translate(-10px, 12px);\n              margin-right: 4px;\n              position: relative;\n              width: 70px;\n              height: 70px; }\n              .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__avatar___1HWW- .botpress-platform-webchat__style__square___3VkAO .botpress-platform-webchat__style__circle___aL8KF {\n                position: absolute;\n                top: 0;\n                right: 0;\n                bottom: 0;\n                left: 0;\n                width: auto;\n                height: auto;\n                border-width: 2px;\n                border-style: solid;\n                border-radius: 50%;\n                background-color: #f8f8f8;\n                -ms-flex-negative: 0;\n                flex-shrink: 0; }\n                .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__avatar___1HWW- .botpress-platform-webchat__style__square___3VkAO .botpress-platform-webchat__style__circle___aL8KF .botpress-platform-webchat__style__picture___2ccuR {\n                  border-radius: 50%;\n                  background-size: cover;\n                  background-position: 50%;\n                  width: 100%;\n                  height: 100%; }\n          .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__name___2LQ8g {\n            font-weight: 500; }\n            .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__line___3vtgj .botpress-platform-webchat__style__title___2Fr-Y .botpress-platform-webchat__style__name___2LQ8g span {\n              color: inherit;\n              opacity: .8; }\n      .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__text___12-Rd {\n        font-size: 14px;\n        font-weight: 400;\n        min-height: 50px;\n        max-height: 150px;\n        overflow-x: hidden;\n        overflow-y: auto;\n        word-break: break-word;\n        white-space: pre-line;\n        -webkit-overflow-scrolling: touch;\n        line-height: 30px;\n        margin-top: 4px;\n        margin-bottom: 0;\n        padding-bottom: 12px;\n        padding-left: 18px;\n        padding-right: 18px; }\n        .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__text___12-Rd .botpress-platform-webchat__style__paragraph___32PaH {\n          font-weight: 300;\n          font-size: 15px;\n          word-break: break-word;\n          white-space: pre-line; }\n          .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__header___364rr .botpress-platform-webchat__style__text___12-Rd .botpress-platform-webchat__style__paragraph___32PaH p {\n            margin: 0;\n            display: block; }\n    .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__composer___32VI5 {\n      position: relative;\n      padding: 8px;\n      border-bottom-left-radius: inherit;\n      border-bottom-right-radius: inherit; }\n      .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__composer___32VI5 .botpress-platform-webchat__style__flex-column___1yGN9 {\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n        flex-direction: column;\n        display: flex; }\n        .botpress-platform-webchat__style__external___2svzf .botpress-platform-webchat__style__internal___2EJRy .botpress-platform-webchat__style__composer___32VI5 .botpress-platform-webchat__style__flex-column___1yGN9 .botpress-platform-webchat__style__bottom___2tqon {\n          padding: 0 8px;\n          display: flex;\n          padding: 0 16px 12px;\n          justify-content: space-between;\n          -webkit-box-align: center; }\n\n.botpress-platform-webchat__style__fadeIn___17jV- {\n  -webkit-animation-name: botpress-platform-webchat__style__zoomIn___1zhjI;\n  animation-name: botpress-platform-webchat__style__zoomIn___1zhjI;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n@-webkit-keyframes botpress-platform-webchat__style__zoomIn___1zhjI {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n@keyframes botpress-platform-webchat__style__zoomIn___1zhjI {\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n.botpress-platform-webchat__style__fadeOut___3_apF {\n  -webkit-animation-name: botpress-platform-webchat__style__zoomOut___3vGfo;\n  animation-name: botpress-platform-webchat__style__zoomOut___3vGfo;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n@-webkit-keyframes botpress-platform-webchat__style__zoomOut___3vGfo {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  100% {\n    opacity: 0; } }\n\n@keyframes botpress-platform-webchat__style__zoomOut___3vGfo {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  100% {\n    opacity: 0; } }\n", ""]);
	
	// exports
	exports.locals = {
		"external": "botpress-platform-webchat__style__external___2svzf",
		"internal": "botpress-platform-webchat__style__internal___2EJRy",
		"header": "botpress-platform-webchat__style__header___364rr",
		"line": "botpress-platform-webchat__style__line___3vtgj",
		"title": "botpress-platform-webchat__style__title___2Fr-Y",
		"avatar": "botpress-platform-webchat__style__avatar___1HWW-",
		"square": "botpress-platform-webchat__style__square___3VkAO",
		"circle": "botpress-platform-webchat__style__circle___aL8KF",
		"picture": "botpress-platform-webchat__style__picture___2ccuR",
		"name": "botpress-platform-webchat__style__name___2LQ8g",
		"text": "botpress-platform-webchat__style__text___12-Rd",
		"paragraph": "botpress-platform-webchat__style__paragraph___32PaH",
		"composer": "botpress-platform-webchat__style__composer___32VI5",
		"flex-column": "botpress-platform-webchat__style__flex-column___1yGN9",
		"bottom": "botpress-platform-webchat__style__bottom___2tqon",
		"fadeIn": "botpress-platform-webchat__style__fadeIn___17jV-",
		"zoomIn": "botpress-platform-webchat__style__zoomIn___1zhjI",
		"fadeOut": "botpress-platform-webchat__style__fadeOut___3_apF",
		"zoomOut": "botpress-platform-webchat__style__zoomOut___3vGfo"
	};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactDom = __webpack_require__(9);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _distance_in_words_to_now = __webpack_require__(24);
	
	var _distance_in_words_to_now2 = _interopRequireDefault(_distance_in_words_to_now);
	
	var _send = __webpack_require__(10);
	
	var _send2 = _interopRequireDefault(_send);
	
	var _messages = __webpack_require__(36);
	
	var _messages2 = _interopRequireDefault(_messages);
	
	var _input = __webpack_require__(15);
	
	var _input2 = _interopRequireDefault(_input);
	
	var _bot_avatar = __webpack_require__(18);
	
	var _bot_avatar2 = _interopRequireDefault(_bot_avatar);
	
	var _style = __webpack_require__(95);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	// import { Picker } from 'emoji-mart'
	
	// require('emoji-mart/css/emoji-mart.css')
	
	var Side = function (_React$Component) {
	  _inherits(Side, _React$Component);
	
	  function Side(props) {
	    _classCallCheck(this, Side);
	
	    var _this = _possibleConstructorReturn(this, (Side.__proto__ || Object.getPrototypeOf(Side)).call(this, props));
	
	    _this.state = {
	      focused: false,
	      showEmoji: false,
	      showConvos: false
	    };
	    return _this;
	  }
	
	  _createClass(Side, [{
	    key: 'componentWillReceiveProps',
	    value: function componentWillReceiveProps(nextProps) {
	      if (!this.props.currentConversation && nextProps.currentConversation) {
	        this.setState({ showConvos: false });
	      }
	    }
	  }, {
	    key: 'handleFocus',
	    value: function handleFocus(value) {
	      this.setState({
	        focused: value
	      });
	    }
	  }, {
	    key: 'handleEmojiClicked',
	    value: function handleEmojiClicked() {
	      this.setState({
	        showEmoji: !this.state.showEmoji
	      });
	    }
	  }, {
	    key: 'handleToggleShowConvos',
	    value: function handleToggleShowConvos() {
	      this.setState({
	        showConvos: !this.state.showConvos
	      });
	    }
	  }, {
	    key: 'renderAvatar',
	    value: function renderAvatar() {
	      var content = _react2.default.createElement(_bot_avatar2.default, { foregroundColor: this.props.config.foregroundColor });
	
	      if (this.props.config && this.props.config.botAvatarUrl) {
	        content = _react2.default.createElement('div', {
	          className: _style2.default.picture,
	          style: { backgroundImage: 'url(' + this.props.config.botAvatarUrl + ')' } });
	      }
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.avatar, style: { color: this.props.config.foregroundColor } },
	        content
	      );
	    }
	  }, {
	    key: 'renderUnreadCount',
	    value: function renderUnreadCount() {
	      return _react2.default.createElement(
	        'span',
	        { className: _style2.default.unread },
	        this.props.unreadCount
	      );
	    }
	  }, {
	    key: 'renderTitle',
	    value: function renderTitle() {
	      var title = this.props.currentConversation && !this.state.showConvos ? this.props.config.botConvoTitle : 'Conversations';
	
	      var description = this.props.config.botConvoDescription;
	      var hasDescription = description && description.length > 0;
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.title },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.name },
	          title,
	          this.props.unreadCount > 0 ? this.renderUnreadCount() : null
	        ),
	        hasDescription && _react2.default.createElement(
	          'div',
	          { className: _style2.default.status },
	          description
	        )
	      );
	    }
	  }, {
	    key: 'renderConvoButton',
	    value: function renderConvoButton() {
	      return _react2.default.createElement(
	        'span',
	        { className: _style2.default.icon },
	        _react2.default.createElement(
	          'i',
	          { onClick: this.handleToggleShowConvos.bind(this) },
	          _react2.default.createElement(
	            'svg',
	            { width: '24', height: '17', viewBox: '0 0 24 17', xmlns: 'http://www.w3.org/2000/svg' },
	            _react2.default.createElement('path', { d: 'M7 14.94h16c.552 0 1 .346 1 .772 0 .427-.448.773-1 .773H7c-.552 0-1-.346-1-.773 0-.426.448-.773 1-.773zM2.25 3.09H.75C.336 3.09 0 2.746 0 2.32V.773C0 .346.336 0 .75 0h1.5c.414 0 .75.346.75.773v1.545c0 .427-.336.773-.75.773zM2.25 10.303H.75c-.414 0-.75-.346-.75-.773V7.985c0-.427.336-.773.75-.773h1.5c.414 0 .75.346.75.773V9.53c0 .427-.336.773-.75.773zM2.25 17H.75c-.414 0-.75-.346-.75-.773v-1.545c0-.427.336-.773.75-.773h1.5c.414 0 .75.345.75.772v1.545c0 .427-.336.773-.75.773zM23 2.06H7c-.552 0-1-.346-1-.772 0-.427.448-.773 1-.773h16c.552 0 1 .346 1 .773 0 .426-.448.773-1 .773zM23 9.273H7c-.552 0-1-.346-1-.773 0-.427.448-.773 1-.773h16c.552 0 1 .346 1 .773 0 .427-.448.773-1 .773z' })
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderCloseButton',
	    value: function renderCloseButton() {
	      if (!this.props.onClose) {
	        return null;
	      }
	
	      return _react2.default.createElement(
	        'span',
	        { className: _style2.default.icon },
	        _react2.default.createElement(
	          'i',
	          { onClick: this.props.onClose },
	          _react2.default.createElement(
	            'svg',
	            { width: '17', height: '17', viewBox: '0 0 17 17', xmlns: 'http://www.w3.org/2000/svg' },
	            _react2.default.createElement('path', { d: 'M9.502 8.5l7.29 7.29c.277.278.277.727 0 1.003-.137.138-.32.207-.5.207s-.362-.07-.5-.207L8.5 9.503l-7.29 7.29c-.14.138-.32.207-.5.207-.183 0-.364-.07-.502-.207-.277-.276-.277-.725 0-1.002l7.29-7.29-7.29-7.29C-.07.932-.07.483.208.206c.277-.276.725-.276 1 0L8.5 7.497l7.29-7.29c.277-.276.725-.276 1.002 0 .277.277.277.726 0 1.002L9.502 8.5z', 'fill-rule': 'evenodd' })
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderHeader',
	    value: function renderHeader() {
	      var status = _react2.default.createElement(
	        'div',
	        { className: _style2.default.status },
	        _react2.default.createElement(
	          'svg',
	          { viewBox: '0 0 10 10' },
	          _react2.default.createElement('ellipse', { cx: '50%', cy: '50%', rx: '50%', ry: '50%' })
	        ),
	        _react2.default.createElement(
	          'span',
	          null,
	          'always online'
	        )
	      );
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.header },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.left },
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.line },
	            this.renderAvatar(),
	            this.renderTitle()
	          )
	        ),
	        this.renderConvoButton(),
	        this.renderCloseButton()
	      );
	    }
	  }, {
	    key: 'renderAttachmentButton',
	    value: function renderAttachmentButton() {
	      return null; // Temporary removed this feature (not implemented yet)
	
	      return _react2.default.createElement(
	        'li',
	        null,
	        _react2.default.createElement(
	          'a',
	          null,
	          _react2.default.createElement(
	            'i',
	            null,
	            _react2.default.createElement(
	              'svg',
	              { width: '18', height: '17', viewBox: '0 0 18 17', xmlns: 'http://www.w3.org/2000/svg' },
	              _react2.default.createElement('path', { d: 'M8.455 16.5c-.19 0-.378-.076-.522-.226-.29-.303-.29-.792 0-1.093l7.66-8.013c.57-.597.885-1.392.885-2.236 0-.844-.315-1.638-.886-2.235-1.18-1.233-3.097-1.232-4.275 0L2.433 11.99c-.5.525-.742 1.03-.715 1.502.026.46.303.815.467.985.275.29.573.41.908.364.42-.054.903-.356 1.398-.874l6.973-7.295c.288-.3.755-.3 1.043 0 .29.303.29.793 0 1.093l-6.97 7.296c-.74.773-1.5 1.215-2.26 1.314-.797.104-1.535-.175-2.135-.804-.537-.562-.856-1.267-.896-1.985-.054-.933.332-1.836 1.144-2.686l8.885-9.297c1.754-1.836 4.61-1.836 6.363 0 .85.888 1.318 2.07 1.318 3.328s-.468 2.44-1.318 3.33l-7.66 8.014c-.143.15-.332.226-.52.226z', 'fill-rule': 'evenodd' })
	            )
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderEmojiButton',
	    value: function renderEmojiButton() {
	      return null; // Temporary removed this feature (emoji-mart lib is too big)
	
	      return _react2.default.createElement(
	        'li',
	        null,
	        _react2.default.createElement(
	          'a',
	          null,
	          _react2.default.createElement(
	            'i',
	            { onClick: this.handleEmojiClicked.bind(this) },
	            _react2.default.createElement(
	              'svg',
	              { preserveAspectRatio: 'xMidYMid', width: '18', height: '18', viewBox: '0 0 24 24' },
	              _react2.default.createElement('path', { d: 'M12 24C5.38 24 0 18.62 0 12S5.38 0 12 0s12 5.38 12 12-5.38 12-12 12zm0-22C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm0 18c-2.9 0-5.56-1.75-6.9-4.57-.24-.5-.03-1.1.47-1.33.5-.24 1.1-.03 1.33.47C7.9 16.67 9.86 18 12 18c2.15 0 4.1-1.3 5.1-3.43.23-.5.83-.7 1.33-.47.5.23.7.83.47 1.33C17.58 18.25 14.93 20 12 20zm4-8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm-8 0c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z' })
	            )
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderComposer',
	    value: function renderComposer() {
	      var name = this.props.config.botName || 'Bot';
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.composer,
	          style: {
	            borderColor: this.state.focused ? this.props.config.foregroundColor : null
	          } },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default['flex-column'] },
	          _react2.default.createElement(_input2.default, { placeholder: "Reply to " + name,
	            send: this.props.onTextSend,
	            change: this.props.onTextChanged,
	            text: this.props.text,
	            focused: this.handleFocus.bind(this),
	            config: this.props.config }),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.line },
	            _react2.default.createElement(
	              'ul',
	              { className: _style2.default.elements },
	              this.renderAttachmentButton(),
	              this.renderEmojiButton()
	            ),
	            _react2.default.createElement(_send2.default, {
	              send: this.props.onTextSend,
	              text: this.props.text,
	              config: this.props.config })
	          ),
	          this.renderEmojiPicker()
	        )
	      );
	    }
	  }, {
	    key: 'renderEmojiPicker',
	    value: function renderEmojiPicker() {
	      if (!this.state.showEmoji) {
	        return null;
	      }
	
	      return null; // Temporary removed this feature (emoji-mart is too big)
	
	      // return <div className={style.emoji}>
	      //     <div className={style.inside}>
	      //       <Picker
	      //         onClick={this.props.addEmojiToText} 
	      //         set='emojione'
	      //         emojiSize={18}
	      //         perLine={10}
	      //         color={this.props.config.foregroundColor}/>
	      //     </div>
	      //   </div>
	    }
	  }, {
	    key: 'renderConversation',
	    value: function renderConversation() {
	      var messagesProps = {
	        typingUntil: this.props.currentConversation && this.props.currentConversation.typingUntil,
	        messages: this.props.currentConversation && this.props.currentConversation.messages,
	        fgColor: this.props.config && this.props.config.foregroundColor,
	        textColor: this.props.config && this.props.config.textColorOnForeground,
	        avatarUrl: this.props.config && this.props.config.botAvatarUrl,
	        onQuickReplySend: this.props.onQuickReplySend,
	        onFormSend: this.props.onFormSend,
	        onFileUploadSend: this.props.onFileUploadSend,
	        onLoginPromptSend: this.props.onLoginPromptSend
	      };
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.conversation },
	        _react2.default.createElement(_messages2.default, messagesProps),
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.bottom },
	          this.renderComposer()
	        )
	      );
	    }
	  }, {
	    key: 'renderItemConvos',
	    value: function renderItemConvos(convo, key) {
	      var _this2 = this;
	
	      var title = convo.title || convo.message_author || 'Untitled Conversation';
	      var date = (0, _distance_in_words_to_now2.default)(new Date(convo.message_sent_on || convo.created_on));
	      var message = convo.message_text || '...';
	
	      var onClick = function onClick() {
	        _this2.props.onSwitchConvo && _this2.props.onSwitchConvo(convo.id);
	
	        _this2.setState({
	          showConvos: false
	        });
	      };
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.item, key: key, onClick: onClick },
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.left },
	          this.renderAvatar()
	        ),
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default.right },
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.title },
	            _react2.default.createElement(
	              'div',
	              { className: _style2.default.name },
	              title
	            ),
	            _react2.default.createElement(
	              'div',
	              { className: _style2.default.date },
	              _react2.default.createElement(
	                'span',
	                null,
	                date
	              )
	            )
	          ),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.text },
	            message
	          )
	        )
	      );
	    }
	  }, {
	    key: 'renderListOfConvos',
	    value: function renderListOfConvos() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.list },
	        this.props.conversations.map(this.renderItemConvos.bind(this))
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var fullscreen = this.props.fullscreen ? 'fullscreen' : null;
	      var classNames = (0, _classnames2.default)(_style2.default.internal, _style2.default[fullscreen], _style2.default[this.props.transition]);
	
	      return _react2.default.createElement(
	        'span',
	        { className: _style2.default.external },
	        _react2.default.createElement(
	          'div',
	          { className: classNames,
	            style: {
	              backgroundColor: this.props.config.backgroundColor,
	              color: this.props.config.textColorOnBackgound
	            } },
	          this.renderHeader(),
	          this.state.showConvos ? this.renderListOfConvos() : this.renderConversation()
	        )
	      );
	    }
	  }]);
	
	  return Side;
	}(_react2.default.Component);
	
	exports.default = Side;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	var distanceInWords = __webpack_require__(25)
	
	/**
	 * @category Common Helpers
	 * @summary Return the distance between the given date and now in words.
	 *
	 * @description
	 * Return the distance between the given date and now in words.
	 *
	 * | Distance to now                                                   | Result              |
	 * |-------------------------------------------------------------------|---------------------|
	 * | 0 ... 30 secs                                                     | less than a minute  |
	 * | 30 secs ... 1 min 30 secs                                         | 1 minute            |
	 * | 1 min 30 secs ... 44 mins 30 secs                                 | [2..44] minutes     |
	 * | 44 mins ... 30 secs ... 89 mins 30 secs                           | about 1 hour        |
	 * | 89 mins 30 secs ... 23 hrs 59 mins 30 secs                        | about [2..24] hours |
	 * | 23 hrs 59 mins 30 secs ... 41 hrs 59 mins 30 secs                 | 1 day               |
	 * | 41 hrs 59 mins 30 secs ... 29 days 23 hrs 59 mins 30 secs         | [2..30] days        |
	 * | 29 days 23 hrs 59 mins 30 secs ... 44 days 23 hrs 59 mins 30 secs | about 1 month       |
	 * | 44 days 23 hrs 59 mins 30 secs ... 59 days 23 hrs 59 mins 30 secs | about 2 months      |
	 * | 59 days 23 hrs 59 mins 30 secs ... 1 yr                           | [2..12] months      |
	 * | 1 yr ... 1 yr 3 months                                            | about 1 year        |
	 * | 1 yr 3 months ... 1 yr 9 month s                                  | over 1 year         |
	 * | 1 yr 9 months ... 2 yrs                                           | almost 2 years      |
	 * | N yrs ... N yrs 3 months                                          | about N years       |
	 * | N yrs 3 months ... N yrs 9 months                                 | over N years        |
	 * | N yrs 9 months ... N+1 yrs                                        | almost N+1 years    |
	 *
	 * With `options.includeSeconds == true`:
	 * | Distance to now     | Result               |
	 * |---------------------|----------------------|
	 * | 0 secs ... 5 secs   | less than 5 seconds  |
	 * | 5 secs ... 10 secs  | less than 10 seconds |
	 * | 10 secs ... 20 secs | less than 20 seconds |
	 * | 20 secs ... 40 secs | half a minute        |
	 * | 40 secs ... 60 secs | less than a minute   |
	 * | 60 secs ... 90 secs | 1 minute             |
	 *
	 * @param {Date|String|Number} date - the given date
	 * @param {Object} [options] - the object with options
	 * @param {Boolean} [options.includeSeconds=false] - distances less than a minute are more detailed
	 * @param {Boolean} [options.addSuffix=false] - result specifies if the second date is earlier or later than the first
	 * @param {Object} [options.locale=enLocale] - the locale object
	 * @returns {String} the distance in words
	 *
	 * @example
	 * // If today is 1 January 2015, what is the distance to 2 July 2014?
	 * var result = distanceInWordsToNow(
	 *   new Date(2014, 6, 2)
	 * )
	 * //=> '6 months'
	 *
	 * @example
	 * // If now is 1 January 2015 00:00:00,
	 * // what is the distance to 1 January 2015 00:00:15, including seconds?
	 * var result = distanceInWordsToNow(
	 *   new Date(2015, 0, 1, 0, 0, 15),
	 *   {includeSeconds: true}
	 * )
	 * //=> 'less than 20 seconds'
	 *
	 * @example
	 * // If today is 1 January 2015,
	 * // what is the distance to 1 January 2016, with a suffix?
	 * var result = distanceInWordsToNow(
	 *   new Date(2016, 0, 1),
	 *   {addSuffix: true}
	 * )
	 * //=> 'in about 1 year'
	 *
	 * @example
	 * // If today is 1 January 2015,
	 * // what is the distance to 1 August 2016 in Esperanto?
	 * var eoLocale = require('date-fns/locale/eo')
	 * var result = distanceInWordsToNow(
	 *   new Date(2016, 7, 1),
	 *   {locale: eoLocale}
	 * )
	 * //=> 'pli ol 1 jaro'
	 */
	function distanceInWordsToNow (dirtyDate, dirtyOptions) {
	  return distanceInWords(Date.now(), dirtyDate, dirtyOptions)
	}
	
	module.exports = distanceInWordsToNow


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	var compareDesc = __webpack_require__(26)
	var parse = __webpack_require__(5)
	var differenceInSeconds = __webpack_require__(27)
	var differenceInMonths = __webpack_require__(29)
	var enLocale = __webpack_require__(32)
	
	var MINUTES_IN_DAY = 1440
	var MINUTES_IN_ALMOST_TWO_DAYS = 2520
	var MINUTES_IN_MONTH = 43200
	var MINUTES_IN_TWO_MONTHS = 86400
	
	/**
	 * @category Common Helpers
	 * @summary Return the distance between the given dates in words.
	 *
	 * @description
	 * Return the distance between the given dates in words.
	 *
	 * | Distance between dates                                            | Result              |
	 * |-------------------------------------------------------------------|---------------------|
	 * | 0 ... 30 secs                                                     | less than a minute  |
	 * | 30 secs ... 1 min 30 secs                                         | 1 minute            |
	 * | 1 min 30 secs ... 44 mins 30 secs                                 | [2..44] minutes     |
	 * | 44 mins ... 30 secs ... 89 mins 30 secs                           | about 1 hour        |
	 * | 89 mins 30 secs ... 23 hrs 59 mins 30 secs                        | about [2..24] hours |
	 * | 23 hrs 59 mins 30 secs ... 41 hrs 59 mins 30 secs                 | 1 day               |
	 * | 41 hrs 59 mins 30 secs ... 29 days 23 hrs 59 mins 30 secs         | [2..30] days        |
	 * | 29 days 23 hrs 59 mins 30 secs ... 44 days 23 hrs 59 mins 30 secs | about 1 month       |
	 * | 44 days 23 hrs 59 mins 30 secs ... 59 days 23 hrs 59 mins 30 secs | about 2 months      |
	 * | 59 days 23 hrs 59 mins 30 secs ... 1 yr                           | [2..12] months      |
	 * | 1 yr ... 1 yr 3 months                                            | about 1 year        |
	 * | 1 yr 3 months ... 1 yr 9 month s                                  | over 1 year         |
	 * | 1 yr 9 months ... 2 yrs                                           | almost 2 years      |
	 * | N yrs ... N yrs 3 months                                          | about N years       |
	 * | N yrs 3 months ... N yrs 9 months                                 | over N years        |
	 * | N yrs 9 months ... N+1 yrs                                        | almost N+1 years    |
	 *
	 * With `options.includeSeconds == true`:
	 * | Distance between dates | Result               |
	 * |------------------------|----------------------|
	 * | 0 secs ... 5 secs      | less than 5 seconds  |
	 * | 5 secs ... 10 secs     | less than 10 seconds |
	 * | 10 secs ... 20 secs    | less than 20 seconds |
	 * | 20 secs ... 40 secs    | half a minute        |
	 * | 40 secs ... 60 secs    | less than a minute   |
	 * | 60 secs ... 90 secs    | 1 minute             |
	 *
	 * @param {Date|String|Number} dateToCompare - the date to compare with
	 * @param {Date|String|Number} date - the other date
	 * @param {Object} [options] - the object with options
	 * @param {Boolean} [options.includeSeconds=false] - distances less than a minute are more detailed
	 * @param {Boolean} [options.addSuffix=false] - result indicates if the second date is earlier or later than the first
	 * @param {Object} [options.locale=enLocale] - the locale object
	 * @returns {String} the distance in words
	 *
	 * @example
	 * // What is the distance between 2 July 2014 and 1 January 2015?
	 * var result = distanceInWords(
	 *   new Date(2014, 6, 2),
	 *   new Date(2015, 0, 1)
	 * )
	 * //=> '6 months'
	 *
	 * @example
	 * // What is the distance between 1 January 2015 00:00:15
	 * // and 1 January 2015 00:00:00, including seconds?
	 * var result = distanceInWords(
	 *   new Date(2015, 0, 1, 0, 0, 15),
	 *   new Date(2015, 0, 1, 0, 0, 0),
	 *   {includeSeconds: true}
	 * )
	 * //=> 'less than 20 seconds'
	 *
	 * @example
	 * // What is the distance from 1 January 2016
	 * // to 1 January 2015, with a suffix?
	 * var result = distanceInWords(
	 *   new Date(2016, 0, 1),
	 *   new Date(2015, 0, 1),
	 *   {addSuffix: true}
	 * )
	 * //=> 'about 1 year ago'
	 *
	 * @example
	 * // What is the distance between 1 August 2016 and 1 January 2015 in Esperanto?
	 * var eoLocale = require('date-fns/locale/eo')
	 * var result = distanceInWords(
	 *   new Date(2016, 7, 1),
	 *   new Date(2015, 0, 1),
	 *   {locale: eoLocale}
	 * )
	 * //=> 'pli ol 1 jaro'
	 */
	function distanceInWords (dirtyDateToCompare, dirtyDate, dirtyOptions) {
	  var options = dirtyOptions || {}
	
	  var comparison = compareDesc(dirtyDateToCompare, dirtyDate)
	
	  var locale = options.locale
	  var localize = enLocale.distanceInWords.localize
	  if (locale && locale.distanceInWords && locale.distanceInWords.localize) {
	    localize = locale.distanceInWords.localize
	  }
	
	  var localizeOptions = {
	    addSuffix: Boolean(options.addSuffix),
	    comparison: comparison
	  }
	
	  var dateLeft, dateRight
	  if (comparison > 0) {
	    dateLeft = parse(dirtyDateToCompare)
	    dateRight = parse(dirtyDate)
	  } else {
	    dateLeft = parse(dirtyDate)
	    dateRight = parse(dirtyDateToCompare)
	  }
	
	  var seconds = differenceInSeconds(dateRight, dateLeft)
	  var offset = dateRight.getTimezoneOffset() - dateLeft.getTimezoneOffset()
	  var minutes = Math.round(seconds / 60) - offset
	  var months
	
	  // 0 up to 2 mins
	  if (minutes < 2) {
	    if (options.includeSeconds) {
	      if (seconds < 5) {
	        return localize('lessThanXSeconds', 5, localizeOptions)
	      } else if (seconds < 10) {
	        return localize('lessThanXSeconds', 10, localizeOptions)
	      } else if (seconds < 20) {
	        return localize('lessThanXSeconds', 20, localizeOptions)
	      } else if (seconds < 40) {
	        return localize('halfAMinute', null, localizeOptions)
	      } else if (seconds < 60) {
	        return localize('lessThanXMinutes', 1, localizeOptions)
	      } else {
	        return localize('xMinutes', 1, localizeOptions)
	      }
	    } else {
	      if (minutes === 0) {
	        return localize('lessThanXMinutes', 1, localizeOptions)
	      } else {
	        return localize('xMinutes', minutes, localizeOptions)
	      }
	    }
	
	  // 2 mins up to 0.75 hrs
	  } else if (minutes < 45) {
	    return localize('xMinutes', minutes, localizeOptions)
	
	  // 0.75 hrs up to 1.5 hrs
	  } else if (minutes < 90) {
	    return localize('aboutXHours', 1, localizeOptions)
	
	  // 1.5 hrs up to 24 hrs
	  } else if (minutes < MINUTES_IN_DAY) {
	    var hours = Math.round(minutes / 60)
	    return localize('aboutXHours', hours, localizeOptions)
	
	  // 1 day up to 1.75 days
	  } else if (minutes < MINUTES_IN_ALMOST_TWO_DAYS) {
	    return localize('xDays', 1, localizeOptions)
	
	  // 1.75 days up to 30 days
	  } else if (minutes < MINUTES_IN_MONTH) {
	    var days = Math.round(minutes / MINUTES_IN_DAY)
	    return localize('xDays', days, localizeOptions)
	
	  // 1 month up to 2 months
	  } else if (minutes < MINUTES_IN_TWO_MONTHS) {
	    months = Math.round(minutes / MINUTES_IN_MONTH)
	    return localize('aboutXMonths', months, localizeOptions)
	  }
	
	  months = differenceInMonths(dateRight, dateLeft)
	
	  // 2 months up to 12 months
	  if (months < 12) {
	    var nearestMonth = Math.round(minutes / MINUTES_IN_MONTH)
	    return localize('xMonths', nearestMonth, localizeOptions)
	
	  // 1 year up to max Date
	  } else {
	    var monthsSinceStartOfYear = months % 12
	    var years = Math.floor(months / 12)
	
	    // N years up to 1 years 3 months
	    if (monthsSinceStartOfYear < 3) {
	      return localize('aboutXYears', years, localizeOptions)
	
	    // N years 3 months up to N years 9 months
	    } else if (monthsSinceStartOfYear < 9) {
	      return localize('overXYears', years, localizeOptions)
	
	    // N years 9 months up to N year 12 months
	    } else {
	      return localize('almostXYears', years + 1, localizeOptions)
	    }
	  }
	}
	
	module.exports = distanceInWords


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Common Helpers
	 * @summary Compare the two dates reverse chronologically and return -1, 0 or 1.
	 *
	 * @description
	 * Compare the two dates and return -1 if the first date is after the second,
	 * 1 if the first date is before the second or 0 if dates are equal.
	 *
	 * @param {Date|String|Number} dateLeft - the first date to compare
	 * @param {Date|String|Number} dateRight - the second date to compare
	 * @returns {Number} the result of the comparison
	 *
	 * @example
	 * // Compare 11 February 1987 and 10 July 1989 reverse chronologically:
	 * var result = compareDesc(
	 *   new Date(1987, 1, 11),
	 *   new Date(1989, 6, 10)
	 * )
	 * //=> 1
	 *
	 * @example
	 * // Sort the array of dates in reverse chronological order:
	 * var result = [
	 *   new Date(1995, 6, 2),
	 *   new Date(1987, 1, 11),
	 *   new Date(1989, 6, 10)
	 * ].sort(compareDesc)
	 * //=> [
	 * //   Sun Jul 02 1995 00:00:00,
	 * //   Mon Jul 10 1989 00:00:00,
	 * //   Wed Feb 11 1987 00:00:00
	 * // ]
	 */
	function compareDesc (dirtyDateLeft, dirtyDateRight) {
	  var dateLeft = parse(dirtyDateLeft)
	  var timeLeft = dateLeft.getTime()
	  var dateRight = parse(dirtyDateRight)
	  var timeRight = dateRight.getTime()
	
	  if (timeLeft > timeRight) {
	    return -1
	  } else if (timeLeft < timeRight) {
	    return 1
	  } else {
	    return 0
	  }
	}
	
	module.exports = compareDesc


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	var differenceInMilliseconds = __webpack_require__(28)
	
	/**
	 * @category Second Helpers
	 * @summary Get the number of seconds between the given dates.
	 *
	 * @description
	 * Get the number of seconds between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of seconds
	 *
	 * @example
	 * // How many seconds are between
	 * // 2 July 2014 12:30:07.999 and 2 July 2014 12:30:20.000?
	 * var result = differenceInSeconds(
	 *   new Date(2014, 6, 2, 12, 30, 20, 0),
	 *   new Date(2014, 6, 2, 12, 30, 7, 999)
	 * )
	 * //=> 12
	 */
	function differenceInSeconds (dirtyDateLeft, dirtyDateRight) {
	  var diff = differenceInMilliseconds(dirtyDateLeft, dirtyDateRight) / 1000
	  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
	}
	
	module.exports = differenceInSeconds


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Millisecond Helpers
	 * @summary Get the number of milliseconds between the given dates.
	 *
	 * @description
	 * Get the number of milliseconds between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of milliseconds
	 *
	 * @example
	 * // How many milliseconds are between
	 * // 2 July 2014 12:30:20.600 and 2 July 2014 12:30:21.700?
	 * var result = differenceInMilliseconds(
	 *   new Date(2014, 6, 2, 12, 30, 21, 700),
	 *   new Date(2014, 6, 2, 12, 30, 20, 600)
	 * )
	 * //=> 1100
	 */
	function differenceInMilliseconds (dirtyDateLeft, dirtyDateRight) {
	  var dateLeft = parse(dirtyDateLeft)
	  var dateRight = parse(dirtyDateRight)
	  return dateLeft.getTime() - dateRight.getTime()
	}
	
	module.exports = differenceInMilliseconds


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	var differenceInCalendarMonths = __webpack_require__(30)
	var compareAsc = __webpack_require__(31)
	
	/**
	 * @category Month Helpers
	 * @summary Get the number of full months between the given dates.
	 *
	 * @description
	 * Get the number of full months between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of full months
	 *
	 * @example
	 * // How many full months are between 31 January 2014 and 1 September 2014?
	 * var result = differenceInMonths(
	 *   new Date(2014, 8, 1),
	 *   new Date(2014, 0, 31)
	 * )
	 * //=> 7
	 */
	function differenceInMonths (dirtyDateLeft, dirtyDateRight) {
	  var dateLeft = parse(dirtyDateLeft)
	  var dateRight = parse(dirtyDateRight)
	
	  var sign = compareAsc(dateLeft, dateRight)
	  var difference = Math.abs(differenceInCalendarMonths(dateLeft, dateRight))
	  dateLeft.setMonth(dateLeft.getMonth() - sign * difference)
	
	  // Math.abs(diff in full months - diff in calendar months) === 1 if last calendar month is not full
	  // If so, result must be decreased by 1 in absolute value
	  var isLastMonthNotFull = compareAsc(dateLeft, dateRight) === -sign
	  return sign * (difference - isLastMonthNotFull)
	}
	
	module.exports = differenceInMonths


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Month Helpers
	 * @summary Get the number of calendar months between the given dates.
	 *
	 * @description
	 * Get the number of calendar months between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of calendar months
	 *
	 * @example
	 * // How many calendar months are between 31 January 2014 and 1 September 2014?
	 * var result = differenceInCalendarMonths(
	 *   new Date(2014, 8, 1),
	 *   new Date(2014, 0, 31)
	 * )
	 * //=> 8
	 */
	function differenceInCalendarMonths (dirtyDateLeft, dirtyDateRight) {
	  var dateLeft = parse(dirtyDateLeft)
	  var dateRight = parse(dirtyDateRight)
	
	  var yearDiff = dateLeft.getFullYear() - dateRight.getFullYear()
	  var monthDiff = dateLeft.getMonth() - dateRight.getMonth()
	
	  return yearDiff * 12 + monthDiff
	}
	
	module.exports = differenceInCalendarMonths


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Common Helpers
	 * @summary Compare the two dates and return -1, 0 or 1.
	 *
	 * @description
	 * Compare the two dates and return 1 if the first date is after the second,
	 * -1 if the first date is before the second or 0 if dates are equal.
	 *
	 * @param {Date|String|Number} dateLeft - the first date to compare
	 * @param {Date|String|Number} dateRight - the second date to compare
	 * @returns {Number} the result of the comparison
	 *
	 * @example
	 * // Compare 11 February 1987 and 10 July 1989:
	 * var result = compareAsc(
	 *   new Date(1987, 1, 11),
	 *   new Date(1989, 6, 10)
	 * )
	 * //=> -1
	 *
	 * @example
	 * // Sort the array of dates:
	 * var result = [
	 *   new Date(1995, 6, 2),
	 *   new Date(1987, 1, 11),
	 *   new Date(1989, 6, 10)
	 * ].sort(compareAsc)
	 * //=> [
	 * //   Wed Feb 11 1987 00:00:00,
	 * //   Mon Jul 10 1989 00:00:00,
	 * //   Sun Jul 02 1995 00:00:00
	 * // ]
	 */
	function compareAsc (dirtyDateLeft, dirtyDateRight) {
	  var dateLeft = parse(dirtyDateLeft)
	  var timeLeft = dateLeft.getTime()
	  var dateRight = parse(dirtyDateRight)
	  var timeRight = dateRight.getTime()
	
	  if (timeLeft < timeRight) {
	    return -1
	  } else if (timeLeft > timeRight) {
	    return 1
	  } else {
	    return 0
	  }
	}
	
	module.exports = compareAsc


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	var buildDistanceInWordsLocale = __webpack_require__(33)
	var buildFormatLocale = __webpack_require__(34)
	
	/**
	 * @category Locales
	 * @summary English locale.
	 */
	module.exports = {
	  distanceInWords: buildDistanceInWordsLocale(),
	  format: buildFormatLocale()
	}


/***/ }),
/* 33 */
/***/ (function(module, exports) {

	function buildDistanceInWordsLocale () {
	  var distanceInWordsLocale = {
	    lessThanXSeconds: {
	      one: 'less than a second',
	      other: 'less than {{count}} seconds'
	    },
	
	    xSeconds: {
	      one: '1 second',
	      other: '{{count}} seconds'
	    },
	
	    halfAMinute: 'half a minute',
	
	    lessThanXMinutes: {
	      one: 'less than a minute',
	      other: 'less than {{count}} minutes'
	    },
	
	    xMinutes: {
	      one: '1 minute',
	      other: '{{count}} minutes'
	    },
	
	    aboutXHours: {
	      one: 'about 1 hour',
	      other: 'about {{count}} hours'
	    },
	
	    xHours: {
	      one: '1 hour',
	      other: '{{count}} hours'
	    },
	
	    xDays: {
	      one: '1 day',
	      other: '{{count}} days'
	    },
	
	    aboutXMonths: {
	      one: 'about 1 month',
	      other: 'about {{count}} months'
	    },
	
	    xMonths: {
	      one: '1 month',
	      other: '{{count}} months'
	    },
	
	    aboutXYears: {
	      one: 'about 1 year',
	      other: 'about {{count}} years'
	    },
	
	    xYears: {
	      one: '1 year',
	      other: '{{count}} years'
	    },
	
	    overXYears: {
	      one: 'over 1 year',
	      other: 'over {{count}} years'
	    },
	
	    almostXYears: {
	      one: 'almost 1 year',
	      other: 'almost {{count}} years'
	    }
	  }
	
	  function localize (token, count, options) {
	    options = options || {}
	
	    var result
	    if (typeof distanceInWordsLocale[token] === 'string') {
	      result = distanceInWordsLocale[token]
	    } else if (count === 1) {
	      result = distanceInWordsLocale[token].one
	    } else {
	      result = distanceInWordsLocale[token].other.replace('{{count}}', count)
	    }
	
	    if (options.addSuffix) {
	      if (options.comparison > 0) {
	        return 'in ' + result
	      } else {
	        return result + ' ago'
	      }
	    }
	
	    return result
	  }
	
	  return {
	    localize: localize
	  }
	}
	
	module.exports = buildDistanceInWordsLocale


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

	var buildFormattingTokensRegExp = __webpack_require__(35)
	
	function buildFormatLocale () {
	  // Note: in English, the names of days of the week and months are capitalized.
	  // If you are making a new locale based on this one, check if the same is true for the language you're working on.
	  // Generally, formatted dates should look like they are in the middle of a sentence,
	  // e.g. in Spanish language the weekdays and months should be in the lowercase.
	  var months3char = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	  var monthsFull = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	  var weekdays2char = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
	  var weekdays3char = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
	  var weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
	  var meridiemUppercase = ['AM', 'PM']
	  var meridiemLowercase = ['am', 'pm']
	  var meridiemFull = ['a.m.', 'p.m.']
	
	  var formatters = {
	    // Month: Jan, Feb, ..., Dec
	    'MMM': function (date) {
	      return months3char[date.getMonth()]
	    },
	
	    // Month: January, February, ..., December
	    'MMMM': function (date) {
	      return monthsFull[date.getMonth()]
	    },
	
	    // Day of week: Su, Mo, ..., Sa
	    'dd': function (date) {
	      return weekdays2char[date.getDay()]
	    },
	
	    // Day of week: Sun, Mon, ..., Sat
	    'ddd': function (date) {
	      return weekdays3char[date.getDay()]
	    },
	
	    // Day of week: Sunday, Monday, ..., Saturday
	    'dddd': function (date) {
	      return weekdaysFull[date.getDay()]
	    },
	
	    // AM, PM
	    'A': function (date) {
	      return (date.getHours() / 12) >= 1 ? meridiemUppercase[1] : meridiemUppercase[0]
	    },
	
	    // am, pm
	    'a': function (date) {
	      return (date.getHours() / 12) >= 1 ? meridiemLowercase[1] : meridiemLowercase[0]
	    },
	
	    // a.m., p.m.
	    'aa': function (date) {
	      return (date.getHours() / 12) >= 1 ? meridiemFull[1] : meridiemFull[0]
	    }
	  }
	
	  // Generate ordinal version of formatters: M -> Mo, D -> Do, etc.
	  var ordinalFormatters = ['M', 'D', 'DDD', 'd', 'Q', 'W']
	  ordinalFormatters.forEach(function (formatterToken) {
	    formatters[formatterToken + 'o'] = function (date, formatters) {
	      return ordinal(formatters[formatterToken](date))
	    }
	  })
	
	  return {
	    formatters: formatters,
	    formattingTokensRegExp: buildFormattingTokensRegExp(formatters)
	  }
	}
	
	function ordinal (number) {
	  var rem100 = number % 100
	  if (rem100 > 20 || rem100 < 10) {
	    switch (rem100 % 10) {
	      case 1:
	        return number + 'st'
	      case 2:
	        return number + 'nd'
	      case 3:
	        return number + 'rd'
	    }
	  }
	  return number + 'th'
	}
	
	module.exports = buildFormatLocale


/***/ }),
/* 35 */
/***/ (function(module, exports) {

	var commonFormatterKeys = [
	  'M', 'MM', 'Q', 'D', 'DD', 'DDD', 'DDDD', 'd',
	  'E', 'W', 'WW', 'YY', 'YYYY', 'GG', 'GGGG',
	  'H', 'HH', 'h', 'hh', 'm', 'mm',
	  's', 'ss', 'S', 'SS', 'SSS',
	  'Z', 'ZZ', 'X', 'x'
	]
	
	function buildFormattingTokensRegExp (formatters) {
	  var formatterKeys = []
	  for (var key in formatters) {
	    if (formatters.hasOwnProperty(key)) {
	      formatterKeys.push(key)
	    }
	  }
	
	  var formattingTokens = commonFormatterKeys
	    .concat(formatterKeys)
	    .sort()
	    .reverse()
	  var formattingTokensRegExp = new RegExp(
	    '(\\[[^\\[]*\\])|(\\\\)?' + '(' + formattingTokens.join('|') + '|.)', 'g'
	  )
	
	  return formattingTokensRegExp
	}
	
	module.exports = buildFormattingTokensRegExp


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _classnames2 = __webpack_require__(3);
	
	var _classnames3 = _interopRequireDefault(_classnames2);
	
	var _format = __webpack_require__(37);
	
	var _format2 = _interopRequireDefault(_format);
	
	var _difference_in_minutes = __webpack_require__(48);
	
	var _difference_in_minutes2 = _interopRequireDefault(_difference_in_minutes);
	
	var _bot_avatar = __webpack_require__(18);
	
	var _bot_avatar2 = _interopRequireDefault(_bot_avatar);
	
	var _quick_replies = __webpack_require__(49);
	
	var _quick_replies2 = _interopRequireDefault(_quick_replies);
	
	var _login_prompt = __webpack_require__(54);
	
	var _login_prompt2 = _interopRequireDefault(_login_prompt);
	
	var _file = __webpack_require__(55);
	
	var _file2 = _interopRequireDefault(_file);
	
	var _carousel = __webpack_require__(56);
	
	var _carousel2 = _interopRequireDefault(_carousel);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	var _form = __webpack_require__(94);
	
	var _form2 = _interopRequireDefault(_form);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var TIME_BETWEEN_DATES = 10; // 10 minutes
	
	var MessageGroup = function (_Component) {
	  _inherits(MessageGroup, _Component);
	
	  function MessageGroup() {
	    _classCallCheck(this, MessageGroup);
	
	    return _possibleConstructorReturn(this, (MessageGroup.__proto__ || Object.getPrototypeOf(MessageGroup)).apply(this, arguments));
	  }
	
	  _createClass(MessageGroup, [{
	    key: 'renderAvatar',
	    value: function renderAvatar() {
	      var content = _react2.default.createElement(_bot_avatar2.default, { foregroundColor: this.props.fgColor });
	
	      if (this.props.avatarUrl) {
	        content = _react2.default.createElement('div', { className: _style2.default.picture, style: { backgroundImage: 'url(' + this.props.avatarUrl + ')' } });
	      }
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.avatar, style: { color: this.props.fgColor } },
	        content
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;
	
	      var sample = this.props.messages[0];
	      var isBot = !sample.userId;
	
	      var className = (0, _classnames3.default)(_style2.default.message, _defineProperty({}, _style2.default.user, !isBot));
	
	      var bubbleColor = this.props.fgColor;
	      var textColor = this.props.textColor;
	
	      return _react2.default.createElement(
	        'div',
	        { className: className },
	        isBot && this.renderAvatar(),
	        _react2.default.createElement(
	          'div',
	          { className: _style2.default['message-container'] },
	          isBot && _react2.default.createElement(
	            'div',
	            { className: _style2.default['info-line'] },
	            sample.full_name
	          ),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.group },
	            this.props.messages.map(function (data, i) {
	              return _react2.default.createElement(Message, {
	                onLoginPromptSend: _this2.props.onLoginPromptSend,
	                textColor: textColor,
	                bubbleColor: bubbleColor,
	                key: 'msg-' + i,
	                isLastOfGroup: i >= _this2.props.messages.length - 1,
	                isLastGroup: _this2.props.isLastGroup,
	                data: data
	              });
	            })
	          )
	        )
	      );
	    }
	  }]);
	
	  return MessageGroup;
	}(_react.Component);
	
	var MessageList = function (_Component2) {
	  _inherits(MessageList, _Component2);
	
	  function MessageList(props) {
	    _classCallCheck(this, MessageList);
	
	    var _this3 = _possibleConstructorReturn(this, (MessageList.__proto__ || Object.getPrototypeOf(MessageList)).call(this, props));
	
	    _this3.messagesDiv = null;
	    return _this3;
	  }
	
	  _createClass(MessageList, [{
	    key: 'componentDidUpdate',
	    value: function componentDidUpdate(prevProps, prevState) {
	      this.tryScrollToBottom();
	    }
	  }, {
	    key: 'tryScrollToBottom',
	    value: function tryScrollToBottom() {
	      try {
	        this.messagesDiv.scrollTop = this.messagesDiv.scrollHeight;
	      } catch (err) {
	        // Discard the error
	      }
	    }
	  }, {
	    key: 'renderQuickReplies',
	    value: function renderQuickReplies() {
	      var messages = this.props.messages || [];
	      var message = messages[messages.length - 1];
	      var quick_replies = message && message['message_raw'] && message['message_raw']['quick_replies'];
	
	      return _react2.default.createElement(_quick_replies2.default, {
	        quick_replies: quick_replies,
	        fgColor: this.props.fgColor,
	        onQuickReplySend: this.props.onQuickReplySend,
	        onFileUploadSend: this.props.onFileUploadSend
	      });
	    }
	  }, {
	    key: 'renderForm',
	    value: function renderForm() {
	      var messages = this.props.messages || [];
	      var message = messages[messages.length - 1];
	      if (message && message['message_raw'] && message['message_raw']['form']) {
	        var form = message['message_raw']['form'];
	        return _react2.default.createElement(_form2.default, { elements: form.elements, formId: form.id, title: form.title, onFormSend: this.props.onFormSend });
	      }
	    }
	  }, {
	    key: 'renderDate',
	    value: function renderDate(date) {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.date },
	        (0, _format2.default)(new Date(date), 'MMMM Do YYYY, h:mm a'),
	        _react2.default.createElement('div', { className: _style2.default.smallLine })
	      );
	    }
	  }, {
	    key: 'renderMessageGroups',
	    value: function renderMessageGroups() {
	      var _this4 = this;
	
	      var messages = this.props.messages || [];
	      var groups = [];
	
	      var lastSpeaker = null;
	      var lastDate = null;
	      var currentGroup = null;
	
	      messages.forEach(function (m) {
	        var speaker = !!m.userId ? m.userId : 'bot';
	        var date = m.sent_on;
	
	        // Create a new group if messages are separated by more than X minutes or if different speaker
	        if (speaker !== lastSpeaker || (0, _difference_in_minutes2.default)(new Date(date), new Date(lastDate)) >= TIME_BETWEEN_DATES) {
	          currentGroup = [];
	          groups.push(currentGroup);
	        }
	
	        currentGroup.push(m);
	
	        lastSpeaker = speaker;
	        lastDate = date;
	      });
	
	      if (this.props.typingUntil) {
	        if (lastSpeaker !== 'bot') {
	          currentGroup = [];
	          groups.push(currentGroup);
	        }
	
	        currentGroup.push({
	          sent_on: new Date(),
	          userId: null,
	          message_type: 'typing'
	        });
	      }
	
	      return _react2.default.createElement(
	        'div',
	        null,
	        groups.map(function (group, i) {
	          var lastGroup = groups[i - 1];
	          var lastDate = lastGroup && lastGroup[lastGroup.length - 1] && lastGroup[lastGroup.length - 1].sent_on;
	          var groupDate = group && group[0].sent_on;
	
	          var isDateNeeded = !groups[i - 1] || (0, _difference_in_minutes2.default)(new Date(groupDate), new Date(lastDate)) > TIME_BETWEEN_DATES;
	
	          return _react2.default.createElement(
	            'div',
	            null,
	            isDateNeeded ? _this4.renderDate(group[0].sent_on) : null,
	            _react2.default.createElement(MessageGroup, {
	              avatarUrl: _this4.props.avatarUrl,
	              fgColor: _this4.props.fgColor,
	              textColor: _this4.props.textColor,
	              key: 'msg-group-' + i,
	              onLoginPromptSend: _this4.props.onLoginPromptSend,
	              isLastGroup: i >= groups.length - 1,
	              messages: group
	            })
	          );
	        })
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this5 = this;
	
	      return _react2.default.createElement(
	        'div',
	        {
	          className: _style2.default.messages,
	          ref: function ref(m) {
	            _this5.messagesDiv = m;
	          }
	        },
	        this.renderMessageGroups(),
	        this.renderForm(),
	        this.renderQuickReplies()
	      );
	    }
	  }]);
	
	  return MessageList;
	}(_react.Component);
	
	exports.default = MessageList;
	
	var Message = function (_Component3) {
	  _inherits(Message, _Component3);
	
	  function Message() {
	    _classCallCheck(this, Message);
	
	    return _possibleConstructorReturn(this, (Message.__proto__ || Object.getPrototypeOf(Message)).apply(this, arguments));
	  }
	
	  _createClass(Message, [{
	    key: 'getAddStyle',
	    value: function getAddStyle() {
	      return this.props.data.message_raw && this.props.data.message_raw['web-style'];
	    }
	  }, {
	    key: 'render_text',
	    value: function render_text() {
	      return _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'p',
	          { style: this.getAddStyle() },
	          this.props.data.message_text
	        )
	      );
	    }
	  }, {
	    key: 'render_form',
	    value: function render_form() {
	      return _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'p',
	          { style: this.getAddStyle() },
	          this.props.data.message_text
	        )
	      );
	    }
	  }, {
	    key: 'render_quick_reply',
	    value: function render_quick_reply() {
	      return _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'p',
	          null,
	          this.props.data.message_text
	        )
	      );
	    }
	  }, {
	    key: 'render_login_prompt',
	    value: function render_login_prompt() {
	      var isLastMessage = this.props.isLastOfGroup && this.props.isLastGroup;
	      var isBotMessage = !this.props.data.userId;
	
	      return _react2.default.createElement(
	        'div',
	        { style: this.getAddStyle() },
	        _react2.default.createElement(_login_prompt2.default, {
	          isLastMessage: isLastMessage,
	          isBotMessage: isBotMessage,
	          bgColor: this.props.bubbleColor,
	          onLoginPromptSend: this.props.onLoginPromptSend,
	          textColor: this.props.textColor
	        })
	      );
	    }
	  }, {
	    key: 'render_carousel',
	    value: function render_carousel() {
	      return _react2.default.createElement(_carousel2.default, { carousel: this.props.data.message_raw });
	    }
	  }, {
	    key: 'render_typing',
	    value: function render_typing() {
	      var _this7 = this;
	
	      var bubble = function bubble() {
	        return _react2.default.createElement('div', {
	          className: _style2.default.typingBubble,
	          style: { backgroundColor: _this7.props.bubbleColor, color: _this7.props.textColor }
	        });
	      };
	
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.typingGroup },
	        bubble(),
	        bubble(),
	        bubble()
	      );
	    }
	  }, {
	    key: 'render_file',
	    value: function render_file() {
	      return _react2.default.createElement(_file2.default, { file: this.props.data.message_data });
	    }
	  }, {
	    key: 'render_unsupported',
	    value: function render_unsupported() {
	      return _react2.default.createElement(
	        'div',
	        { style: this.getAddStyle() },
	        _react2.default.createElement(
	          'p',
	          null,
	          '*Unsupported message type*'
	        )
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var bubbleStyle = !!this.props.data.userId ? { backgroundColor: this.props.bubbleColor, color: this.props.textColor } : null;
	
	      var renderer = (this['render_' + this.props.data.message_type] || this.render_unsupported).bind(this);
	
	      var className = _style2.default.bubble;
	
	      if (_style2.default[this.props.data.message_type]) {
	        className += ' ' + _style2.default[this.props.data.message_type];
	      }
	
	      return _react2.default.createElement(
	        'div',
	        { className: className, style: bubbleStyle },
	        renderer()
	      );
	    }
	  }]);

	  return Message;
	}(_react.Component);

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	var getDayOfYear = __webpack_require__(38)
	var getISOWeek = __webpack_require__(42)
	var getISOYear = __webpack_require__(46)
	var parse = __webpack_require__(5)
	var isValid = __webpack_require__(47)
	var enLocale = __webpack_require__(32)
	
	/**
	 * @category Common Helpers
	 * @summary Format the date.
	 *
	 * @description
	 * Return the formatted date string in the given format.
	 *
	 * Accepted tokens:
	 * | Unit                    | Token | Result examples                  |
	 * |-------------------------|-------|----------------------------------|
	 * | Month                   | M     | 1, 2, ..., 12                    |
	 * |                         | Mo    | 1st, 2nd, ..., 12th              |
	 * |                         | MM    | 01, 02, ..., 12                  |
	 * |                         | MMM   | Jan, Feb, ..., Dec               |
	 * |                         | MMMM  | January, February, ..., December |
	 * | Quarter                 | Q     | 1, 2, 3, 4                       |
	 * |                         | Qo    | 1st, 2nd, 3rd, 4th               |
	 * | Day of month            | D     | 1, 2, ..., 31                    |
	 * |                         | Do    | 1st, 2nd, ..., 31st              |
	 * |                         | DD    | 01, 02, ..., 31                  |
	 * | Day of year             | DDD   | 1, 2, ..., 366                   |
	 * |                         | DDDo  | 1st, 2nd, ..., 366th             |
	 * |                         | DDDD  | 001, 002, ..., 366               |
	 * | Day of week             | d     | 0, 1, ..., 6                     |
	 * |                         | do    | 0th, 1st, ..., 6th               |
	 * |                         | dd    | Su, Mo, ..., Sa                  |
	 * |                         | ddd   | Sun, Mon, ..., Sat               |
	 * |                         | dddd  | Sunday, Monday, ..., Saturday    |
	 * | Day of ISO week         | E     | 1, 2, ..., 7                     |
	 * | ISO week                | W     | 1, 2, ..., 53                    |
	 * |                         | Wo    | 1st, 2nd, ..., 53rd              |
	 * |                         | WW    | 01, 02, ..., 53                  |
	 * | Year                    | YY    | 00, 01, ..., 99                  |
	 * |                         | YYYY  | 1900, 1901, ..., 2099            |
	 * | ISO week-numbering year | GG    | 00, 01, ..., 99                  |
	 * |                         | GGGG  | 1900, 1901, ..., 2099            |
	 * | AM/PM                   | A     | AM, PM                           |
	 * |                         | a     | am, pm                           |
	 * |                         | aa    | a.m., p.m.                       |
	 * | Hour                    | H     | 0, 1, ... 23                     |
	 * |                         | HH    | 00, 01, ... 23                   |
	 * |                         | h     | 1, 2, ..., 12                    |
	 * |                         | hh    | 01, 02, ..., 12                  |
	 * | Minute                  | m     | 0, 1, ..., 59                    |
	 * |                         | mm    | 00, 01, ..., 59                  |
	 * | Second                  | s     | 0, 1, ..., 59                    |
	 * |                         | ss    | 00, 01, ..., 59                  |
	 * | 1/10 of second          | S     | 0, 1, ..., 9                     |
	 * | 1/100 of second         | SS    | 00, 01, ..., 99                  |
	 * | Millisecond             | SSS   | 000, 001, ..., 999               |
	 * | Timezone                | Z     | -01:00, +00:00, ... +12:00       |
	 * |                         | ZZ    | -0100, +0000, ..., +1200         |
	 * | Seconds timestamp       | X     | 512969520                        |
	 * | Milliseconds timestamp  | x     | 512969520900                     |
	 *
	 * The characters wrapped in square brackets are escaped.
	 *
	 * The result may vary by locale.
	 *
	 * @param {Date|String|Number} date - the original date
	 * @param {String} [format='YYYY-MM-DDTHH:mm:ss.SSSZ'] - the string of tokens
	 * @param {Object} [options] - the object with options
	 * @param {Object} [options.locale=enLocale] - the locale object
	 * @returns {String} the formatted date string
	 *
	 * @example
	 * // Represent 11 February 2014 in middle-endian format:
	 * var result = format(
	 *   new Date(2014, 1, 11),
	 *   'MM/DD/YYYY'
	 * )
	 * //=> '02/11/2014'
	 *
	 * @example
	 * // Represent 2 July 2014 in Esperanto:
	 * var eoLocale = require('date-fns/locale/eo')
	 * var result = format(
	 *   new Date(2014, 6, 2),
	 *   'Do [de] MMMM YYYY',
	 *   {locale: eoLocale}
	 * )
	 * //=> '2-a de julio 2014'
	 */
	function format (dirtyDate, dirtyFormatStr, dirtyOptions) {
	  var formatStr = dirtyFormatStr ? String(dirtyFormatStr) : 'YYYY-MM-DDTHH:mm:ss.SSSZ'
	  var options = dirtyOptions || {}
	
	  var locale = options.locale
	  var localeFormatters = enLocale.format.formatters
	  var formattingTokensRegExp = enLocale.format.formattingTokensRegExp
	  if (locale && locale.format && locale.format.formatters) {
	    localeFormatters = locale.format.formatters
	
	    if (locale.format.formattingTokensRegExp) {
	      formattingTokensRegExp = locale.format.formattingTokensRegExp
	    }
	  }
	
	  var date = parse(dirtyDate)
	
	  if (!isValid(date)) {
	    return 'Invalid Date'
	  }
	
	  var formatFn = buildFormatFn(formatStr, localeFormatters, formattingTokensRegExp)
	
	  return formatFn(date)
	}
	
	var formatters = {
	  // Month: 1, 2, ..., 12
	  'M': function (date) {
	    return date.getMonth() + 1
	  },
	
	  // Month: 01, 02, ..., 12
	  'MM': function (date) {
	    return addLeadingZeros(date.getMonth() + 1, 2)
	  },
	
	  // Quarter: 1, 2, 3, 4
	  'Q': function (date) {
	    return Math.ceil((date.getMonth() + 1) / 3)
	  },
	
	  // Day of month: 1, 2, ..., 31
	  'D': function (date) {
	    return date.getDate()
	  },
	
	  // Day of month: 01, 02, ..., 31
	  'DD': function (date) {
	    return addLeadingZeros(date.getDate(), 2)
	  },
	
	  // Day of year: 1, 2, ..., 366
	  'DDD': function (date) {
	    return getDayOfYear(date)
	  },
	
	  // Day of year: 001, 002, ..., 366
	  'DDDD': function (date) {
	    return addLeadingZeros(getDayOfYear(date), 3)
	  },
	
	  // Day of week: 0, 1, ..., 6
	  'd': function (date) {
	    return date.getDay()
	  },
	
	  // Day of ISO week: 1, 2, ..., 7
	  'E': function (date) {
	    return date.getDay() || 7
	  },
	
	  // ISO week: 1, 2, ..., 53
	  'W': function (date) {
	    return getISOWeek(date)
	  },
	
	  // ISO week: 01, 02, ..., 53
	  'WW': function (date) {
	    return addLeadingZeros(getISOWeek(date), 2)
	  },
	
	  // Year: 00, 01, ..., 99
	  'YY': function (date) {
	    return addLeadingZeros(date.getFullYear(), 4).substr(2)
	  },
	
	  // Year: 1900, 1901, ..., 2099
	  'YYYY': function (date) {
	    return addLeadingZeros(date.getFullYear(), 4)
	  },
	
	  // ISO week-numbering year: 00, 01, ..., 99
	  'GG': function (date) {
	    return String(getISOYear(date)).substr(2)
	  },
	
	  // ISO week-numbering year: 1900, 1901, ..., 2099
	  'GGGG': function (date) {
	    return getISOYear(date)
	  },
	
	  // Hour: 0, 1, ... 23
	  'H': function (date) {
	    return date.getHours()
	  },
	
	  // Hour: 00, 01, ..., 23
	  'HH': function (date) {
	    return addLeadingZeros(date.getHours(), 2)
	  },
	
	  // Hour: 1, 2, ..., 12
	  'h': function (date) {
	    var hours = date.getHours()
	    if (hours === 0) {
	      return 12
	    } else if (hours > 12) {
	      return hours % 12
	    } else {
	      return hours
	    }
	  },
	
	  // Hour: 01, 02, ..., 12
	  'hh': function (date) {
	    return addLeadingZeros(formatters['h'](date), 2)
	  },
	
	  // Minute: 0, 1, ..., 59
	  'm': function (date) {
	    return date.getMinutes()
	  },
	
	  // Minute: 00, 01, ..., 59
	  'mm': function (date) {
	    return addLeadingZeros(date.getMinutes(), 2)
	  },
	
	  // Second: 0, 1, ..., 59
	  's': function (date) {
	    return date.getSeconds()
	  },
	
	  // Second: 00, 01, ..., 59
	  'ss': function (date) {
	    return addLeadingZeros(date.getSeconds(), 2)
	  },
	
	  // 1/10 of second: 0, 1, ..., 9
	  'S': function (date) {
	    return Math.floor(date.getMilliseconds() / 100)
	  },
	
	  // 1/100 of second: 00, 01, ..., 99
	  'SS': function (date) {
	    return addLeadingZeros(Math.floor(date.getMilliseconds() / 10), 2)
	  },
	
	  // Millisecond: 000, 001, ..., 999
	  'SSS': function (date) {
	    return addLeadingZeros(date.getMilliseconds(), 3)
	  },
	
	  // Timezone: -01:00, +00:00, ... +12:00
	  'Z': function (date) {
	    return formatTimezone(date.getTimezoneOffset(), ':')
	  },
	
	  // Timezone: -0100, +0000, ... +1200
	  'ZZ': function (date) {
	    return formatTimezone(date.getTimezoneOffset())
	  },
	
	  // Seconds timestamp: 512969520
	  'X': function (date) {
	    return Math.floor(date.getTime() / 1000)
	  },
	
	  // Milliseconds timestamp: 512969520900
	  'x': function (date) {
	    return date.getTime()
	  }
	}
	
	function buildFormatFn (formatStr, localeFormatters, formattingTokensRegExp) {
	  var array = formatStr.match(formattingTokensRegExp)
	  var length = array.length
	
	  var i
	  var formatter
	  for (i = 0; i < length; i++) {
	    formatter = localeFormatters[array[i]] || formatters[array[i]]
	    if (formatter) {
	      array[i] = formatter
	    } else {
	      array[i] = removeFormattingTokens(array[i])
	    }
	  }
	
	  return function (date) {
	    var output = ''
	    for (var i = 0; i < length; i++) {
	      if (array[i] instanceof Function) {
	        output += array[i](date, formatters)
	      } else {
	        output += array[i]
	      }
	    }
	    return output
	  }
	}
	
	function removeFormattingTokens (input) {
	  if (input.match(/\[[\s\S]/)) {
	    return input.replace(/^\[|]$/g, '')
	  }
	  return input.replace(/\\/g, '')
	}
	
	function formatTimezone (offset, delimeter) {
	  delimeter = delimeter || ''
	  var sign = offset > 0 ? '-' : '+'
	  var absOffset = Math.abs(offset)
	  var hours = Math.floor(absOffset / 60)
	  var minutes = absOffset % 60
	  return sign + addLeadingZeros(hours, 2) + delimeter + addLeadingZeros(minutes, 2)
	}
	
	function addLeadingZeros (number, targetLength) {
	  var output = Math.abs(number).toString()
	  while (output.length < targetLength) {
	    output = '0' + output
	  }
	  return output
	}
	
	module.exports = format


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	var startOfYear = __webpack_require__(39)
	var differenceInCalendarDays = __webpack_require__(40)
	
	/**
	 * @category Day Helpers
	 * @summary Get the day of the year of the given date.
	 *
	 * @description
	 * Get the day of the year of the given date.
	 *
	 * @param {Date|String|Number} date - the given date
	 * @returns {Number} the day of year
	 *
	 * @example
	 * // Which day of the year is 2 July 2014?
	 * var result = getDayOfYear(new Date(2014, 6, 2))
	 * //=> 183
	 */
	function getDayOfYear (dirtyDate) {
	  var date = parse(dirtyDate)
	  var diff = differenceInCalendarDays(date, startOfYear(date))
	  var dayOfYear = diff + 1
	  return dayOfYear
	}
	
	module.exports = getDayOfYear


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Year Helpers
	 * @summary Return the start of a year for the given date.
	 *
	 * @description
	 * Return the start of a year for the given date.
	 * The result will be in the local timezone.
	 *
	 * @param {Date|String|Number} date - the original date
	 * @returns {Date} the start of a year
	 *
	 * @example
	 * // The start of a year for 2 September 2014 11:55:00:
	 * var result = startOfYear(new Date(2014, 8, 2, 11, 55, 00))
	 * //=> Wed Jan 01 2014 00:00:00
	 */
	function startOfYear (dirtyDate) {
	  var cleanDate = parse(dirtyDate)
	  var date = new Date(0)
	  date.setFullYear(cleanDate.getFullYear(), 0, 1)
	  date.setHours(0, 0, 0, 0)
	  return date
	}
	
	module.exports = startOfYear


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	var startOfDay = __webpack_require__(41)
	
	var MILLISECONDS_IN_MINUTE = 60000
	var MILLISECONDS_IN_DAY = 86400000
	
	/**
	 * @category Day Helpers
	 * @summary Get the number of calendar days between the given dates.
	 *
	 * @description
	 * Get the number of calendar days between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of calendar days
	 *
	 * @example
	 * // How many calendar days are between
	 * // 2 July 2011 23:00:00 and 2 July 2012 00:00:00?
	 * var result = differenceInCalendarDays(
	 *   new Date(2012, 6, 2, 0, 0),
	 *   new Date(2011, 6, 2, 23, 0)
	 * )
	 * //=> 366
	 */
	function differenceInCalendarDays (dirtyDateLeft, dirtyDateRight) {
	  var startOfDayLeft = startOfDay(dirtyDateLeft)
	  var startOfDayRight = startOfDay(dirtyDateRight)
	
	  var timestampLeft = startOfDayLeft.getTime() -
	    startOfDayLeft.getTimezoneOffset() * MILLISECONDS_IN_MINUTE
	  var timestampRight = startOfDayRight.getTime() -
	    startOfDayRight.getTimezoneOffset() * MILLISECONDS_IN_MINUTE
	
	  // Round the number of days to the nearest integer
	  // because the number of milliseconds in a day is not constant
	  // (e.g. it's different in the day of the daylight saving time clock shift)
	  return Math.round((timestampLeft - timestampRight) / MILLISECONDS_IN_DAY)
	}
	
	module.exports = differenceInCalendarDays


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Day Helpers
	 * @summary Return the start of a day for the given date.
	 *
	 * @description
	 * Return the start of a day for the given date.
	 * The result will be in the local timezone.
	 *
	 * @param {Date|String|Number} date - the original date
	 * @returns {Date} the start of a day
	 *
	 * @example
	 * // The start of a day for 2 September 2014 11:55:00:
	 * var result = startOfDay(new Date(2014, 8, 2, 11, 55, 0))
	 * //=> Tue Sep 02 2014 00:00:00
	 */
	function startOfDay (dirtyDate) {
	  var date = parse(dirtyDate)
	  date.setHours(0, 0, 0, 0)
	  return date
	}
	
	module.exports = startOfDay


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	var startOfISOWeek = __webpack_require__(43)
	var startOfISOYear = __webpack_require__(45)
	
	var MILLISECONDS_IN_WEEK = 604800000
	
	/**
	 * @category ISO Week Helpers
	 * @summary Get the ISO week of the given date.
	 *
	 * @description
	 * Get the ISO week of the given date.
	 *
	 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
	 *
	 * @param {Date|String|Number} date - the given date
	 * @returns {Number} the ISO week
	 *
	 * @example
	 * // Which week of the ISO-week numbering year is 2 January 2005?
	 * var result = getISOWeek(new Date(2005, 0, 2))
	 * //=> 53
	 */
	function getISOWeek (dirtyDate) {
	  var date = parse(dirtyDate)
	  var diff = startOfISOWeek(date).getTime() - startOfISOYear(date).getTime()
	
	  // Round the number of days to the nearest integer
	  // because the number of milliseconds in a week is not constant
	  // (e.g. it's different in the week of the daylight saving time clock shift)
	  return Math.round(diff / MILLISECONDS_IN_WEEK) + 1
	}
	
	module.exports = getISOWeek


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	var startOfWeek = __webpack_require__(44)
	
	/**
	 * @category ISO Week Helpers
	 * @summary Return the start of an ISO week for the given date.
	 *
	 * @description
	 * Return the start of an ISO week for the given date.
	 * The result will be in the local timezone.
	 *
	 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
	 *
	 * @param {Date|String|Number} date - the original date
	 * @returns {Date} the start of an ISO week
	 *
	 * @example
	 * // The start of an ISO week for 2 September 2014 11:55:00:
	 * var result = startOfISOWeek(new Date(2014, 8, 2, 11, 55, 0))
	 * //=> Mon Sep 01 2014 00:00:00
	 */
	function startOfISOWeek (dirtyDate) {
	  return startOfWeek(dirtyDate, {weekStartsOn: 1})
	}
	
	module.exports = startOfISOWeek


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	
	/**
	 * @category Week Helpers
	 * @summary Return the start of a week for the given date.
	 *
	 * @description
	 * Return the start of a week for the given date.
	 * The result will be in the local timezone.
	 *
	 * @param {Date|String|Number} date - the original date
	 * @param {Object} [options] - the object with options
	 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
	 * @returns {Date} the start of a week
	 *
	 * @example
	 * // The start of a week for 2 September 2014 11:55:00:
	 * var result = startOfWeek(new Date(2014, 8, 2, 11, 55, 0))
	 * //=> Sun Aug 31 2014 00:00:00
	 *
	 * @example
	 * // If the week starts on Monday, the start of the week for 2 September 2014 11:55:00:
	 * var result = startOfWeek(new Date(2014, 8, 2, 11, 55, 0), {weekStartsOn: 1})
	 * //=> Mon Sep 01 2014 00:00:00
	 */
	function startOfWeek (dirtyDate, dirtyOptions) {
	  var weekStartsOn = dirtyOptions ? (Number(dirtyOptions.weekStartsOn) || 0) : 0
	
	  var date = parse(dirtyDate)
	  var day = date.getDay()
	  var diff = (day < weekStartsOn ? 7 : 0) + day - weekStartsOn
	
	  date.setDate(date.getDate() - diff)
	  date.setHours(0, 0, 0, 0)
	  return date
	}
	
	module.exports = startOfWeek


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	var getISOYear = __webpack_require__(46)
	var startOfISOWeek = __webpack_require__(43)
	
	/**
	 * @category ISO Week-Numbering Year Helpers
	 * @summary Return the start of an ISO week-numbering year for the given date.
	 *
	 * @description
	 * Return the start of an ISO week-numbering year,
	 * which always starts 3 days before the year's first Thursday.
	 * The result will be in the local timezone.
	 *
	 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
	 *
	 * @param {Date|String|Number} date - the original date
	 * @returns {Date} the start of an ISO year
	 *
	 * @example
	 * // The start of an ISO week-numbering year for 2 July 2005:
	 * var result = startOfISOYear(new Date(2005, 6, 2))
	 * //=> Mon Jan 03 2005 00:00:00
	 */
	function startOfISOYear (dirtyDate) {
	  var year = getISOYear(dirtyDate)
	  var fourthOfJanuary = new Date(0)
	  fourthOfJanuary.setFullYear(year, 0, 4)
	  fourthOfJanuary.setHours(0, 0, 0, 0)
	  var date = startOfISOWeek(fourthOfJanuary)
	  return date
	}
	
	module.exports = startOfISOYear


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	var parse = __webpack_require__(5)
	var startOfISOWeek = __webpack_require__(43)
	
	/**
	 * @category ISO Week-Numbering Year Helpers
	 * @summary Get the ISO week-numbering year of the given date.
	 *
	 * @description
	 * Get the ISO week-numbering year of the given date,
	 * which always starts 3 days before the year's first Thursday.
	 *
	 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
	 *
	 * @param {Date|String|Number} date - the given date
	 * @returns {Number} the ISO week-numbering year
	 *
	 * @example
	 * // Which ISO-week numbering year is 2 January 2005?
	 * var result = getISOYear(new Date(2005, 0, 2))
	 * //=> 2004
	 */
	function getISOYear (dirtyDate) {
	  var date = parse(dirtyDate)
	  var year = date.getFullYear()
	
	  var fourthOfJanuaryOfNextYear = new Date(0)
	  fourthOfJanuaryOfNextYear.setFullYear(year + 1, 0, 4)
	  fourthOfJanuaryOfNextYear.setHours(0, 0, 0, 0)
	  var startOfNextYear = startOfISOWeek(fourthOfJanuaryOfNextYear)
	
	  var fourthOfJanuaryOfThisYear = new Date(0)
	  fourthOfJanuaryOfThisYear.setFullYear(year, 0, 4)
	  fourthOfJanuaryOfThisYear.setHours(0, 0, 0, 0)
	  var startOfThisYear = startOfISOWeek(fourthOfJanuaryOfThisYear)
	
	  if (date.getTime() >= startOfNextYear.getTime()) {
	    return year + 1
	  } else if (date.getTime() >= startOfThisYear.getTime()) {
	    return year
	  } else {
	    return year - 1
	  }
	}
	
	module.exports = getISOYear


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	var isDate = __webpack_require__(6)
	
	/**
	 * @category Common Helpers
	 * @summary Is the given date valid?
	 *
	 * @description
	 * Returns false if argument is Invalid Date and true otherwise.
	 * Invalid Date is a Date, whose time value is NaN.
	 *
	 * Time value of Date: http://es5.github.io/#x15.9.1.1
	 *
	 * @param {Date} date - the date to check
	 * @returns {Boolean} the date is valid
	 * @throws {TypeError} argument must be an instance of Date
	 *
	 * @example
	 * // For the valid date:
	 * var result = isValid(new Date(2014, 1, 31))
	 * //=> true
	 *
	 * @example
	 * // For the invalid date:
	 * var result = isValid(new Date(''))
	 * //=> false
	 */
	function isValid (dirtyDate) {
	  if (isDate(dirtyDate)) {
	    return !isNaN(dirtyDate)
	  } else {
	    throw new TypeError(toString.call(dirtyDate) + ' is not an instance of Date')
	  }
	}
	
	module.exports = isValid


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	var differenceInMilliseconds = __webpack_require__(28)
	
	var MILLISECONDS_IN_MINUTE = 60000
	
	/**
	 * @category Minute Helpers
	 * @summary Get the number of minutes between the given dates.
	 *
	 * @description
	 * Get the number of minutes between the given dates.
	 *
	 * @param {Date|String|Number} dateLeft - the later date
	 * @param {Date|String|Number} dateRight - the earlier date
	 * @returns {Number} the number of minutes
	 *
	 * @example
	 * // How many minutes are between 2 July 2014 12:07:59 and 2 July 2014 12:20:00?
	 * var result = differenceInMinutes(
	 *   new Date(2014, 6, 2, 12, 20, 0),
	 *   new Date(2014, 6, 2, 12, 7, 59)
	 * )
	 * //=> 12
	 */
	function differenceInMinutes (dirtyDateLeft, dirtyDateRight) {
	  var diff = differenceInMilliseconds(dirtyDateLeft, dirtyDateRight) / MILLISECONDS_IN_MINUTE
	  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
	}
	
	module.exports = differenceInMinutes


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _misc = __webpack_require__(50);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	var _reactFileInput = __webpack_require__(53);
	
	var _reactFileInput2 = _interopRequireDefault(_reactFileInput);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var QuickReply = function (_Component) {
	  _inherits(QuickReply, _Component);
	
	  function QuickReply(props) {
	    _classCallCheck(this, QuickReply);
	
	    var _this = _possibleConstructorReturn(this, (QuickReply.__proto__ || Object.getPrototypeOf(QuickReply)).call(this, props));
	
	    _this.state = { hover: false };
	    return _this;
	  }
	
	  _createClass(QuickReply, [{
	    key: 'handleClick',
	    value: function handleClick(event) {
	      this.props.onQuickReplySend && this.props.onQuickReplySend(this.props.title, this.props.payload);
	    }
	  }, {
	    key: 'handleFileUpload',
	    value: function handleFileUpload(event) {
	      if (!event.target.files) {
	        return;
	      }
	
	      this.props.onFileUploadSend && this.props.onFileUploadSend(this.props.title, this.props.payload, event.target.files[0]);
	    }
	  }, {
	    key: 'renderFileUpload',
	    value: function renderFileUpload(accept) {
	      var _this2 = this;
	
	      var backgroundColor = this.state.hover ? (0, _misc.hexToRGBA)(this.props.fgColor, 0.07) : (0, _misc.hexToRGBA)(this.props.fgColor, 0);
	
	      return _react2.default.createElement(
	        'button',
	        {
	          className: _style2.default.bubble,
	          style: { color: this.props.fgColor, backgroundColor: backgroundColor },
	          onMouseOver: function onMouseOver() {
	            return _this2.setState({ hover: true });
	          },
	          onMouseOut: function onMouseOut() {
	            return _this2.setState({ hover: false });
	          } },
	        _react2.default.createElement(
	          'span',
	          null,
	          this.props.title
	        ),
	        _react2.default.createElement(_reactFileInput2.default, {
	          name: 'uploadField',
	          accept: accept,
	          className: _style2.default.filePicker,
	          placeholder: this.props.title,
	          onChange: this.handleFileUpload.bind(this) })
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this3 = this;
	
	      var backgroundColor = this.state.hover ? (0, _misc.hexToRGBA)(this.props.fgColor, 0.07) : (0, _misc.hexToRGBA)(this.props.fgColor, 0);
	
	      if (this.props.payload === 'BOTPRESS.IMAGE_UPLOAD') {
	        return this.renderFileUpload('image/*');
	      }
	
	      if (this.props.payload === 'BOTPRESS.FILE_UPLOAD') {
	        return this.renderFileUpload('*/*');
	      }
	
	      return _react2.default.createElement(
	        'button',
	        {
	          className: _style2.default.bubble,
	          style: { color: this.props.fgColor, backgroundColor: backgroundColor },
	          onClick: this.handleClick.bind(this),
	          onMouseOver: function onMouseOver() {
	            return _this3.setState({ hover: true });
	          },
	          onMouseOut: function onMouseOut() {
	            return _this3.setState({ hover: false });
	          } },
	        this.props.title
	      );
	    }
	  }]);
	
	  return QuickReply;
	}(_react.Component);
	
	var QuickReplies = function QuickReplies(props) {
	  if (!props.quick_replies) {
	    return null;
	  }
	
	  var quick_replies = props.quick_replies.map(function (qr) {
	    return _react2.default.createElement(QuickReply, _extends({}, props, qr));
	  });
	
	  return _react2.default.createElement(
	    'div',
	    { className: _style2.default.quickReplyContainer },
	    quick_replies
	  );
	};
	
	exports.default = QuickReplies;

/***/ }),
/* 50 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.hexToRGBA = hexToRGBA;
	function hexToRGBA(hex) {
	  var alpha = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
	
	  var r = parseInt(hex.slice(1, 3), 16);
	  var g = parseInt(hex.slice(3, 5), 16);
	  var b = parseInt(hex.slice(5, 7), 16);
	  return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
	}

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(52);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, ".botpress-platform-webchat__style__avatar___3fBbF {\n  margin-top: .15rem;\n  margin-right: .75rem;\n  -ms-flex-negative: 0;\n  flex-shrink: 0;\n  width: 1.875rem;\n  height: 1.875rem; }\n  .botpress-platform-webchat__style__avatar___3fBbF .botpress-platform-webchat__style__color___1M6jC {\n    fill: currentColor; }\n  .botpress-platform-webchat__style__avatar___3fBbF .botpress-platform-webchat__style__picture___uiWi7 {\n    border-radius: 50%;\n    background-size: cover;\n    background-position: 50%;\n    width: 100%;\n    height: 100%; }\n\n.botpress-platform-webchat__style__date___3RYpm {\n  color: #9a9a9a;\n  text-align: center;\n  font-size: 12px;\n  margin: 40px 0 20px; }\n  .botpress-platform-webchat__style__date___3RYpm .botpress-platform-webchat__style__smallLine___3MArC {\n    border-bottom: solid 1px #e6e6e6;\n    margin: auto;\n    width: 80px;\n    margin-top: 5px; }\n\n.botpress-platform-webchat__style__messages___tQB_X {\n  -webkit-box-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  overflow-y: auto;\n  -ms-flex-positive: 1;\n  flex-grow: 1;\n  padding: 0 .65rem 1rem; }\n\n.botpress-platform-webchat__style__message___2Z-Wf {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 1rem 0;\n  -webkit-box-align: end;\n  -ms-flex-align: end;\n  align-items: flex-end;\n  padding-right: 18px;\n  padding-left: 0; }\n  .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq {\n    width: 100%;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n    -ms-flex-direction: column;\n    flex-direction: column;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__info-line___2iX2w {\n      margin: 0 12px;\n      -webkit-box-flex: 0;\n      -ms-flex: 0 1 auto;\n      flex: 0 1 auto;\n      padding: .3125rem 0;\n      font-size: 12px;\n      color: #9a9a9a;\n      -webkit-box-align: center;\n      -ms-flex-align: center;\n      align-items: center;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex; }\n    .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n      flex-direction: column;\n      -webkit-box-align: start;\n      -ms-flex-align: start;\n      align-items: flex-start;\n      width: 100%; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE > :not(:last-child) {\n        margin-bottom: 2px; }\n    .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J {\n      border-top-left-radius: 4px;\n      border-bottom-left-radius: 4px;\n      border-top-right-radius: 8px;\n      border-bottom-right-radius: 8px;\n      max-width: 80%;\n      background-color: #f8f8f8;\n      padding: .5rem .75rem;\n      white-space: pre-wrap;\n      word-break: break-word; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J:first-of-type {\n        border-top-left-radius: 16px;\n        border-top-right-radius: 16px; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J:last-of-type {\n        border-bottom-left-radius: 16px;\n        border-bottom-right-radius: 16px; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J.botpress-platform-webchat__style__file___1q6sA {\n        padding: 0; }\n        .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J.botpress-platform-webchat__style__file___1q6sA a {\n          display: block;\n          cursor: pointer;\n          border-top-left-radius: inherit;\n          border-top-right-radius: inherit;\n          border-bottom-right-radius: inherit;\n          border-bottom-left-radius: inherit; }\n          .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J.botpress-platform-webchat__style__file___1q6sA a img {\n            max-height: 240px;\n            max-width: 100%;\n            border-top-left-radius: inherit;\n            border-top-right-radius: inherit;\n            border-bottom-right-radius: inherit;\n            border-bottom-left-radius: inherit; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J.botpress-platform-webchat__style__carousel___3GAV5 {\n        background: none;\n        padding: 0;\n        margin: 10px 0; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J p {\n        margin: 0; }\n      .botpress-platform-webchat__style__message___2Z-Wf .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__bubble___eNt_J .botpress-platform-webchat__style__specialAction___2EaSP {\n        opacity: 0.6; }\n\n.botpress-platform-webchat__style__otherFile___3yT-c {\n  padding: .5rem .75rem; }\n\n.botpress-platform-webchat__style__user___2KjQ8 {\n  padding-right: 0; }\n  .botpress-platform-webchat__style__user___2KjQ8 .botpress-platform-webchat__style__message-container___1cLJq {\n    -webkit-box-pack: end;\n    -ms-flex-pack: end;\n    justify-content: flex-end;\n    -webkit-box-align: end;\n    -ms-flex-align: end;\n    align-items: flex-end; }\n    .botpress-platform-webchat__style__user___2KjQ8 .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE {\n      padding-left: 60px;\n      -webkit-box-pack: end;\n      -ms-flex-pack: end;\n      justify-content: flex-end;\n      -webkit-box-align: end;\n      -ms-flex-align: end;\n      align-items: flex-end; }\n      .botpress-platform-webchat__style__user___2KjQ8 .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE .botpress-platform-webchat__style__bubble___eNt_J {\n        color: white;\n        background-color: #0176ff;\n        border-top-left-radius: 8px;\n        border-bottom-left-radius: 8px;\n        border-top-right-radius: 4px;\n        border-bottom-right-radius: 4px; }\n        .botpress-platform-webchat__style__user___2KjQ8 .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE .botpress-platform-webchat__style__bubble___eNt_J:first-of-type {\n          border-top-left-radius: 16px;\n          border-top-right-radius: 16px; }\n        .botpress-platform-webchat__style__user___2KjQ8 .botpress-platform-webchat__style__message-container___1cLJq .botpress-platform-webchat__style__group___1GcAE .botpress-platform-webchat__style__bubble___eNt_J:last-of-type {\n          border-bottom-right-radius: 16px;\n          border-bottom-left-radius: 16px; }\n\n.botpress-platform-webchat__style__formOverlay___2ycVo {\n  background-color: rgba(0, 0, 0, 0.5);\n  width: 100vw;\n  position: absolute;\n  top: 0;\n  z-index: 1000000;\n  height: 100vh;\n  left: 0;\n  padding: 5%; }\n\n.botpress-platform-webchat__style__formContainer___5YpLm {\n  width: 320px;\n  margin: auto;\n  background-color: #fff;\n  padding: 25px;\n  border-radius: 5px; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__formClose___2c2Ll {\n    display: inline-block;\n    float: right;\n    vertical-align: top;\n    position: relative;\n    top: 0;\n    cursor: pointer; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__formTextarea___B0S4a {\n    display: block;\n    width: 100%;\n    height: 80px;\n    min-height: 80px;\n    max-width: 270px;\n    padding: 6px 12px;\n    font-size: 14px;\n    line-height: 1.42857143;\n    color: #555;\n    background-color: #fff;\n    background-image: none;\n    border: 1px solid #ccc;\n    border-radius: 4px;\n    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;\n    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;\n    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__formInput___3nun6 {\n    display: block;\n    width: 100%;\n    height: 34px;\n    padding: 6px 12px;\n    font-size: 14px;\n    line-height: 1.42857143;\n    color: #555;\n    background-color: #fff;\n    background-image: none;\n    border: 1px solid #ccc;\n    border-radius: 4px;\n    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;\n    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;\n    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__formTitle___nj4zQ {\n    color: #000;\n    font-size: 18px; }\n  .botpress-platform-webchat__style__formContainer___5YpLm label {\n    display: inline-block;\n    max-width: 100%;\n    margin-bottom: 5px;\n    font-weight: bold;\n    font-size: 14px; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__formGroup___2vIrz {\n    margin-bottom: 15px;\n    margin-top: 10px; }\n  .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__buttonLayer___3KNiC {\n    text-align: right; }\n    .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__buttonLayer___3KNiC .botpress-platform-webchat__style__formSubmit___yqm-f {\n      display: inline-block;\n      padding: 6px 12px;\n      margin-bottom: 0;\n      font-size: 14px;\n      font-weight: normal;\n      line-height: 1.42857143;\n      text-align: center;\n      white-space: nowrap;\n      vertical-align: middle;\n      -ms-touch-action: manipulation;\n      touch-action: manipulation;\n      cursor: pointer;\n      -webkit-user-select: none;\n      -moz-user-select: none;\n      -ms-user-select: none;\n      user-select: none;\n      background-image: none;\n      border-radius: 4px;\n      color: #333;\n      background-color: #fff;\n      border: 1px solid #ccc; }\n    .botpress-platform-webchat__style__formContainer___5YpLm .botpress-platform-webchat__style__buttonLayer___3KNiC .botpress-platform-webchat__style__formSubmit___yqm-f:hover {\n      background-color: #ccc; }\n\n.botpress-platform-webchat__style__loginPromptContainer___SR5Cp label {\n  display: block; }\n\n.botpress-platform-webchat__style__loginPromptContainer___SR5Cp .botpress-platform-webchat__style__loginInput___jFL-7 {\n  height: 30px;\n  border: none;\n  outline: none;\n  background-color: #f8f8f8;\n  border-bottom: 1px solid #bbb;\n  width: 200px;\n  text-align: center;\n  margin: 10px;\n  font-size: 14px; }\n\n.botpress-platform-webchat__style__loginPromptContainer___SR5Cp .botpress-platform-webchat__style__loginButton___i7cOj {\n  display: block;\n  font-size: 16px;\n  height: 30px;\n  width: 130px;\n  text-align: center;\n  margin: auto;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border: none;\n  outline: none;\n  background-color: black;\n  color: white; }\n\n@keyframes botpress-platform-webchat__style__typingAnimation___A3b6N {\n  0% {\n    transform: translateY(0px); }\n  28% {\n    transform: translateY(-6px); }\n  44% {\n    transform: translateY(0px); } }\n\n.botpress-platform-webchat__style__typingGroup___3DxRA {\n  align-items: center;\n  display: flex;\n  height: 17px; }\n  .botpress-platform-webchat__style__typingGroup___3DxRA .botpress-platform-webchat__style__typingBubble___2VoUG {\n    animation: botpress-platform-webchat__style__typingAnimation___A3b6N 1s infinite ease-in-out;\n    border-radius: 50%;\n    height: 6px;\n    margin-right: 4px;\n    vertical-align: middle;\n    width: 6px;\n    background-color: #4076e2;\n    color: #943e3e;\n    white-space: pre-wrap;\n    font-size: 16px;\n    direction: ltr; }\n    .botpress-platform-webchat__style__typingGroup___3DxRA .botpress-platform-webchat__style__typingBubble___2VoUG:nth-child(1) {\n      animation-delay: 200ms; }\n    .botpress-platform-webchat__style__typingGroup___3DxRA .botpress-platform-webchat__style__typingBubble___2VoUG:nth-child(2) {\n      animation-delay: 300ms; }\n    .botpress-platform-webchat__style__typingGroup___3DxRA .botpress-platform-webchat__style__typingBubble___2VoUG:nth-child(3) {\n      animation-delay: 400ms; }\n\n.botpress-platform-webchat__style__quickReplyContainer___1iL8H {\n  box-sizing: border-box;\n  display: block;\n  text-align: right;\n  text-align: right; }\n  .botpress-platform-webchat__style__quickReplyContainer___1iL8H button {\n    display: inline-block;\n    margin: 3px 2px;\n    cursor: pointer;\n    text-transform: none;\n    overflow: visible;\n    align-items: flex-start; }\n    .botpress-platform-webchat__style__quickReplyContainer___1iL8H button.botpress-platform-webchat__style__bubble___eNt_J {\n      box-shadow: none;\n      background-color: transparent;\n      font-size: 1rem;\n      padding: .4rem 0.8rem;\n      width: initial;\n      min-width: 5rem;\n      height: 40px;\n      text-align: center;\n      border: 1px solid currentColor;\n      border-radius: 20px; }\n    .botpress-platform-webchat__style__quickReplyContainer___1iL8H button .botpress-platform-webchat__style__filePicker___LVaur {\n      top: -32px !important;\n      height: 38px !important;\n      cursor: pointer; }\n\n.botpress-platform-webchat__style__carousel___3GAV5 .slick-arrow {\n  background-color: #fff;\n  border: 1px solid #dddfe2;\n  width: 32px;\n  height: 32px;\n  border-radius: 3px;\n  box-shadow: none;\n  color: #444; }\n  .botpress-platform-webchat__style__carousel___3GAV5 .slick-arrow:before {\n    color: #444; }\n\n.botpress-platform-webchat__style__carousel-item___ryfbW {\n  box-sizing: border-box;\n  color: black;\n  line-height: 1.28em;\n  width: 500px !important;\n  margin-right: 10px;\n  min-height: 150px;\n  max-height: 410px;\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  border-radius: 4px;\n  overflow: hidden; }\n  .botpress-platform-webchat__style__carousel-item___ryfbW .botpress-platform-webchat__style__picture___uiWi7 {\n    background-position: 50% 50%;\n    background-size: cover;\n    height: 250px;\n    width: 100%; }\n  .botpress-platform-webchat__style__carousel-item___ryfbW .botpress-platform-webchat__style__info___2ozhR {\n    padding: 6px 12px 12px 12px;\n    min-height: 150px; }\n    .botpress-platform-webchat__style__carousel-item___ryfbW .botpress-platform-webchat__style__info___2ozhR .botpress-platform-webchat__style__title___DZhyN {\n      text-overflow: ellipsis;\n      white-space: normal;\n      font-weight: bold;\n      line-height: 1.28em;\n      max-height: 40px;\n      overflow: hidden; }\n    .botpress-platform-webchat__style__carousel-item___ryfbW .botpress-platform-webchat__style__info___2ozhR .botpress-platform-webchat__style__subtitle___1hsEW {\n      color: rgba(0, 0, 0, 0.4); }\n  .botpress-platform-webchat__style__carousel-item___ryfbW .botpress-platform-webchat__style__action___2J_jR {\n    color: #0084ff;\n    font-weight: 500;\n    padding: 0 10px;\n    border-top: 1px solid rgba(0, 0, 0, 0.1);\n    display: block;\n    cursor: pointer;\n    text-decoration: none;\n    line-height: 32px;\n    text-align: center; }\n", ""]);
	
	// exports
	exports.locals = {
		"avatar": "botpress-platform-webchat__style__avatar___3fBbF",
		"color": "botpress-platform-webchat__style__color___1M6jC",
		"picture": "botpress-platform-webchat__style__picture___uiWi7",
		"date": "botpress-platform-webchat__style__date___3RYpm",
		"smallLine": "botpress-platform-webchat__style__smallLine___3MArC",
		"messages": "botpress-platform-webchat__style__messages___tQB_X",
		"message": "botpress-platform-webchat__style__message___2Z-Wf",
		"message-container": "botpress-platform-webchat__style__message-container___1cLJq",
		"info-line": "botpress-platform-webchat__style__info-line___2iX2w",
		"group": "botpress-platform-webchat__style__group___1GcAE",
		"bubble": "botpress-platform-webchat__style__bubble___eNt_J",
		"file": "botpress-platform-webchat__style__file___1q6sA",
		"carousel": "botpress-platform-webchat__style__carousel___3GAV5",
		"specialAction": "botpress-platform-webchat__style__specialAction___2EaSP",
		"otherFile": "botpress-platform-webchat__style__otherFile___3yT-c",
		"user": "botpress-platform-webchat__style__user___2KjQ8",
		"formOverlay": "botpress-platform-webchat__style__formOverlay___2ycVo",
		"formContainer": "botpress-platform-webchat__style__formContainer___5YpLm",
		"formClose": "botpress-platform-webchat__style__formClose___2c2Ll",
		"formTextarea": "botpress-platform-webchat__style__formTextarea___B0S4a",
		"formInput": "botpress-platform-webchat__style__formInput___3nun6",
		"formTitle": "botpress-platform-webchat__style__formTitle___nj4zQ",
		"formGroup": "botpress-platform-webchat__style__formGroup___2vIrz",
		"buttonLayer": "botpress-platform-webchat__style__buttonLayer___3KNiC",
		"formSubmit": "botpress-platform-webchat__style__formSubmit___yqm-f",
		"loginPromptContainer": "botpress-platform-webchat__style__loginPromptContainer___SR5Cp",
		"loginInput": "botpress-platform-webchat__style__loginInput___jFL-7",
		"loginButton": "botpress-platform-webchat__style__loginButton___i7cOj",
		"typingGroup": "botpress-platform-webchat__style__typingGroup___3DxRA",
		"typingBubble": "botpress-platform-webchat__style__typingBubble___2VoUG",
		"typingAnimation": "botpress-platform-webchat__style__typingAnimation___A3b6N",
		"quickReplyContainer": "botpress-platform-webchat__style__quickReplyContainer___1iL8H",
		"filePicker": "botpress-platform-webchat__style__filePicker___LVaur",
		"carousel-item": "botpress-platform-webchat__style__carousel-item___ryfbW",
		"info": "botpress-platform-webchat__style__info___2ozhR",
		"title": "botpress-platform-webchat__style__title___DZhyN",
		"subtitle": "botpress-platform-webchat__style__subtitle___1hsEW",
		"action": "botpress-platform-webchat__style__action___2J_jR"
	};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

	var React = __webpack_require__(2);
	
	var FileInput = React.createClass({
	  getInitialState: function() {
	    return {
	      value: '',
	      styles: {
	        parent: {
	          position: 'relative'
	        },
	        file: {
	          position: 'absolute',
	          top: 0,
	          left: 0,
	          opacity: 0,
	          width: '100%',
	          zIndex: 1
	        },
	        text: {
	          position: 'relative',
	          zIndex: -1
	        }
	      }
	    };
	  },
	
	  handleChange: function(e) {
	    this.setState({
	      value: e.target.value.split(/(\\|\/)/g).pop()
	    });
	    if (this.props.onChange) this.props.onChange(e);
	  },
	
	  render: function() {
	    return React.DOM.div({
	        style: this.state.styles.parent
	      },
	
	      // Actual file input
	      React.DOM.input({
	        type: 'file',
	        name: this.props.name,
	        className: this.props.className,
	        onChange: this.handleChange,
	        disabled: this.props.disabled,
	        accept: this.props.accept,
	        style: this.state.styles.file
	      }),
	
	      // Emulated file input
	      React.DOM.input({
	        type: 'text',
	        tabIndex: -1,
	        name: this.props.name + '_filename',
	        value: this.state.value,
	        className: this.props.className,
	        onChange: function() {},
	        placeholder: this.props.placeholder,
	        disabled: this.props.disabled,
	        style: this.state.styles.text
	      }));
	  }
	});
	
	module.exports = FileInput;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _misc = __webpack_require__(50);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var LoginPrompt = function (_Component) {
	  _inherits(LoginPrompt, _Component);
	
	  function LoginPrompt(props) {
	    _classCallCheck(this, LoginPrompt);
	
	    var _this = _possibleConstructorReturn(this, (LoginPrompt.__proto__ || Object.getPrototypeOf(LoginPrompt)).call(this, props));
	
	    _this.state = {
	      username: '',
	      password: ''
	    };
	    return _this;
	  }
	
	  _createClass(LoginPrompt, [{
	    key: 'handleChange',
	    value: function handleChange(field) {
	      var _this2 = this;
	
	      return function (e) {
	        _this2.setState(_defineProperty({}, field, e.target.value));
	      };
	    }
	  }, {
	    key: 'handleSubmit',
	    value: function handleSubmit(event) {
	      if (this.props.onLoginPromptSend) {
	        this.props.onLoginPromptSend(this.state.username, this.state.password);
	      }
	
	      event.preventDefault();
	    }
	  }, {
	    key: 'render_bot_active',
	    value: function render_bot_active() {
	      var buttonBackgroundColor = (0, _misc.hexToRGBA)(this.props.bgColor, 1);
	      var buttonTextColor = (0, _misc.hexToRGBA)(this.props.textColor, 1);
	
	      var fieldTextColor = (0, _misc.hexToRGBA)(this.props.bgColor, 0.7);
	      var usernameLineColor = (0, _misc.hexToRGBA)(this.props.bgColor, 0.07);
	      var passwordLineColor = (0, _misc.hexToRGBA)(this.props.bgColor, 0.07);
	
	      return _react2.default.createElement(
	        'form',
	        { className: _style2.default.loginPromptContainer, onSubmit: this.handleSubmit.bind(this) },
	        _react2.default.createElement(
	          'label',
	          null,
	          _react2.default.createElement('input', {
	            style: { 'border-bottom-color': usernameLineColor, color: fieldTextColor },
	            className: _style2.default.loginInput,
	            type: 'input',
	            placeholder: 'Username',
	            onChange: this.handleChange.call(this, 'username') })
	        ),
	        _react2.default.createElement(
	          'label',
	          null,
	          _react2.default.createElement('input', {
	            style: { 'border-bottom-color': passwordLineColor, color: fieldTextColor },
	            className: _style2.default.loginInput,
	            type: 'password',
	            placeholder: 'Password',
	            onChange: this.handleChange.call(this, 'password') })
	        ),
	        _react2.default.createElement('input', {
	          style: { 'background-color': buttonBackgroundColor, color: buttonTextColor },
	          className: _style2.default.loginButton,
	          type: 'submit',
	          value: 'Submit' })
	      );
	    }
	  }, {
	    key: 'render_bot_past',
	    value: function render_bot_past() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.specialAction },
	        _react2.default.createElement(
	          'p',
	          null,
	          '* Login form *'
	        )
	      );
	    }
	  }, {
	    key: 'render_user',
	    value: function render_user() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.specialAction },
	        _react2.default.createElement(
	          'p',
	          null,
	          '* Provided credentials *'
	        )
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      if (!this.props.isBotMessage) {
	        return this.render_user();
	      }
	
	      if (this.props.isLastMessage) {
	        return this.render_bot_active();
	      }
	
	      return this.render_bot_past();
	    }
	  }]);
	
	  return LoginPrompt;
	}(_react.Component);
	
	exports.default = LoginPrompt;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _misc = __webpack_require__(50);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var FileMessage = function (_Component) {
	  _inherits(FileMessage, _Component);
	
	  function FileMessage(props) {
	    _classCallCheck(this, FileMessage);
	
	    var _this = _possibleConstructorReturn(this, (FileMessage.__proto__ || Object.getPrototypeOf(FileMessage)).call(this, props));
	
	    _this.state = { hover: false };
	    return _this;
	  }
	
	  _createClass(FileMessage, [{
	    key: 'renderLocalFile',
	    value: function renderLocalFile() {
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.otherFile },
	        _react2.default.createElement(
	          'div',
	          null,
	          this.props.file.name,
	          ' (local)'
	        )
	      );
	    }
	  }, {
	    key: 'renderRemoteFile',
	    value: function renderRemoteFile() {
	      if (this.props.file && this.props.file.mime) {
	        if (this.props.file.mime.includes('image/')) return this.renderRemoteImage();else if (this.props.file.mime.includes('audio/')) return this.renderAudio();else if (this.props.file.mime.includes('video/')) return this.renderVideo();else if (this.props.file.mime.includes('audio/')) return this.renderAudio();
	      } else {
	        return _react2.default.createElement(
	          'div',
	          { className: _style2.default.otherFile },
	          _react2.default.createElement(
	            'a',
	            { href: this.props.file.url },
	            this.props.file.name
	          )
	        );
	      }
	    }
	  }, {
	    key: 'renderRemoteImage',
	    value: function renderRemoteImage() {
	      return _react2.default.createElement(
	        'a',
	        { href: this.props.file.url, target: '_blank' },
	        _react2.default.createElement('img', { src: this.props.file.url, title: this.props.file.name })
	      );
	    }
	  }, {
	    key: 'renderAudio',
	    value: function renderAudio() {
	      return _react2.default.createElement(
	        'audio',
	        { controls: true },
	        _react2.default.createElement('source', { src: this.props.file.url, type: this.props.file.mime })
	      );
	    }
	  }, {
	    key: 'renderVideo',
	    value: function renderVideo() {
	      return _react2.default.createElement(
	        'video',
	        { width: '240', controls: true },
	        _react2.default.createElement('source', { src: this.props.file.url, type: this.props.file.mime })
	      );
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      if (this.props.file.storage !== 'local') {
	        return this.renderRemoteFile();
	      }
	
	      return this.renderLocalFile();
	    }
	  }]);
	
	  return FileMessage;
	}(_react.Component);
	
	exports.default = FileMessage;

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactSlick = __webpack_require__(57);
	
	var _reactSlick2 = _interopRequireDefault(_reactSlick);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	__webpack_require__(85);
	__webpack_require__(87);
	
	var CarouselMessage = function (_Component) {
	  _inherits(CarouselMessage, _Component);
	
	  function CarouselMessage(props) {
	    _classCallCheck(this, CarouselMessage);
	
	    var _this = _possibleConstructorReturn(this, (CarouselMessage.__proto__ || Object.getPrototypeOf(CarouselMessage)).call(this, props));
	
	    _this.state = { hover: false };
	    return _this;
	  }
	
	  _createClass(CarouselMessage, [{
	    key: 'render',
	    value: function render() {
	
	      var CarouselElement = function CarouselElement(el) {
	
	        return _react2.default.createElement(
	          'div',
	          { className: _style2.default['carousel-item'] },
	          el.picture && _react2.default.createElement('div', { className: _style2.default.picture, style: { backgroundImage: 'url("' + el.picture + '")' } }),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.more },
	            _react2.default.createElement(
	              'div',
	              { className: _style2.default.info },
	              _react2.default.createElement(
	                'div',
	                { className: _style2.default.title },
	                el.title
	              ),
	              el.subtitle && _react2.default.createElement(
	                'div',
	                { className: _style2.default.subtitle },
	                el.subtitle
	              )
	            ),
	            _react2.default.createElement(
	              'div',
	              { className: _style2.default.buttons },
	              el.buttons.map(function (btn) {
	                if (btn.url) {
	                  return _react2.default.createElement(
	                    'a',
	                    { href: btn.url, target: '_blank', className: _style2.default.action },
	                    btn.title
	                  );
	                } else {
	                  return _react2.default.createElement(
	                    'a',
	                    { href: '#', className: _style2.default.action },
	                    '[TODO] ' + btn.title || btn
	                  );
	                }
	              })
	            )
	          )
	        );
	      };
	
	      var elements = this.props.carousel.elements || [];
	
	      var settings = {
	        dots: false,
	        infinite: false,
	        // slidesToShow: 3,
	        responsible: [{ breakpoint: 550, settings: { slidesToShow: 1 } }, { breakpoint: 1024, settings: { slidesToShow: 2 } }, { breakpoint: 1548, settings: { slidesToShow: 3 } }, { breakpoint: 2072, settings: { slidesToShow: 4 } }, { breakpoint: 10000, settings: 'unslick' }],
	        slidesToScroll: 1,
	        autoplay: false,
	        centerMode: false,
	        arrows: elements.length > 1
	      };
	
	      return _react2.default.createElement(
	        _reactSlick2.default,
	        settings,
	        elements.map(function (el) {
	          return CarouselElement(el);
	        })
	      );
	    }
	  }]);
	
	  return CarouselMessage;
	}(_react.Component);
	
	exports.default = CarouselMessage;

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	module.exports = __webpack_require__(58);

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _innerSlider = __webpack_require__(59);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _json2mq = __webpack_require__(77);
	
	var _json2mq2 = _interopRequireDefault(_json2mq);
	
	var _defaultProps = __webpack_require__(66);
	
	var _defaultProps2 = _interopRequireDefault(_defaultProps);
	
	var _canUseDom = __webpack_require__(79);
	
	var _canUseDom2 = _interopRequireDefault(_canUseDom);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var enquire = _canUseDom2.default && __webpack_require__(80);
	
	var Slider = function (_React$Component) {
	  _inherits(Slider, _React$Component);
	
	  function Slider(props) {
	    _classCallCheck(this, Slider);
	
	    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));
	
	    _this.state = {
	      breakpoint: null
	    };
	    _this._responsiveMediaHandlers = [];
	    _this.innerSliderRefHandler = _this.innerSliderRefHandler.bind(_this);
	    return _this;
	  }
	
	  Slider.prototype.innerSliderRefHandler = function innerSliderRefHandler(ref) {
	    this.innerSlider = ref;
	  };
	
	  Slider.prototype.media = function media(query, handler) {
	    enquire.register(query, handler);
	    this._responsiveMediaHandlers.push({ query: query, handler: handler });
	  };
	
	  Slider.prototype.componentWillMount = function componentWillMount() {
	    var _this2 = this;
	
	    if (this.props.responsive) {
	      var breakpoints = this.props.responsive.map(function (breakpt) {
	        return breakpt.breakpoint;
	      });
	      breakpoints.sort(function (x, y) {
	        return x - y;
	      });
	
	      breakpoints.forEach(function (breakpoint, index) {
	        var bQuery;
	        if (index === 0) {
	          bQuery = (0, _json2mq2.default)({ minWidth: 0, maxWidth: breakpoint });
	        } else {
	          bQuery = (0, _json2mq2.default)({ minWidth: breakpoints[index - 1], maxWidth: breakpoint });
	        }
	        _canUseDom2.default && _this2.media(bQuery, function () {
	          _this2.setState({ breakpoint: breakpoint });
	        });
	      });
	
	      // Register media query for full screen. Need to support resize from small to large
	      var query = (0, _json2mq2.default)({ minWidth: breakpoints.slice(-1)[0] });
	
	      _canUseDom2.default && this.media(query, function () {
	        _this2.setState({ breakpoint: null });
	      });
	    }
	  };
	
	  Slider.prototype.componentWillUnmount = function componentWillUnmount() {
	    this._responsiveMediaHandlers.forEach(function (obj) {
	      enquire.unregister(obj.query, obj.handler);
	    });
	  };
	
	  Slider.prototype.slickPrev = function slickPrev() {
	    this.innerSlider.slickPrev();
	  };
	
	  Slider.prototype.slickNext = function slickNext() {
	    this.innerSlider.slickNext();
	  };
	
	  Slider.prototype.slickGoTo = function slickGoTo(slide) {
	    this.innerSlider.slickGoTo(slide);
	  };
	
	  Slider.prototype.render = function render() {
	    var _this3 = this;
	
	    var settings;
	    var newProps;
	    if (this.state.breakpoint) {
	      newProps = this.props.responsive.filter(function (resp) {
	        return resp.breakpoint === _this3.state.breakpoint;
	      });
	      settings = newProps[0].settings === 'unslick' ? 'unslick' : (0, _objectAssign2.default)({}, this.props, newProps[0].settings);
	    } else {
	      settings = (0, _objectAssign2.default)({}, _defaultProps2.default, this.props);
	    }
	
	    var children = this.props.children;
	    if (!Array.isArray(children)) {
	      children = [children];
	    }
	
	    // Children may contain false or null, so we should filter them
	    children = children.filter(function (child) {
	      return !!child;
	    });
	
	    if (settings === 'unslick') {
	      // if 'unslick' responsive breakpoint setting used, just return the <Slider> tag nested HTML
	      return _react2.default.createElement(
	        'div',
	        { className: this.props.className + ' unslicked' },
	        children
	      );
	    } else {
	      return _react2.default.createElement(
	        _innerSlider.InnerSlider,
	        _extends({ ref: this.innerSliderRefHandler }, settings),
	        children
	      );
	    }
	  };
	
	  return Slider;
	}(_react2.default.Component);
	
	exports.default = Slider;

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	exports.__esModule = true;
	exports.InnerSlider = undefined;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _eventHandlers = __webpack_require__(61);
	
	var _eventHandlers2 = _interopRequireDefault(_eventHandlers);
	
	var _helpers = __webpack_require__(64);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	var _initialState = __webpack_require__(65);
	
	var _initialState2 = _interopRequireDefault(_initialState);
	
	var _defaultProps = __webpack_require__(66);
	
	var _defaultProps2 = _interopRequireDefault(_defaultProps);
	
	var _createReactClass = __webpack_require__(67);
	
	var _createReactClass2 = _interopRequireDefault(_createReactClass);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _track = __webpack_require__(74);
	
	var _dots = __webpack_require__(75);
	
	var _arrows = __webpack_require__(76);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var InnerSlider = exports.InnerSlider = (0, _createReactClass2.default)({
	  displayName: 'InnerSlider',
	
	  mixins: [_helpers2.default, _eventHandlers2.default],
	  list: null,
	  track: null,
	  listRefHandler: function listRefHandler(ref) {
	    this.list = ref;
	  },
	  trackRefHandler: function trackRefHandler(ref) {
	    this.track = ref;
	  },
	  getInitialState: function getInitialState() {
	    return _extends({}, _initialState2.default, {
	      currentSlide: this.props.initialSlide
	    });
	  },
	  getDefaultProps: function getDefaultProps() {
	    return _defaultProps2.default;
	  },
	  componentWillMount: function componentWillMount() {
	    if (this.props.init) {
	      this.props.init();
	    }
	    this.setState({
	      mounted: true
	    });
	    var lazyLoadedList = [];
	    for (var i = 0; i < _react2.default.Children.count(this.props.children); i++) {
	      if (i >= this.state.currentSlide && i < this.state.currentSlide + this.props.slidesToShow) {
	        lazyLoadedList.push(i);
	      }
	    }
	
	    if (this.props.lazyLoad && this.state.lazyLoadedList.length === 0) {
	      this.setState({
	        lazyLoadedList: lazyLoadedList
	      });
	    }
	  },
	  componentDidMount: function componentDidMount() {
	    // Hack for autoplay -- Inspect Later
	    this.initialize(this.props);
	    this.adaptHeight();
	
	    // To support server-side rendering
	    if (!window) {
	      return;
	    }
	    if (window.addEventListener) {
	      window.addEventListener('resize', this.onWindowResized);
	    } else {
	      window.attachEvent('onresize', this.onWindowResized);
	    }
	  },
	  componentWillUnmount: function componentWillUnmount() {
	    if (this.animationEndCallback) {
	      clearTimeout(this.animationEndCallback);
	    }
	    if (window.addEventListener) {
	      window.removeEventListener('resize', this.onWindowResized);
	    } else {
	      window.detachEvent('onresize', this.onWindowResized);
	    }
	    if (this.state.autoPlayTimer) {
	      clearInterval(this.state.autoPlayTimer);
	    }
	  },
	  componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
	    if (this.props.slickGoTo != nextProps.slickGoTo) {
	      if (process.env.NODE_ENV !== 'production') {
	        console.warn('react-slick deprecation warning: slickGoTo prop is deprecated and it will be removed in next release. Use slickGoTo method instead');
	      }
	      this.changeSlide({
	        message: 'index',
	        index: nextProps.slickGoTo,
	        currentSlide: this.state.currentSlide
	      });
	    } else if (this.state.currentSlide >= nextProps.children.length) {
	      this.update(nextProps);
	      this.changeSlide({
	        message: 'index',
	        index: nextProps.children.length - nextProps.slidesToShow,
	        currentSlide: this.state.currentSlide
	      });
	    } else {
	      this.update(nextProps);
	    }
	  },
	  componentDidUpdate: function componentDidUpdate() {
	    this.adaptHeight();
	  },
	  onWindowResized: function onWindowResized() {
	    this.update(this.props);
	    // animating state should be cleared while resizing, otherwise autoplay stops working
	    this.setState({
	      animating: false
	    });
	    clearTimeout(this.animationEndCallback);
	    delete this.animationEndCallback;
	  },
	  slickPrev: function slickPrev() {
	    this.changeSlide({ message: 'previous' });
	  },
	  slickNext: function slickNext() {
	    this.changeSlide({ message: 'next' });
	  },
	  slickGoTo: function slickGoTo(slide) {
	    slide = Number(slide);
	    !isNaN(slide) && this.changeSlide({
	      message: 'index',
	      index: slide,
	      currentSlide: this.state.currentSlide
	    });
	  },
	  render: function render() {
	    var className = (0, _classnames2.default)('slick-initialized', 'slick-slider', this.props.className, {
	      'slick-vertical': this.props.vertical
	    });
	
	    var trackProps = {
	      fade: this.props.fade,
	      cssEase: this.props.cssEase,
	      speed: this.props.speed,
	      infinite: this.props.infinite,
	      centerMode: this.props.centerMode,
	      focusOnSelect: this.props.focusOnSelect ? this.selectHandler : null,
	      currentSlide: this.state.currentSlide,
	      lazyLoad: this.props.lazyLoad,
	      lazyLoadedList: this.state.lazyLoadedList,
	      rtl: this.props.rtl,
	      slideWidth: this.state.slideWidth,
	      slidesToShow: this.props.slidesToShow,
	      slidesToScroll: this.props.slidesToScroll,
	      slideCount: this.state.slideCount,
	      trackStyle: this.state.trackStyle,
	      variableWidth: this.props.variableWidth
	    };
	
	    var dots;
	
	    if (this.props.dots === true && this.state.slideCount >= this.props.slidesToShow) {
	      var dotProps = {
	        dotsClass: this.props.dotsClass,
	        slideCount: this.state.slideCount,
	        slidesToShow: this.props.slidesToShow,
	        currentSlide: this.state.currentSlide,
	        slidesToScroll: this.props.slidesToScroll,
	        clickHandler: this.changeSlide,
	        children: this.props.children,
	        customPaging: this.props.customPaging
	      };
	
	      dots = _react2.default.createElement(_dots.Dots, dotProps);
	    }
	
	    var prevArrow, nextArrow;
	
	    var arrowProps = {
	      infinite: this.props.infinite,
	      centerMode: this.props.centerMode,
	      currentSlide: this.state.currentSlide,
	      slideCount: this.state.slideCount,
	      slidesToShow: this.props.slidesToShow,
	      prevArrow: this.props.prevArrow,
	      nextArrow: this.props.nextArrow,
	      clickHandler: this.changeSlide
	    };
	
	    if (this.props.arrows) {
	      prevArrow = _react2.default.createElement(_arrows.PrevArrow, arrowProps);
	      nextArrow = _react2.default.createElement(_arrows.NextArrow, arrowProps);
	    }
	
	    var verticalHeightStyle = null;
	
	    if (this.props.vertical) {
	      verticalHeightStyle = {
	        height: this.state.listHeight
	      };
	    }
	
	    var centerPaddingStyle = null;
	
	    if (this.props.vertical === false) {
	      if (this.props.centerMode === true) {
	        centerPaddingStyle = {
	          padding: '0px ' + this.props.centerPadding
	        };
	      }
	    } else {
	      if (this.props.centerMode === true) {
	        centerPaddingStyle = {
	          padding: this.props.centerPadding + ' 0px'
	        };
	      }
	    }
	
	    var listStyle = (0, _objectAssign2.default)({}, verticalHeightStyle, centerPaddingStyle);
	
	    return _react2.default.createElement(
	      'div',
	      {
	        className: className,
	        onMouseEnter: this.onInnerSliderEnter,
	        onMouseLeave: this.onInnerSliderLeave,
	        onMouseOver: this.onInnerSliderOver
	      },
	      prevArrow,
	      _react2.default.createElement(
	        'div',
	        {
	          ref: this.listRefHandler,
	          className: 'slick-list',
	          style: listStyle,
	          onMouseDown: this.swipeStart,
	          onMouseMove: this.state.dragging ? this.swipeMove : null,
	          onMouseUp: this.swipeEnd,
	          onMouseLeave: this.state.dragging ? this.swipeEnd : null,
	          onTouchStart: this.swipeStart,
	          onTouchMove: this.state.dragging ? this.swipeMove : null,
	          onTouchEnd: this.swipeEnd,
	          onTouchCancel: this.state.dragging ? this.swipeEnd : null,
	          onKeyDown: this.props.accessibility ? this.keyHandler : null },
	        _react2.default.createElement(
	          _track.Track,
	          _extends({ ref: this.trackRefHandler }, trackProps),
	          this.props.children
	        )
	      ),
	      nextArrow,
	      dots
	    );
	  }
	});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 60 */
/***/ (function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	
	
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	
	
	
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;
	
	process.listeners = function (name) { return [] }
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	var _trackHelper = __webpack_require__(62);
	
	var _helpers = __webpack_require__(64);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _reactDom = __webpack_require__(9);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var EventHandlers = {
	  // Event handler for previous and next
	  changeSlide: function changeSlide(options) {
	    var indexOffset, previousInt, slideOffset, unevenOffset, targetSlide;
	    var _props = this.props,
	        slidesToScroll = _props.slidesToScroll,
	        slidesToShow = _props.slidesToShow;
	    var _state = this.state,
	        slideCount = _state.slideCount,
	        currentSlide = _state.currentSlide;
	
	    unevenOffset = slideCount % slidesToScroll !== 0;
	    indexOffset = unevenOffset ? 0 : (slideCount - currentSlide) % slidesToScroll;
	
	    if (options.message === 'previous') {
	      slideOffset = indexOffset === 0 ? slidesToScroll : slidesToShow - indexOffset;
	      targetSlide = currentSlide - slideOffset;
	      if (this.props.lazyLoad) {
	        previousInt = currentSlide - slideOffset;
	        targetSlide = previousInt === -1 ? slideCount - 1 : previousInt;
	      }
	    } else if (options.message === 'next') {
	      slideOffset = indexOffset === 0 ? slidesToScroll : indexOffset;
	      targetSlide = currentSlide + slideOffset;
	      if (this.props.lazyLoad) {
	        targetSlide = (currentSlide + slidesToScroll) % slideCount + indexOffset;
	      }
	    } else if (options.message === 'dots' || options.message === 'children') {
	      // Click on dots
	      targetSlide = options.index * options.slidesToScroll;
	      if (targetSlide === options.currentSlide) {
	        return;
	      }
	    } else if (options.message === 'index') {
	      targetSlide = Number(options.index);
	      if (targetSlide === options.currentSlide) {
	        return;
	      }
	    }
	
	    this.slideHandler(targetSlide);
	  },
	
	  // Accessiblity handler for previous and next
	  keyHandler: function keyHandler(e) {
	    //Dont slide if the cursor is inside the form fields and arrow keys are pressed
	    if (!e.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
	      if (e.keyCode === 37 && this.props.accessibility === true) {
	        this.changeSlide({
	          message: this.props.rtl === true ? 'next' : 'previous'
	        });
	      } else if (e.keyCode === 39 && this.props.accessibility === true) {
	        this.changeSlide({
	          message: this.props.rtl === true ? 'previous' : 'next'
	        });
	      }
	    }
	  },
	  // Focus on selecting a slide (click handler on track)
	  selectHandler: function selectHandler(options) {
	    this.changeSlide(options);
	  },
	  swipeStart: function swipeStart(e) {
	    var touches, posX, posY;
	
	    if (this.props.swipe === false || 'ontouchend' in document && this.props.swipe === false) {
	      return;
	    } else if (this.props.draggable === false && e.type.indexOf('mouse') !== -1) {
	      return;
	    }
	    posX = e.touches !== undefined ? e.touches[0].pageX : e.clientX;
	    posY = e.touches !== undefined ? e.touches[0].pageY : e.clientY;
	    this.setState({
	      dragging: true,
	      touchObject: {
	        startX: posX,
	        startY: posY,
	        curX: posX,
	        curY: posY
	      }
	    });
	  },
	  swipeMove: function swipeMove(e) {
	    if (!this.state.dragging) {
	      e.preventDefault();
	      return;
	    }
	    if (this.state.scrolling) {
	      return;
	    }
	    if (this.state.animating) {
	      e.preventDefault();
	      return;
	    }
	    if (this.props.vertical && this.props.swipeToSlide && this.props.verticalSwiping) {
	      e.preventDefault();
	    }
	    var swipeLeft;
	    var curLeft, positionOffset;
	    var touchObject = this.state.touchObject;
	
	    curLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	      slideIndex: this.state.currentSlide,
	      trackRef: this.track
	    }, this.props, this.state));
	    touchObject.curX = e.touches ? e.touches[0].pageX : e.clientX;
	    touchObject.curY = e.touches ? e.touches[0].pageY : e.clientY;
	    touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(touchObject.curX - touchObject.startX, 2)));
	    var verticalSwipeLength = Math.round(Math.sqrt(Math.pow(touchObject.curY - touchObject.startY, 2)));
	
	    if (!this.props.verticalSwiping && !this.state.swiping && verticalSwipeLength > 4) {
	      this.setState({
	        scrolling: true
	      });
	      return;
	    }
	
	    if (this.props.verticalSwiping) {
	      touchObject.swipeLength = verticalSwipeLength;
	    }
	
	    positionOffset = (this.props.rtl === false ? 1 : -1) * (touchObject.curX > touchObject.startX ? 1 : -1);
	
	    if (this.props.verticalSwiping) {
	      positionOffset = touchObject.curY > touchObject.startY ? 1 : -1;
	    }
	
	    var currentSlide = this.state.currentSlide;
	    var dotCount = Math.ceil(this.state.slideCount / this.props.slidesToScroll);
	    var swipeDirection = this.swipeDirection(this.state.touchObject);
	    var touchSwipeLength = touchObject.swipeLength;
	
	    if (this.props.infinite === false) {
	      if (currentSlide === 0 && swipeDirection === 'right' || currentSlide + 1 >= dotCount && swipeDirection === 'left') {
	        touchSwipeLength = touchObject.swipeLength * this.props.edgeFriction;
	
	        if (this.state.edgeDragged === false && this.props.edgeEvent) {
	          this.props.edgeEvent(swipeDirection);
	          this.setState({ edgeDragged: true });
	        }
	      }
	    }
	
	    if (this.state.swiped === false && this.props.swipeEvent) {
	      this.props.swipeEvent(swipeDirection);
	      this.setState({ swiped: true });
	    }
	
	    if (!this.props.vertical) {
	      swipeLeft = curLeft + touchSwipeLength * positionOffset;
	    } else {
	      swipeLeft = curLeft + touchSwipeLength * (this.state.listHeight / this.state.listWidth) * positionOffset;
	    }
	
	    if (this.props.verticalSwiping) {
	      swipeLeft = curLeft + touchSwipeLength * positionOffset;
	    }
	
	    this.setState({
	      touchObject: touchObject,
	      swipeLeft: swipeLeft,
	      trackStyle: (0, _trackHelper.getTrackCSS)((0, _objectAssign2.default)({ left: swipeLeft }, this.props, this.state))
	    });
	
	    if (Math.abs(touchObject.curX - touchObject.startX) < Math.abs(touchObject.curY - touchObject.startY) * 0.8) {
	      return;
	    }
	    if (touchObject.swipeLength > 4) {
	      this.setState({
	        swiping: true
	      });
	      e.preventDefault();
	    }
	  },
	  getNavigableIndexes: function getNavigableIndexes() {
	    var max = void 0;
	    var breakPoint = 0;
	    var counter = 0;
	    var indexes = [];
	
	    if (!this.props.infinite) {
	      max = this.state.slideCount;
	    } else {
	      breakPoint = this.props.slidesToShow * -1;
	      counter = this.props.slidesToShow * -1;
	      max = this.state.slideCount * 2;
	    }
	
	    while (breakPoint < max) {
	      indexes.push(breakPoint);
	      breakPoint = counter + this.props.slidesToScroll;
	
	      counter += this.props.slidesToScroll <= this.props.slidesToShow ? this.props.slidesToScroll : this.props.slidesToShow;
	    }
	
	    return indexes;
	  },
	  checkNavigable: function checkNavigable(index) {
	    var navigables = this.getNavigableIndexes();
	    var prevNavigable = 0;
	
	    if (index > navigables[navigables.length - 1]) {
	      index = navigables[navigables.length - 1];
	    } else {
	      for (var n in navigables) {
	        if (index < navigables[n]) {
	          index = prevNavigable;
	          break;
	        }
	
	        prevNavigable = navigables[n];
	      }
	    }
	
	    return index;
	  },
	  getSlideCount: function getSlideCount() {
	    var _this = this;
	
	    var centerOffset = this.props.centerMode ? this.state.slideWidth * Math.floor(this.props.slidesToShow / 2) : 0;
	
	    if (this.props.swipeToSlide) {
	      var swipedSlide = void 0;
	
	      var slickList = _reactDom2.default.findDOMNode(this.list);
	
	      var slides = slickList.querySelectorAll('.slick-slide');
	
	      Array.from(slides).every(function (slide) {
	        if (!_this.props.vertical) {
	          if (slide.offsetLeft - centerOffset + _this.getWidth(slide) / 2 > _this.state.swipeLeft * -1) {
	            swipedSlide = slide;
	            return false;
	          }
	        } else {
	          if (slide.offsetTop + _this.getHeight(slide) / 2 > _this.state.swipeLeft * -1) {
	            swipedSlide = slide;
	            return false;
	          }
	        }
	
	        return true;
	      });
	
	      var slidesTraversed = Math.abs(swipedSlide.dataset.index - this.state.currentSlide) || 1;
	
	      return slidesTraversed;
	    } else {
	      return this.props.slidesToScroll;
	    }
	  },
	
	  swipeEnd: function swipeEnd(e) {
	    if (!this.state.dragging) {
	      if (this.props.swipe) {
	        e.preventDefault();
	      }
	      return;
	    }
	    var touchObject = this.state.touchObject;
	    var minSwipe = this.state.listWidth / this.props.touchThreshold;
	    var swipeDirection = this.swipeDirection(touchObject);
	
	    if (this.props.verticalSwiping) {
	      minSwipe = this.state.listHeight / this.props.touchThreshold;
	    }
	
	    var wasScrolling = this.state.scrolling;
	    // reset the state of touch related state variables.
	    this.setState({
	      dragging: false,
	      edgeDragged: false,
	      scrolling: false,
	      swiping: false,
	      swiped: false,
	      swipeLeft: null,
	      touchObject: {}
	    });
	    if (wasScrolling) {
	      return;
	    }
	
	    // Fix for #13
	    if (!touchObject.swipeLength) {
	      return;
	    }
	    if (touchObject.swipeLength > minSwipe) {
	      e.preventDefault();
	
	      var slideCount = void 0,
	          newSlide = void 0;
	
	      switch (swipeDirection) {
	
	        case 'left':
	        case 'down':
	          newSlide = this.state.currentSlide + this.getSlideCount();
	          slideCount = this.props.swipeToSlide ? this.checkNavigable(newSlide) : newSlide;
	          this.state.currentDirection = 0;
	          break;
	
	        case 'right':
	        case 'up':
	          newSlide = this.state.currentSlide - this.getSlideCount();
	          slideCount = this.props.swipeToSlide ? this.checkNavigable(newSlide) : newSlide;
	          this.state.currentDirection = 1;
	          break;
	
	        default:
	          slideCount = this.state.currentSlide;
	
	      }
	
	      this.slideHandler(slideCount);
	    } else {
	      // Adjust the track back to it's original position.
	      var currentLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	        slideIndex: this.state.currentSlide,
	        trackRef: this.track
	      }, this.props, this.state));
	
	      this.setState({
	        trackStyle: (0, _trackHelper.getTrackAnimateCSS)((0, _objectAssign2.default)({ left: currentLeft }, this.props, this.state))
	      });
	    }
	  },
	  onInnerSliderEnter: function onInnerSliderEnter(e) {
	    if (this.props.autoplay && this.props.pauseOnHover) {
	      this.pause();
	    }
	  },
	  onInnerSliderOver: function onInnerSliderOver(e) {
	    if (this.props.autoplay && this.props.pauseOnHover) {
	      this.pause();
	    }
	  },
	  onInnerSliderLeave: function onInnerSliderLeave(e) {
	    if (this.props.autoplay && this.props.pauseOnHover) {
	      this.autoPlay();
	    }
	  }
	};
	
	exports.default = EventHandlers;

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports.getTrackLeft = exports.getTrackAnimateCSS = exports.getTrackCSS = undefined;
	
	var _reactDom = __webpack_require__(9);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var checkSpecKeys = function checkSpecKeys(spec, keysArray) {
	  return keysArray.reduce(function (value, key) {
	    return value && spec.hasOwnProperty(key);
	  }, true) ? null : console.error('Keys Missing', spec);
	};
	
	var getTrackCSS = exports.getTrackCSS = function getTrackCSS(spec) {
	  checkSpecKeys(spec, ['left', 'variableWidth', 'slideCount', 'slidesToShow', 'slideWidth']);
	
	  var trackWidth, trackHeight;
	
	  var trackChildren = spec.slideCount + 2 * spec.slidesToShow;
	
	  if (!spec.vertical) {
	    if (spec.variableWidth) {
	      trackWidth = (spec.slideCount + 2 * spec.slidesToShow) * spec.slideWidth;
	    } else if (spec.centerMode) {
	      trackWidth = (spec.slideCount + 2 * (spec.slidesToShow + 1)) * spec.slideWidth;
	    } else {
	      trackWidth = (spec.slideCount + 2 * spec.slidesToShow) * spec.slideWidth;
	    }
	  } else {
	    trackHeight = trackChildren * spec.slideHeight;
	  }
	
	  var style = {
	    opacity: 1,
	    WebkitTransform: !spec.vertical ? 'translate3d(' + spec.left + 'px, 0px, 0px)' : 'translate3d(0px, ' + spec.left + 'px, 0px)',
	    transform: !spec.vertical ? 'translate3d(' + spec.left + 'px, 0px, 0px)' : 'translate3d(0px, ' + spec.left + 'px, 0px)',
	    transition: '',
	    WebkitTransition: '',
	    msTransform: !spec.vertical ? 'translateX(' + spec.left + 'px)' : 'translateY(' + spec.left + 'px)'
	  };
	
	  if (trackWidth) {
	    (0, _objectAssign2.default)(style, { width: trackWidth });
	  }
	
	  if (trackHeight) {
	    (0, _objectAssign2.default)(style, { height: trackHeight });
	  }
	
	  // Fallback for IE8
	  if (window && !window.addEventListener && window.attachEvent) {
	    if (!spec.vertical) {
	      style.marginLeft = spec.left + 'px';
	    } else {
	      style.marginTop = spec.left + 'px';
	    }
	  }
	
	  return style;
	};
	
	var getTrackAnimateCSS = exports.getTrackAnimateCSS = function getTrackAnimateCSS(spec) {
	  checkSpecKeys(spec, ['left', 'variableWidth', 'slideCount', 'slidesToShow', 'slideWidth', 'speed', 'cssEase']);
	
	  var style = getTrackCSS(spec);
	  // useCSS is true by default so it can be undefined
	  style.WebkitTransition = '-webkit-transform ' + spec.speed + 'ms ' + spec.cssEase;
	  style.transition = 'transform ' + spec.speed + 'ms ' + spec.cssEase;
	  return style;
	};
	
	var getTrackLeft = exports.getTrackLeft = function getTrackLeft(spec) {
	
	  checkSpecKeys(spec, ['slideIndex', 'trackRef', 'infinite', 'centerMode', 'slideCount', 'slidesToShow', 'slidesToScroll', 'slideWidth', 'listWidth', 'variableWidth', 'slideHeight']);
	
	  var slideOffset = 0;
	  var targetLeft;
	  var targetSlide;
	  var verticalOffset = 0;
	
	  if (spec.fade) {
	    return 0;
	  }
	
	  if (spec.infinite) {
	    if (spec.slideCount >= spec.slidesToShow) {
	      slideOffset = spec.slideWidth * spec.slidesToShow * -1;
	      verticalOffset = spec.slideHeight * spec.slidesToShow * -1;
	    }
	    if (spec.slideCount % spec.slidesToScroll !== 0) {
	      if (spec.slideIndex + spec.slidesToScroll > spec.slideCount && spec.slideCount > spec.slidesToShow) {
	        if (spec.slideIndex > spec.slideCount) {
	          slideOffset = (spec.slidesToShow - (spec.slideIndex - spec.slideCount)) * spec.slideWidth * -1;
	          verticalOffset = (spec.slidesToShow - (spec.slideIndex - spec.slideCount)) * spec.slideHeight * -1;
	        } else {
	          slideOffset = spec.slideCount % spec.slidesToScroll * spec.slideWidth * -1;
	          verticalOffset = spec.slideCount % spec.slidesToScroll * spec.slideHeight * -1;
	        }
	      }
	    }
	  } else {
	
	    if (spec.slideCount % spec.slidesToScroll !== 0) {
	      if (spec.slideIndex + spec.slidesToScroll > spec.slideCount && spec.slideCount > spec.slidesToShow) {
	        var slidesToOffset = spec.slidesToShow - spec.slideCount % spec.slidesToScroll;
	        slideOffset = slidesToOffset * spec.slideWidth;
	      }
	    }
	  }
	
	  if (spec.centerMode) {
	    if (spec.infinite) {
	      slideOffset += spec.slideWidth * Math.floor(spec.slidesToShow / 2);
	    } else {
	      slideOffset = spec.slideWidth * Math.floor(spec.slidesToShow / 2);
	    }
	  }
	
	  if (!spec.vertical) {
	    targetLeft = spec.slideIndex * spec.slideWidth * -1 + slideOffset;
	  } else {
	    targetLeft = spec.slideIndex * spec.slideHeight * -1 + verticalOffset;
	  }
	
	  if (spec.variableWidth === true) {
	    var targetSlideIndex;
	    if (spec.slideCount <= spec.slidesToShow || spec.infinite === false) {
	      targetSlide = _reactDom2.default.findDOMNode(spec.trackRef).childNodes[spec.slideIndex];
	    } else {
	      targetSlideIndex = spec.slideIndex + spec.slidesToShow;
	      targetSlide = _reactDom2.default.findDOMNode(spec.trackRef).childNodes[targetSlideIndex];
	    }
	    targetLeft = targetSlide ? targetSlide.offsetLeft * -1 : 0;
	    if (spec.centerMode === true) {
	      if (spec.infinite === false) {
	        targetSlide = _reactDom2.default.findDOMNode(spec.trackRef).children[spec.slideIndex];
	      } else {
	        targetSlide = _reactDom2.default.findDOMNode(spec.trackRef).children[spec.slideIndex + spec.slidesToShow + 1];
	      }
	
	      if (targetSlide) {
	        targetLeft = targetSlide.offsetLeft * -1 + (spec.listWidth - targetSlide.offsetWidth) / 2;
	      }
	    }
	  }
	
	  return targetLeft;
	};

/***/ }),
/* 63 */
/***/ (function(module, exports) {

	/*
	object-assign
	(c) Sindre Sorhus
	@license MIT
	*/
	
	'use strict';
	/* eslint-disable no-unused-vars */
	var getOwnPropertySymbols = Object.getOwnPropertySymbols;
	var hasOwnProperty = Object.prototype.hasOwnProperty;
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;
	
	function toObject(val) {
		if (val === null || val === undefined) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}
	
		return Object(val);
	}
	
	function shouldUseNative() {
		try {
			if (!Object.assign) {
				return false;
			}
	
			// Detect buggy property enumeration order in older V8 versions.
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=4118
			var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
			test1[5] = 'de';
			if (Object.getOwnPropertyNames(test1)[0] === '5') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test2 = {};
			for (var i = 0; i < 10; i++) {
				test2['_' + String.fromCharCode(i)] = i;
			}
			var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
				return test2[n];
			});
			if (order2.join('') !== '0123456789') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test3 = {};
			'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
				test3[letter] = letter;
			});
			if (Object.keys(Object.assign({}, test3)).join('') !==
					'abcdefghijklmnopqrst') {
				return false;
			}
	
			return true;
		} catch (err) {
			// We don't expect any of the above to throw, but better to be safe.
			return false;
		}
	}
	
	module.exports = shouldUseNative() ? Object.assign : function (target, source) {
		var from;
		var to = toObject(target);
		var symbols;
	
		for (var s = 1; s < arguments.length; s++) {
			from = Object(arguments[s]);
	
			for (var key in from) {
				if (hasOwnProperty.call(from, key)) {
					to[key] = from[key];
				}
			}
	
			if (getOwnPropertySymbols) {
				symbols = getOwnPropertySymbols(from);
				for (var i = 0; i < symbols.length; i++) {
					if (propIsEnumerable.call(from, symbols[i])) {
						to[symbols[i]] = from[symbols[i]];
					}
				}
			}
		}
	
		return to;
	};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactDom = __webpack_require__(9);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _trackHelper = __webpack_require__(62);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var helpers = {
	  initialize: function initialize(props) {
	    var slickList = _reactDom2.default.findDOMNode(this.list);
	
	    var slideCount = _react2.default.Children.count(props.children);
	    var listWidth = this.getWidth(slickList);
	    var trackWidth = this.getWidth(_reactDom2.default.findDOMNode(this.track));
	    var slideWidth;
	
	    if (!props.vertical) {
	      var centerPaddingAdj = props.centerMode && parseInt(props.centerPadding) * 2;
	      slideWidth = (this.getWidth(_reactDom2.default.findDOMNode(this)) - centerPaddingAdj) / props.slidesToShow;
	    } else {
	      slideWidth = this.getWidth(_reactDom2.default.findDOMNode(this));
	    }
	
	    var slideHeight = this.getHeight(slickList.querySelector('[data-index="0"]'));
	    var listHeight = slideHeight * props.slidesToShow;
	
	    var currentSlide = props.rtl ? slideCount - 1 - props.initialSlide : props.initialSlide;
	
	    this.setState({
	      slideCount: slideCount,
	      slideWidth: slideWidth,
	      listWidth: listWidth,
	      trackWidth: trackWidth,
	      currentSlide: currentSlide,
	      slideHeight: slideHeight,
	      listHeight: listHeight
	    }, function () {
	
	      var targetLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	        slideIndex: this.state.currentSlide,
	        trackRef: this.track
	      }, props, this.state));
	      // getCSS function needs previously set state
	      var trackStyle = (0, _trackHelper.getTrackCSS)((0, _objectAssign2.default)({ left: targetLeft }, props, this.state));
	
	      this.setState({ trackStyle: trackStyle });
	
	      this.autoPlay(); // once we're set up, trigger the initial autoplay.
	    });
	  },
	  update: function update(props) {
	    var slickList = _reactDom2.default.findDOMNode(this.list);
	    // This method has mostly same code as initialize method.
	    // Refactor it
	    var slideCount = _react2.default.Children.count(props.children);
	    var listWidth = this.getWidth(slickList);
	    var trackWidth = this.getWidth(_reactDom2.default.findDOMNode(this.track));
	    var slideWidth;
	
	    if (!props.vertical) {
	      var centerPaddingAdj = props.centerMode && parseInt(props.centerPadding) * 2;
	      slideWidth = (this.getWidth(_reactDom2.default.findDOMNode(this)) - centerPaddingAdj) / props.slidesToShow;
	    } else {
	      slideWidth = this.getWidth(_reactDom2.default.findDOMNode(this));
	    }
	
	    var slideHeight = this.getHeight(slickList.querySelector('[data-index="0"]'));
	    var listHeight = slideHeight * props.slidesToShow;
	
	    // pause slider if autoplay is set to false
	    if (!props.autoplay) {
	      this.pause();
	    } else {
	      this.autoPlay();
	    }
	
	    this.setState({
	      slideCount: slideCount,
	      slideWidth: slideWidth,
	      listWidth: listWidth,
	      trackWidth: trackWidth,
	      slideHeight: slideHeight,
	      listHeight: listHeight
	    }, function () {
	
	      var targetLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	        slideIndex: this.state.currentSlide,
	        trackRef: this.track
	      }, props, this.state));
	      // getCSS function needs previously set state
	      var trackStyle = (0, _trackHelper.getTrackCSS)((0, _objectAssign2.default)({ left: targetLeft }, props, this.state));
	
	      this.setState({ trackStyle: trackStyle });
	    });
	  },
	  getWidth: function getWidth(elem) {
	    return elem && (elem.getBoundingClientRect().width || elem.offsetWidth) || 0;
	  },
	  getHeight: function getHeight(elem) {
	    return elem && (elem.getBoundingClientRect().height || elem.offsetHeight) || 0;
	  },
	
	  adaptHeight: function adaptHeight() {
	    if (this.props.adaptiveHeight) {
	      var selector = '[data-index="' + this.state.currentSlide + '"]';
	      if (this.list) {
	        var slickList = _reactDom2.default.findDOMNode(this.list);
	        slickList.style.height = slickList.querySelector(selector).offsetHeight + 'px';
	      }
	    }
	  },
	  canGoNext: function canGoNext(opts) {
	    var canGo = true;
	    if (!opts.infinite) {
	      if (opts.centerMode) {
	        // check if current slide is last slide
	        if (opts.currentSlide >= opts.slideCount - 1) {
	          canGo = false;
	        }
	      } else {
	        // check if all slides are shown in slider
	        if (opts.slideCount <= opts.slidesToShow || opts.currentSlide >= opts.slideCount - opts.slidesToShow) {
	          canGo = false;
	        }
	      }
	    }
	    return canGo;
	  },
	  slideHandler: function slideHandler(index) {
	    var _this = this;
	
	    // Functionality of animateSlide and postSlide is merged into this function
	    // console.log('slideHandler', index);
	    var targetSlide, currentSlide;
	    var targetLeft, currentLeft;
	    var callback;
	
	    if (this.props.waitForAnimate && this.state.animating) {
	      return;
	    }
	
	    if (this.props.fade) {
	      currentSlide = this.state.currentSlide;
	
	      // Don't change slide if it's not infite and current slide is the first or last slide.
	      if (this.props.infinite === false && (index < 0 || index >= this.state.slideCount)) {
	        return;
	      }
	
	      //  Shifting targetSlide back into the range
	      if (index < 0) {
	        targetSlide = index + this.state.slideCount;
	      } else if (index >= this.state.slideCount) {
	        targetSlide = index - this.state.slideCount;
	      } else {
	        targetSlide = index;
	      }
	
	      if (this.props.lazyLoad && this.state.lazyLoadedList.indexOf(targetSlide) < 0) {
	        this.setState({
	          lazyLoadedList: this.state.lazyLoadedList.concat(targetSlide)
	        });
	      }
	
	      callback = function callback() {
	        _this.setState({
	          animating: false
	        });
	        if (_this.props.afterChange) {
	          _this.props.afterChange(targetSlide);
	        }
	        delete _this.animationEndCallback;
	      };
	
	      this.setState({
	        animating: true,
	        currentSlide: targetSlide
	      }, function () {
	        this.animationEndCallback = setTimeout(callback, this.props.speed);
	      });
	
	      if (this.props.beforeChange) {
	        this.props.beforeChange(this.state.currentSlide, targetSlide);
	      }
	
	      this.autoPlay();
	      return;
	    }
	
	    targetSlide = index;
	    if (targetSlide < 0) {
	      if (this.props.infinite === false) {
	        currentSlide = 0;
	      } else if (this.state.slideCount % this.props.slidesToScroll !== 0) {
	        currentSlide = this.state.slideCount - this.state.slideCount % this.props.slidesToScroll;
	      } else {
	        currentSlide = this.state.slideCount + targetSlide;
	      }
	    } else if (targetSlide >= this.state.slideCount) {
	      if (this.props.infinite === false) {
	        currentSlide = this.state.slideCount - this.props.slidesToShow;
	      } else if (this.state.slideCount % this.props.slidesToScroll !== 0) {
	        currentSlide = 0;
	      } else {
	        currentSlide = targetSlide - this.state.slideCount;
	      }
	    } else {
	      currentSlide = targetSlide;
	    }
	
	    targetLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	      slideIndex: targetSlide,
	      trackRef: this.track
	    }, this.props, this.state));
	
	    currentLeft = (0, _trackHelper.getTrackLeft)((0, _objectAssign2.default)({
	      slideIndex: currentSlide,
	      trackRef: this.track
	    }, this.props, this.state));
	
	    if (this.props.infinite === false) {
	      targetLeft = currentLeft;
	    }
	
	    if (this.props.beforeChange) {
	      this.props.beforeChange(this.state.currentSlide, currentSlide);
	    }
	
	    if (this.props.lazyLoad) {
	      var loaded = true;
	      var slidesToLoad = [];
	      for (var i = targetSlide; i < targetSlide + this.props.slidesToShow; i++) {
	        loaded = loaded && this.state.lazyLoadedList.indexOf(i) >= 0;
	        if (!loaded) {
	          slidesToLoad.push(i);
	        }
	      }
	      if (!loaded) {
	        this.setState({
	          lazyLoadedList: this.state.lazyLoadedList.concat(slidesToLoad)
	        });
	      }
	    }
	
	    // Slide Transition happens here.
	    // animated transition happens to target Slide and
	    // non - animated transition happens to current Slide
	    // If CSS transitions are false, directly go the current slide.
	
	    if (this.props.useCSS === false) {
	
	      this.setState({
	        currentSlide: currentSlide,
	        trackStyle: (0, _trackHelper.getTrackCSS)((0, _objectAssign2.default)({ left: currentLeft }, this.props, this.state))
	      }, function () {
	        if (this.props.afterChange) {
	          this.props.afterChange(currentSlide);
	        }
	      });
	    } else {
	
	      var nextStateChanges = {
	        animating: false,
	        currentSlide: currentSlide,
	        trackStyle: (0, _trackHelper.getTrackCSS)((0, _objectAssign2.default)({ left: currentLeft }, this.props, this.state)),
	        swipeLeft: null
	      };
	
	      callback = function callback() {
	        _this.setState(nextStateChanges);
	        if (_this.props.afterChange) {
	          _this.props.afterChange(currentSlide);
	        }
	        delete _this.animationEndCallback;
	      };
	
	      this.setState({
	        animating: true,
	        currentSlide: currentSlide,
	        trackStyle: (0, _trackHelper.getTrackAnimateCSS)((0, _objectAssign2.default)({ left: targetLeft }, this.props, this.state))
	      }, function () {
	        this.animationEndCallback = setTimeout(callback, this.props.speed);
	      });
	    }
	
	    this.autoPlay();
	  },
	  swipeDirection: function swipeDirection(touchObject) {
	    var xDist, yDist, r, swipeAngle;
	
	    xDist = touchObject.startX - touchObject.curX;
	    yDist = touchObject.startY - touchObject.curY;
	    r = Math.atan2(yDist, xDist);
	
	    swipeAngle = Math.round(r * 180 / Math.PI);
	    if (swipeAngle < 0) {
	      swipeAngle = 360 - Math.abs(swipeAngle);
	    }
	    if (swipeAngle <= 45 && swipeAngle >= 0 || swipeAngle <= 360 && swipeAngle >= 315) {
	      return this.props.rtl === false ? 'left' : 'right';
	    }
	    if (swipeAngle >= 135 && swipeAngle <= 225) {
	      return this.props.rtl === false ? 'right' : 'left';
	    }
	    if (this.props.verticalSwiping === true) {
	      if (swipeAngle >= 35 && swipeAngle <= 135) {
	        return 'down';
	      } else {
	        return 'up';
	      }
	    }
	
	    return 'vertical';
	  },
	  play: function play() {
	    var nextIndex;
	
	    if (!this.state.mounted) {
	      return false;
	    }
	
	    if (this.props.rtl) {
	      nextIndex = this.state.currentSlide - this.props.slidesToScroll;
	    } else {
	      if (this.canGoNext(_extends({}, this.props, this.state))) {
	        nextIndex = this.state.currentSlide + this.props.slidesToScroll;
	      } else {
	        return false;
	      }
	    }
	
	    this.slideHandler(nextIndex);
	  },
	  autoPlay: function autoPlay() {
	    if (this.state.autoPlayTimer) {
	      clearTimeout(this.state.autoPlayTimer);
	    }
	    if (this.props.autoplay) {
	      this.setState({
	        autoPlayTimer: setTimeout(this.play, this.props.autoplaySpeed)
	      });
	    }
	  },
	  pause: function pause() {
	    if (this.state.autoPlayTimer) {
	      clearTimeout(this.state.autoPlayTimer);
	      this.setState({
	        autoPlayTimer: null
	      });
	    }
	  }
	};
	
	exports.default = helpers;

/***/ }),
/* 65 */
/***/ (function(module, exports) {

	"use strict";
	
	var initialState = {
	    animating: false,
	    dragging: false,
	    autoPlayTimer: null,
	    currentDirection: 0,
	    currentLeft: null,
	    currentSlide: 0,
	    direction: 1,
	    listWidth: null,
	    listHeight: null,
	    scrolling: false,
	    // loadIndex: 0,
	    slideCount: null,
	    slideWidth: null,
	    slideHeight: null,
	    swiping: false,
	    // sliding: false,
	    // slideOffset: 0,
	    swipeLeft: null,
	    touchObject: {
	        startX: 0,
	        startY: 0,
	        curX: 0,
	        curY: 0
	    },
	
	    lazyLoadedList: [],
	
	    // added for react
	    initialized: false,
	    edgeDragged: false,
	    swiped: false, // used by swipeEvent. differentites between touch and swipe.
	    trackStyle: {},
	    trackWidth: 0
	
	    // Removed
	    // transformsEnabled: false,
	    // $nextArrow: null,
	    // $prevArrow: null,
	    // $dots: null,
	    // $list: null,
	    // $slideTrack: null,
	    // $slides: null,
	};
	
	module.exports = initialState;

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var defaultProps = {
	    className: '',
	    accessibility: true,
	    adaptiveHeight: false,
	    arrows: true,
	    autoplay: false,
	    autoplaySpeed: 3000,
	    centerMode: false,
	    centerPadding: '50px',
	    cssEase: 'ease',
	    customPaging: function customPaging(i) {
	        return _react2.default.createElement(
	            'button',
	            null,
	            i + 1
	        );
	    },
	    dots: false,
	    dotsClass: 'slick-dots',
	    draggable: true,
	    easing: 'linear',
	    edgeFriction: 0.35,
	    fade: false,
	    focusOnSelect: false,
	    infinite: true,
	    initialSlide: 0,
	    lazyLoad: false,
	    pauseOnHover: true,
	    responsive: null,
	    rtl: false,
	    slide: 'div',
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    speed: 500,
	    swipe: true,
	    swipeToSlide: false,
	    touchMove: true,
	    touchThreshold: 5,
	    useCSS: true,
	    variableWidth: false,
	    vertical: false,
	    waitForAnimate: true,
	    afterChange: null,
	    beforeChange: null,
	    edgeEvent: null,
	    init: null,
	    swipeEvent: null,
	    // nextArrow, prevArrow are react componets
	    nextArrow: null,
	    prevArrow: null
	};
	
	module.exports = defaultProps;

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */
	
	'use strict';
	
	var React = __webpack_require__(2);
	var factory = __webpack_require__(68);
	
	if (typeof React === 'undefined') {
	  throw Error(
	    'create-react-class could not find the React object. If you are using script tags, ' +
	      'make sure that React is being loaded before create-react-class.'
	  );
	}
	
	// Hack to grab NoopUpdateQueue from isomorphic React
	var ReactNoopUpdateQueue = new React.Component().updater;
	
	module.exports = factory(
	  React.Component,
	  React.isValidElement,
	  ReactNoopUpdateQueue
	);


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */
	
	'use strict';
	
	var _assign = __webpack_require__(69);
	
	var emptyObject = __webpack_require__(70);
	var _invariant = __webpack_require__(71);
	
	if (process.env.NODE_ENV !== 'production') {
	  var warning = __webpack_require__(72);
	}
	
	var MIXINS_KEY = 'mixins';
	
	// Helper function to allow the creation of anonymous functions which do not
	// have .name set to the name of the variable being assigned to.
	function identity(fn) {
	  return fn;
	}
	
	var ReactPropTypeLocationNames;
	if (process.env.NODE_ENV !== 'production') {
	  ReactPropTypeLocationNames = {
	    prop: 'prop',
	    context: 'context',
	    childContext: 'child context'
	  };
	} else {
	  ReactPropTypeLocationNames = {};
	}
	
	function factory(ReactComponent, isValidElement, ReactNoopUpdateQueue) {
	  /**
	   * Policies that describe methods in `ReactClassInterface`.
	   */
	
	  var injectedMixins = [];
	
	  /**
	   * Composite components are higher-level components that compose other composite
	   * or host components.
	   *
	   * To create a new type of `ReactClass`, pass a specification of
	   * your new class to `React.createClass`. The only requirement of your class
	   * specification is that you implement a `render` method.
	   *
	   *   var MyComponent = React.createClass({
	   *     render: function() {
	   *       return <div>Hello World</div>;
	   *     }
	   *   });
	   *
	   * The class specification supports a specific protocol of methods that have
	   * special meaning (e.g. `render`). See `ReactClassInterface` for
	   * more the comprehensive protocol. Any other properties and methods in the
	   * class specification will be available on the prototype.
	   *
	   * @interface ReactClassInterface
	   * @internal
	   */
	  var ReactClassInterface = {
	    /**
	     * An array of Mixin objects to include when defining your component.
	     *
	     * @type {array}
	     * @optional
	     */
	    mixins: 'DEFINE_MANY',
	
	    /**
	     * An object containing properties and methods that should be defined on
	     * the component's constructor instead of its prototype (static methods).
	     *
	     * @type {object}
	     * @optional
	     */
	    statics: 'DEFINE_MANY',
	
	    /**
	     * Definition of prop types for this component.
	     *
	     * @type {object}
	     * @optional
	     */
	    propTypes: 'DEFINE_MANY',
	
	    /**
	     * Definition of context types for this component.
	     *
	     * @type {object}
	     * @optional
	     */
	    contextTypes: 'DEFINE_MANY',
	
	    /**
	     * Definition of context types this component sets for its children.
	     *
	     * @type {object}
	     * @optional
	     */
	    childContextTypes: 'DEFINE_MANY',
	
	    // ==== Definition methods ====
	
	    /**
	     * Invoked when the component is mounted. Values in the mapping will be set on
	     * `this.props` if that prop is not specified (i.e. using an `in` check).
	     *
	     * This method is invoked before `getInitialState` and therefore cannot rely
	     * on `this.state` or use `this.setState`.
	     *
	     * @return {object}
	     * @optional
	     */
	    getDefaultProps: 'DEFINE_MANY_MERGED',
	
	    /**
	     * Invoked once before the component is mounted. The return value will be used
	     * as the initial value of `this.state`.
	     *
	     *   getInitialState: function() {
	     *     return {
	     *       isOn: false,
	     *       fooBaz: new BazFoo()
	     *     }
	     *   }
	     *
	     * @return {object}
	     * @optional
	     */
	    getInitialState: 'DEFINE_MANY_MERGED',
	
	    /**
	     * @return {object}
	     * @optional
	     */
	    getChildContext: 'DEFINE_MANY_MERGED',
	
	    /**
	     * Uses props from `this.props` and state from `this.state` to render the
	     * structure of the component.
	     *
	     * No guarantees are made about when or how often this method is invoked, so
	     * it must not have side effects.
	     *
	     *   render: function() {
	     *     var name = this.props.name;
	     *     return <div>Hello, {name}!</div>;
	     *   }
	     *
	     * @return {ReactComponent}
	     * @required
	     */
	    render: 'DEFINE_ONCE',
	
	    // ==== Delegate methods ====
	
	    /**
	     * Invoked when the component is initially created and about to be mounted.
	     * This may have side effects, but any external subscriptions or data created
	     * by this method must be cleaned up in `componentWillUnmount`.
	     *
	     * @optional
	     */
	    componentWillMount: 'DEFINE_MANY',
	
	    /**
	     * Invoked when the component has been mounted and has a DOM representation.
	     * However, there is no guarantee that the DOM node is in the document.
	     *
	     * Use this as an opportunity to operate on the DOM when the component has
	     * been mounted (initialized and rendered) for the first time.
	     *
	     * @param {DOMElement} rootNode DOM element representing the component.
	     * @optional
	     */
	    componentDidMount: 'DEFINE_MANY',
	
	    /**
	     * Invoked before the component receives new props.
	     *
	     * Use this as an opportunity to react to a prop transition by updating the
	     * state using `this.setState`. Current props are accessed via `this.props`.
	     *
	     *   componentWillReceiveProps: function(nextProps, nextContext) {
	     *     this.setState({
	     *       likesIncreasing: nextProps.likeCount > this.props.likeCount
	     *     });
	     *   }
	     *
	     * NOTE: There is no equivalent `componentWillReceiveState`. An incoming prop
	     * transition may cause a state change, but the opposite is not true. If you
	     * need it, you are probably looking for `componentWillUpdate`.
	     *
	     * @param {object} nextProps
	     * @optional
	     */
	    componentWillReceiveProps: 'DEFINE_MANY',
	
	    /**
	     * Invoked while deciding if the component should be updated as a result of
	     * receiving new props, state and/or context.
	     *
	     * Use this as an opportunity to `return false` when you're certain that the
	     * transition to the new props/state/context will not require a component
	     * update.
	     *
	     *   shouldComponentUpdate: function(nextProps, nextState, nextContext) {
	     *     return !equal(nextProps, this.props) ||
	     *       !equal(nextState, this.state) ||
	     *       !equal(nextContext, this.context);
	     *   }
	     *
	     * @param {object} nextProps
	     * @param {?object} nextState
	     * @param {?object} nextContext
	     * @return {boolean} True if the component should update.
	     * @optional
	     */
	    shouldComponentUpdate: 'DEFINE_ONCE',
	
	    /**
	     * Invoked when the component is about to update due to a transition from
	     * `this.props`, `this.state` and `this.context` to `nextProps`, `nextState`
	     * and `nextContext`.
	     *
	     * Use this as an opportunity to perform preparation before an update occurs.
	     *
	     * NOTE: You **cannot** use `this.setState()` in this method.
	     *
	     * @param {object} nextProps
	     * @param {?object} nextState
	     * @param {?object} nextContext
	     * @param {ReactReconcileTransaction} transaction
	     * @optional
	     */
	    componentWillUpdate: 'DEFINE_MANY',
	
	    /**
	     * Invoked when the component's DOM representation has been updated.
	     *
	     * Use this as an opportunity to operate on the DOM when the component has
	     * been updated.
	     *
	     * @param {object} prevProps
	     * @param {?object} prevState
	     * @param {?object} prevContext
	     * @param {DOMElement} rootNode DOM element representing the component.
	     * @optional
	     */
	    componentDidUpdate: 'DEFINE_MANY',
	
	    /**
	     * Invoked when the component is about to be removed from its parent and have
	     * its DOM representation destroyed.
	     *
	     * Use this as an opportunity to deallocate any external resources.
	     *
	     * NOTE: There is no `componentDidUnmount` since your component will have been
	     * destroyed by that point.
	     *
	     * @optional
	     */
	    componentWillUnmount: 'DEFINE_MANY',
	
	    // ==== Advanced methods ====
	
	    /**
	     * Updates the component's currently mounted DOM representation.
	     *
	     * By default, this implements React's rendering and reconciliation algorithm.
	     * Sophisticated clients may wish to override this.
	     *
	     * @param {ReactReconcileTransaction} transaction
	     * @internal
	     * @overridable
	     */
	    updateComponent: 'OVERRIDE_BASE'
	  };
	
	  /**
	   * Mapping from class specification keys to special processing functions.
	   *
	   * Although these are declared like instance properties in the specification
	   * when defining classes using `React.createClass`, they are actually static
	   * and are accessible on the constructor instead of the prototype. Despite
	   * being static, they must be defined outside of the "statics" key under
	   * which all other static methods are defined.
	   */
	  var RESERVED_SPEC_KEYS = {
	    displayName: function(Constructor, displayName) {
	      Constructor.displayName = displayName;
	    },
	    mixins: function(Constructor, mixins) {
	      if (mixins) {
	        for (var i = 0; i < mixins.length; i++) {
	          mixSpecIntoComponent(Constructor, mixins[i]);
	        }
	      }
	    },
	    childContextTypes: function(Constructor, childContextTypes) {
	      if (process.env.NODE_ENV !== 'production') {
	        validateTypeDef(Constructor, childContextTypes, 'childContext');
	      }
	      Constructor.childContextTypes = _assign(
	        {},
	        Constructor.childContextTypes,
	        childContextTypes
	      );
	    },
	    contextTypes: function(Constructor, contextTypes) {
	      if (process.env.NODE_ENV !== 'production') {
	        validateTypeDef(Constructor, contextTypes, 'context');
	      }
	      Constructor.contextTypes = _assign(
	        {},
	        Constructor.contextTypes,
	        contextTypes
	      );
	    },
	    /**
	     * Special case getDefaultProps which should move into statics but requires
	     * automatic merging.
	     */
	    getDefaultProps: function(Constructor, getDefaultProps) {
	      if (Constructor.getDefaultProps) {
	        Constructor.getDefaultProps = createMergedResultFunction(
	          Constructor.getDefaultProps,
	          getDefaultProps
	        );
	      } else {
	        Constructor.getDefaultProps = getDefaultProps;
	      }
	    },
	    propTypes: function(Constructor, propTypes) {
	      if (process.env.NODE_ENV !== 'production') {
	        validateTypeDef(Constructor, propTypes, 'prop');
	      }
	      Constructor.propTypes = _assign({}, Constructor.propTypes, propTypes);
	    },
	    statics: function(Constructor, statics) {
	      mixStaticSpecIntoComponent(Constructor, statics);
	    },
	    autobind: function() {}
	  };
	
	  function validateTypeDef(Constructor, typeDef, location) {
	    for (var propName in typeDef) {
	      if (typeDef.hasOwnProperty(propName)) {
	        // use a warning instead of an _invariant so components
	        // don't show up in prod but only in __DEV__
	        if (process.env.NODE_ENV !== 'production') {
	          warning(
	            typeof typeDef[propName] === 'function',
	            '%s: %s type `%s` is invalid; it must be a function, usually from ' +
	              'React.PropTypes.',
	            Constructor.displayName || 'ReactClass',
	            ReactPropTypeLocationNames[location],
	            propName
	          );
	        }
	      }
	    }
	  }
	
	  function validateMethodOverride(isAlreadyDefined, name) {
	    var specPolicy = ReactClassInterface.hasOwnProperty(name)
	      ? ReactClassInterface[name]
	      : null;
	
	    // Disallow overriding of base class methods unless explicitly allowed.
	    if (ReactClassMixin.hasOwnProperty(name)) {
	      _invariant(
	        specPolicy === 'OVERRIDE_BASE',
	        'ReactClassInterface: You are attempting to override ' +
	          '`%s` from your class specification. Ensure that your method names ' +
	          'do not overlap with React methods.',
	        name
	      );
	    }
	
	    // Disallow defining methods more than once unless explicitly allowed.
	    if (isAlreadyDefined) {
	      _invariant(
	        specPolicy === 'DEFINE_MANY' || specPolicy === 'DEFINE_MANY_MERGED',
	        'ReactClassInterface: You are attempting to define ' +
	          '`%s` on your component more than once. This conflict may be due ' +
	          'to a mixin.',
	        name
	      );
	    }
	  }
	
	  /**
	   * Mixin helper which handles policy validation and reserved
	   * specification keys when building React classes.
	   */
	  function mixSpecIntoComponent(Constructor, spec) {
	    if (!spec) {
	      if (process.env.NODE_ENV !== 'production') {
	        var typeofSpec = typeof spec;
	        var isMixinValid = typeofSpec === 'object' && spec !== null;
	
	        if (process.env.NODE_ENV !== 'production') {
	          warning(
	            isMixinValid,
	            "%s: You're attempting to include a mixin that is either null " +
	              'or not an object. Check the mixins included by the component, ' +
	              'as well as any mixins they include themselves. ' +
	              'Expected object but got %s.',
	            Constructor.displayName || 'ReactClass',
	            spec === null ? null : typeofSpec
	          );
	        }
	      }
	
	      return;
	    }
	
	    _invariant(
	      typeof spec !== 'function',
	      "ReactClass: You're attempting to " +
	        'use a component class or function as a mixin. Instead, just use a ' +
	        'regular object.'
	    );
	    _invariant(
	      !isValidElement(spec),
	      "ReactClass: You're attempting to " +
	        'use a component as a mixin. Instead, just use a regular object.'
	    );
	
	    var proto = Constructor.prototype;
	    var autoBindPairs = proto.__reactAutoBindPairs;
	
	    // By handling mixins before any other properties, we ensure the same
	    // chaining order is applied to methods with DEFINE_MANY policy, whether
	    // mixins are listed before or after these methods in the spec.
	    if (spec.hasOwnProperty(MIXINS_KEY)) {
	      RESERVED_SPEC_KEYS.mixins(Constructor, spec.mixins);
	    }
	
	    for (var name in spec) {
	      if (!spec.hasOwnProperty(name)) {
	        continue;
	      }
	
	      if (name === MIXINS_KEY) {
	        // We have already handled mixins in a special case above.
	        continue;
	      }
	
	      var property = spec[name];
	      var isAlreadyDefined = proto.hasOwnProperty(name);
	      validateMethodOverride(isAlreadyDefined, name);
	
	      if (RESERVED_SPEC_KEYS.hasOwnProperty(name)) {
	        RESERVED_SPEC_KEYS[name](Constructor, property);
	      } else {
	        // Setup methods on prototype:
	        // The following member methods should not be automatically bound:
	        // 1. Expected ReactClass methods (in the "interface").
	        // 2. Overridden methods (that were mixed in).
	        var isReactClassMethod = ReactClassInterface.hasOwnProperty(name);
	        var isFunction = typeof property === 'function';
	        var shouldAutoBind =
	          isFunction &&
	          !isReactClassMethod &&
	          !isAlreadyDefined &&
	          spec.autobind !== false;
	
	        if (shouldAutoBind) {
	          autoBindPairs.push(name, property);
	          proto[name] = property;
	        } else {
	          if (isAlreadyDefined) {
	            var specPolicy = ReactClassInterface[name];
	
	            // These cases should already be caught by validateMethodOverride.
	            _invariant(
	              isReactClassMethod &&
	                (specPolicy === 'DEFINE_MANY_MERGED' ||
	                  specPolicy === 'DEFINE_MANY'),
	              'ReactClass: Unexpected spec policy %s for key %s ' +
	                'when mixing in component specs.',
	              specPolicy,
	              name
	            );
	
	            // For methods which are defined more than once, call the existing
	            // methods before calling the new property, merging if appropriate.
	            if (specPolicy === 'DEFINE_MANY_MERGED') {
	              proto[name] = createMergedResultFunction(proto[name], property);
	            } else if (specPolicy === 'DEFINE_MANY') {
	              proto[name] = createChainedFunction(proto[name], property);
	            }
	          } else {
	            proto[name] = property;
	            if (process.env.NODE_ENV !== 'production') {
	              // Add verbose displayName to the function, which helps when looking
	              // at profiling tools.
	              if (typeof property === 'function' && spec.displayName) {
	                proto[name].displayName = spec.displayName + '_' + name;
	              }
	            }
	          }
	        }
	      }
	    }
	  }
	
	  function mixStaticSpecIntoComponent(Constructor, statics) {
	    if (!statics) {
	      return;
	    }
	    for (var name in statics) {
	      var property = statics[name];
	      if (!statics.hasOwnProperty(name)) {
	        continue;
	      }
	
	      var isReserved = name in RESERVED_SPEC_KEYS;
	      _invariant(
	        !isReserved,
	        'ReactClass: You are attempting to define a reserved ' +
	          'property, `%s`, that shouldn\'t be on the "statics" key. Define it ' +
	          'as an instance property instead; it will still be accessible on the ' +
	          'constructor.',
	        name
	      );
	
	      var isInherited = name in Constructor;
	      _invariant(
	        !isInherited,
	        'ReactClass: You are attempting to define ' +
	          '`%s` on your component more than once. This conflict may be ' +
	          'due to a mixin.',
	        name
	      );
	      Constructor[name] = property;
	    }
	  }
	
	  /**
	   * Merge two objects, but throw if both contain the same key.
	   *
	   * @param {object} one The first object, which is mutated.
	   * @param {object} two The second object
	   * @return {object} one after it has been mutated to contain everything in two.
	   */
	  function mergeIntoWithNoDuplicateKeys(one, two) {
	    _invariant(
	      one && two && typeof one === 'object' && typeof two === 'object',
	      'mergeIntoWithNoDuplicateKeys(): Cannot merge non-objects.'
	    );
	
	    for (var key in two) {
	      if (two.hasOwnProperty(key)) {
	        _invariant(
	          one[key] === undefined,
	          'mergeIntoWithNoDuplicateKeys(): ' +
	            'Tried to merge two objects with the same key: `%s`. This conflict ' +
	            'may be due to a mixin; in particular, this may be caused by two ' +
	            'getInitialState() or getDefaultProps() methods returning objects ' +
	            'with clashing keys.',
	          key
	        );
	        one[key] = two[key];
	      }
	    }
	    return one;
	  }
	
	  /**
	   * Creates a function that invokes two functions and merges their return values.
	   *
	   * @param {function} one Function to invoke first.
	   * @param {function} two Function to invoke second.
	   * @return {function} Function that invokes the two argument functions.
	   * @private
	   */
	  function createMergedResultFunction(one, two) {
	    return function mergedResult() {
	      var a = one.apply(this, arguments);
	      var b = two.apply(this, arguments);
	      if (a == null) {
	        return b;
	      } else if (b == null) {
	        return a;
	      }
	      var c = {};
	      mergeIntoWithNoDuplicateKeys(c, a);
	      mergeIntoWithNoDuplicateKeys(c, b);
	      return c;
	    };
	  }
	
	  /**
	   * Creates a function that invokes two functions and ignores their return vales.
	   *
	   * @param {function} one Function to invoke first.
	   * @param {function} two Function to invoke second.
	   * @return {function} Function that invokes the two argument functions.
	   * @private
	   */
	  function createChainedFunction(one, two) {
	    return function chainedFunction() {
	      one.apply(this, arguments);
	      two.apply(this, arguments);
	    };
	  }
	
	  /**
	   * Binds a method to the component.
	   *
	   * @param {object} component Component whose method is going to be bound.
	   * @param {function} method Method to be bound.
	   * @return {function} The bound method.
	   */
	  function bindAutoBindMethod(component, method) {
	    var boundMethod = method.bind(component);
	    if (process.env.NODE_ENV !== 'production') {
	      boundMethod.__reactBoundContext = component;
	      boundMethod.__reactBoundMethod = method;
	      boundMethod.__reactBoundArguments = null;
	      var componentName = component.constructor.displayName;
	      var _bind = boundMethod.bind;
	      boundMethod.bind = function(newThis) {
	        for (
	          var _len = arguments.length,
	            args = Array(_len > 1 ? _len - 1 : 0),
	            _key = 1;
	          _key < _len;
	          _key++
	        ) {
	          args[_key - 1] = arguments[_key];
	        }
	
	        // User is trying to bind() an autobound method; we effectively will
	        // ignore the value of "this" that the user is trying to use, so
	        // let's warn.
	        if (newThis !== component && newThis !== null) {
	          if (process.env.NODE_ENV !== 'production') {
	            warning(
	              false,
	              'bind(): React component methods may only be bound to the ' +
	                'component instance. See %s',
	              componentName
	            );
	          }
	        } else if (!args.length) {
	          if (process.env.NODE_ENV !== 'production') {
	            warning(
	              false,
	              'bind(): You are binding a component method to the component. ' +
	                'React does this for you automatically in a high-performance ' +
	                'way, so you can safely remove this call. See %s',
	              componentName
	            );
	          }
	          return boundMethod;
	        }
	        var reboundMethod = _bind.apply(boundMethod, arguments);
	        reboundMethod.__reactBoundContext = component;
	        reboundMethod.__reactBoundMethod = method;
	        reboundMethod.__reactBoundArguments = args;
	        return reboundMethod;
	      };
	    }
	    return boundMethod;
	  }
	
	  /**
	   * Binds all auto-bound methods in a component.
	   *
	   * @param {object} component Component whose method is going to be bound.
	   */
	  function bindAutoBindMethods(component) {
	    var pairs = component.__reactAutoBindPairs;
	    for (var i = 0; i < pairs.length; i += 2) {
	      var autoBindKey = pairs[i];
	      var method = pairs[i + 1];
	      component[autoBindKey] = bindAutoBindMethod(component, method);
	    }
	  }
	
	  var IsMountedPreMixin = {
	    componentDidMount: function() {
	      this.__isMounted = true;
	    }
	  };
	
	  var IsMountedPostMixin = {
	    componentWillUnmount: function() {
	      this.__isMounted = false;
	    }
	  };
	
	  /**
	   * Add more to the ReactClass base class. These are all legacy features and
	   * therefore not already part of the modern ReactComponent.
	   */
	  var ReactClassMixin = {
	    /**
	     * TODO: This will be deprecated because state should always keep a consistent
	     * type signature and the only use case for this, is to avoid that.
	     */
	    replaceState: function(newState, callback) {
	      this.updater.enqueueReplaceState(this, newState, callback);
	    },
	
	    /**
	     * Checks whether or not this composite component is mounted.
	     * @return {boolean} True if mounted, false otherwise.
	     * @protected
	     * @final
	     */
	    isMounted: function() {
	      if (process.env.NODE_ENV !== 'production') {
	        warning(
	          this.__didWarnIsMounted,
	          '%s: isMounted is deprecated. Instead, make sure to clean up ' +
	            'subscriptions and pending requests in componentWillUnmount to ' +
	            'prevent memory leaks.',
	          (this.constructor && this.constructor.displayName) ||
	            this.name ||
	            'Component'
	        );
	        this.__didWarnIsMounted = true;
	      }
	      return !!this.__isMounted;
	    }
	  };
	
	  var ReactClassComponent = function() {};
	  _assign(
	    ReactClassComponent.prototype,
	    ReactComponent.prototype,
	    ReactClassMixin
	  );
	
	  /**
	   * Creates a composite component class given a class specification.
	   * See https://facebook.github.io/react/docs/top-level-api.html#react.createclass
	   *
	   * @param {object} spec Class specification (which must define `render`).
	   * @return {function} Component constructor function.
	   * @public
	   */
	  function createClass(spec) {
	    // To keep our warnings more understandable, we'll use a little hack here to
	    // ensure that Constructor.name !== 'Constructor'. This makes sure we don't
	    // unnecessarily identify a class without displayName as 'Constructor'.
	    var Constructor = identity(function(props, context, updater) {
	      // This constructor gets overridden by mocks. The argument is used
	      // by mocks to assert on what gets mounted.
	
	      if (process.env.NODE_ENV !== 'production') {
	        warning(
	          this instanceof Constructor,
	          'Something is calling a React component directly. Use a factory or ' +
	            'JSX instead. See: https://fb.me/react-legacyfactory'
	        );
	      }
	
	      // Wire up auto-binding
	      if (this.__reactAutoBindPairs.length) {
	        bindAutoBindMethods(this);
	      }
	
	      this.props = props;
	      this.context = context;
	      this.refs = emptyObject;
	      this.updater = updater || ReactNoopUpdateQueue;
	
	      this.state = null;
	
	      // ReactClasses doesn't have constructors. Instead, they use the
	      // getInitialState and componentWillMount methods for initialization.
	
	      var initialState = this.getInitialState ? this.getInitialState() : null;
	      if (process.env.NODE_ENV !== 'production') {
	        // We allow auto-mocks to proceed as if they're returning null.
	        if (
	          initialState === undefined &&
	          this.getInitialState._isMockFunction
	        ) {
	          // This is probably bad practice. Consider warning here and
	          // deprecating this convenience.
	          initialState = null;
	        }
	      }
	      _invariant(
	        typeof initialState === 'object' && !Array.isArray(initialState),
	        '%s.getInitialState(): must return an object or null',
	        Constructor.displayName || 'ReactCompositeComponent'
	      );
	
	      this.state = initialState;
	    });
	    Constructor.prototype = new ReactClassComponent();
	    Constructor.prototype.constructor = Constructor;
	    Constructor.prototype.__reactAutoBindPairs = [];
	
	    injectedMixins.forEach(mixSpecIntoComponent.bind(null, Constructor));
	
	    mixSpecIntoComponent(Constructor, IsMountedPreMixin);
	    mixSpecIntoComponent(Constructor, spec);
	    mixSpecIntoComponent(Constructor, IsMountedPostMixin);
	
	    // Initialize the defaultProps property after all mixins have been merged.
	    if (Constructor.getDefaultProps) {
	      Constructor.defaultProps = Constructor.getDefaultProps();
	    }
	
	    if (process.env.NODE_ENV !== 'production') {
	      // This is a tag to indicate that the use of these method names is ok,
	      // since it's used with createClass. If it's not, then it's likely a
	      // mistake so we'll warn you to use the static property, property
	      // initializer or constructor respectively.
	      if (Constructor.getDefaultProps) {
	        Constructor.getDefaultProps.isReactClassApproved = {};
	      }
	      if (Constructor.prototype.getInitialState) {
	        Constructor.prototype.getInitialState.isReactClassApproved = {};
	      }
	    }
	
	    _invariant(
	      Constructor.prototype.render,
	      'createClass(...): Class specification must implement a `render` method.'
	    );
	
	    if (process.env.NODE_ENV !== 'production') {
	      warning(
	        !Constructor.prototype.componentShouldUpdate,
	        '%s has a method called ' +
	          'componentShouldUpdate(). Did you mean shouldComponentUpdate()? ' +
	          'The name is phrased as a question because the function is ' +
	          'expected to return a value.',
	        spec.displayName || 'A component'
	      );
	      warning(
	        !Constructor.prototype.componentWillRecieveProps,
	        '%s has a method called ' +
	          'componentWillRecieveProps(). Did you mean componentWillReceiveProps()?',
	        spec.displayName || 'A component'
	      );
	    }
	
	    // Reduce time spent doing lookups by setting these on the prototype.
	    for (var methodName in ReactClassInterface) {
	      if (!Constructor.prototype[methodName]) {
	        Constructor.prototype[methodName] = null;
	      }
	    }
	
	    return Constructor;
	  }
	
	  return createClass;
	}
	
	module.exports = factory;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 69 */
/***/ (function(module, exports) {

	/*
	object-assign
	(c) Sindre Sorhus
	@license MIT
	*/
	
	'use strict';
	/* eslint-disable no-unused-vars */
	var getOwnPropertySymbols = Object.getOwnPropertySymbols;
	var hasOwnProperty = Object.prototype.hasOwnProperty;
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;
	
	function toObject(val) {
		if (val === null || val === undefined) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}
	
		return Object(val);
	}
	
	function shouldUseNative() {
		try {
			if (!Object.assign) {
				return false;
			}
	
			// Detect buggy property enumeration order in older V8 versions.
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=4118
			var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
			test1[5] = 'de';
			if (Object.getOwnPropertyNames(test1)[0] === '5') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test2 = {};
			for (var i = 0; i < 10; i++) {
				test2['_' + String.fromCharCode(i)] = i;
			}
			var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
				return test2[n];
			});
			if (order2.join('') !== '0123456789') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test3 = {};
			'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
				test3[letter] = letter;
			});
			if (Object.keys(Object.assign({}, test3)).join('') !==
					'abcdefghijklmnopqrst') {
				return false;
			}
	
			return true;
		} catch (err) {
			// We don't expect any of the above to throw, but better to be safe.
			return false;
		}
	}
	
	module.exports = shouldUseNative() ? Object.assign : function (target, source) {
		var from;
		var to = toObject(target);
		var symbols;
	
		for (var s = 1; s < arguments.length; s++) {
			from = Object(arguments[s]);
	
			for (var key in from) {
				if (hasOwnProperty.call(from, key)) {
					to[key] = from[key];
				}
			}
	
			if (getOwnPropertySymbols) {
				symbols = getOwnPropertySymbols(from);
				for (var i = 0; i < symbols.length; i++) {
					if (propIsEnumerable.call(from, symbols[i])) {
						to[symbols[i]] = from[symbols[i]];
					}
				}
			}
		}
	
		return to;
	};


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */
	
	'use strict';
	
	var emptyObject = {};
	
	if (process.env.NODE_ENV !== 'production') {
	  Object.freeze(emptyObject);
	}
	
	module.exports = emptyObject;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */
	
	'use strict';
	
	/**
	 * Use invariant() to assert state which your program assumes to be true.
	 *
	 * Provide sprintf-style format (only %s is supported) and arguments
	 * to provide information about what broke and what you were
	 * expecting.
	 *
	 * The invariant message will be stripped in production, but the invariant
	 * will remain to ensure logic does not differ in production.
	 */
	
	var validateFormat = function validateFormat(format) {};
	
	if (process.env.NODE_ENV !== 'production') {
	  validateFormat = function validateFormat(format) {
	    if (format === undefined) {
	      throw new Error('invariant requires an error message argument');
	    }
	  };
	}
	
	function invariant(condition, format, a, b, c, d, e, f) {
	  validateFormat(format);
	
	  if (!condition) {
	    var error;
	    if (format === undefined) {
	      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
	    } else {
	      var args = [a, b, c, d, e, f];
	      var argIndex = 0;
	      error = new Error(format.replace(/%s/g, function () {
	        return args[argIndex++];
	      }));
	      error.name = 'Invariant Violation';
	    }
	
	    error.framesToPop = 1; // we don't care about invariant's own frame
	    throw error;
	  }
	}
	
	module.exports = invariant;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2014-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */
	
	'use strict';
	
	var emptyFunction = __webpack_require__(73);
	
	/**
	 * Similar to invariant but only logs a warning if the condition is not met.
	 * This can be used to log issues in development environments in critical
	 * paths. Removing the logging code for production environments will keep the
	 * same logic and follow the same code paths.
	 */
	
	var warning = emptyFunction;
	
	if (process.env.NODE_ENV !== 'production') {
	  var printWarning = function printWarning(format) {
	    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	      args[_key - 1] = arguments[_key];
	    }
	
	    var argIndex = 0;
	    var message = 'Warning: ' + format.replace(/%s/g, function () {
	      return args[argIndex++];
	    });
	    if (typeof console !== 'undefined') {
	      console.error(message);
	    }
	    try {
	      // --- Welcome to debugging React ---
	      // This error was thrown as a convenience so that you can use this stack
	      // to find the callsite that caused this warning to fire.
	      throw new Error(message);
	    } catch (x) {}
	  };
	
	  warning = function warning(condition, format) {
	    if (format === undefined) {
	      throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
	    }
	
	    if (format.indexOf('Failed Composite propType: ') === 0) {
	      return; // Ignore CompositeComponent proptype check.
	    }
	
	    if (!condition) {
	      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
	        args[_key2 - 2] = arguments[_key2];
	      }
	
	      printWarning.apply(undefined, [format].concat(args));
	    }
	  };
	}
	
	module.exports = warning;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 73 */
/***/ (function(module, exports) {

	"use strict";
	
	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 * 
	 */
	
	function makeEmptyFunction(arg) {
	  return function () {
	    return arg;
	  };
	}
	
	/**
	 * This function accepts and discards inputs; it has no side effects. This is
	 * primarily useful idiomatically for overridable function endpoints which
	 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
	 */
	var emptyFunction = function emptyFunction() {};
	
	emptyFunction.thatReturns = makeEmptyFunction;
	emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
	emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
	emptyFunction.thatReturnsNull = makeEmptyFunction(null);
	emptyFunction.thatReturnsThis = function () {
	  return this;
	};
	emptyFunction.thatReturnsArgument = function (arg) {
	  return arg;
	};
	
	module.exports = emptyFunction;

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports.Track = undefined;
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _objectAssign = __webpack_require__(63);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var getSlideClasses = function getSlideClasses(spec) {
	  var slickActive, slickCenter, slickCloned;
	  var centerOffset, index;
	
	  if (spec.rtl) {
	    index = spec.slideCount - 1 - spec.index;
	  } else {
	    index = spec.index;
	  }
	  slickCloned = index < 0 || index >= spec.slideCount;
	  if (spec.centerMode) {
	    centerOffset = Math.floor(spec.slidesToShow / 2);
	    slickCenter = (index - spec.currentSlide) % spec.slideCount === 0;
	    if (index > spec.currentSlide - centerOffset - 1 && index <= spec.currentSlide + centerOffset) {
	      slickActive = true;
	    }
	  } else {
	    slickActive = spec.currentSlide <= index && index < spec.currentSlide + spec.slidesToShow;
	  }
	  return (0, _classnames2.default)({
	    'slick-slide': true,
	    'slick-active': slickActive,
	    'slick-center': slickCenter,
	    'slick-cloned': slickCloned
	  });
	};
	
	var getSlideStyle = function getSlideStyle(spec) {
	  var style = {};
	
	  if (spec.variableWidth === undefined || spec.variableWidth === false) {
	    style.width = spec.slideWidth;
	  }
	
	  if (spec.fade) {
	    style.position = 'relative';
	    style.left = -spec.index * spec.slideWidth;
	    style.opacity = spec.currentSlide === spec.index ? 1 : 0;
	    style.transition = 'opacity ' + spec.speed + 'ms ' + spec.cssEase;
	    style.WebkitTransition = 'opacity ' + spec.speed + 'ms ' + spec.cssEase;
	  }
	
	  return style;
	};
	
	var getKey = function getKey(child, fallbackKey) {
	  // key could be a zero
	  return child.key === null || child.key === undefined ? fallbackKey : child.key;
	};
	
	var renderSlides = function renderSlides(spec) {
	  var key;
	  var slides = [];
	  var preCloneSlides = [];
	  var postCloneSlides = [];
	  var count = _react2.default.Children.count(spec.children);
	
	  _react2.default.Children.forEach(spec.children, function (elem, index) {
	    var child = void 0;
	    var childOnClickOptions = {
	      message: 'children',
	      index: index,
	      slidesToScroll: spec.slidesToScroll,
	      currentSlide: spec.currentSlide
	    };
	
	    if (!spec.lazyLoad | (spec.lazyLoad && spec.lazyLoadedList.indexOf(index) >= 0)) {
	      child = elem;
	    } else {
	      child = _react2.default.createElement('div', null);
	    }
	    var childStyle = getSlideStyle((0, _objectAssign2.default)({}, spec, { index: index }));
	    var slideClass = child.props.className || '';
	
	    var onClick = function onClick(e) {
	      child.props && child.props.onClick && child.props.onClick(e);
	      if (spec.focusOnSelect) {
	        spec.focusOnSelect(childOnClickOptions);
	      }
	    };
	
	    slides.push(_react2.default.cloneElement(child, {
	      key: 'original' + getKey(child, index),
	      'data-index': index,
	      className: (0, _classnames2.default)(getSlideClasses((0, _objectAssign2.default)({ index: index }, spec)), slideClass),
	      tabIndex: '-1',
	      style: (0, _objectAssign2.default)({ outline: 'none' }, child.props.style || {}, childStyle),
	      onClick: onClick
	    }));
	
	    // variableWidth doesn't wrap properly.
	    if (spec.infinite && spec.fade === false) {
	      var infiniteCount = spec.variableWidth ? spec.slidesToShow + 1 : spec.slidesToShow;
	
	      if (index >= count - infiniteCount) {
	        key = -(count - index);
	        preCloneSlides.push(_react2.default.cloneElement(child, {
	          key: 'precloned' + getKey(child, key),
	          'data-index': key,
	          className: (0, _classnames2.default)(getSlideClasses((0, _objectAssign2.default)({ index: key }, spec)), slideClass),
	          style: (0, _objectAssign2.default)({}, child.props.style || {}, childStyle),
	          onClick: onClick
	        }));
	      }
	
	      if (index < infiniteCount) {
	        key = count + index;
	        postCloneSlides.push(_react2.default.cloneElement(child, {
	          key: 'postcloned' + getKey(child, key),
	          'data-index': key,
	          className: (0, _classnames2.default)(getSlideClasses((0, _objectAssign2.default)({ index: key }, spec)), slideClass),
	          style: (0, _objectAssign2.default)({}, child.props.style || {}, childStyle),
	          onClick: onClick
	        }));
	      }
	    }
	  });
	
	  if (spec.rtl) {
	    return preCloneSlides.concat(slides, postCloneSlides).reverse();
	  } else {
	    return preCloneSlides.concat(slides, postCloneSlides);
	  }
	};
	
	var Track = exports.Track = function (_React$Component) {
	  _inherits(Track, _React$Component);
	
	  function Track() {
	    _classCallCheck(this, Track);
	
	    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
	  }
	
	  Track.prototype.render = function render() {
	    var slides = renderSlides.call(this, this.props);
	    return _react2.default.createElement(
	      'div',
	      { className: 'slick-track', style: this.props.trackStyle },
	      slides
	    );
	  };
	
	  return Track;
	}(_react2.default.Component);

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports.Dots = undefined;
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var getDotCount = function getDotCount(spec) {
	  var dots;
	  dots = Math.ceil(spec.slideCount / spec.slidesToScroll);
	  return dots;
	};
	
	var Dots = exports.Dots = function (_React$Component) {
	  _inherits(Dots, _React$Component);
	
	  function Dots() {
	    _classCallCheck(this, Dots);
	
	    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
	  }
	
	  Dots.prototype.clickHandler = function clickHandler(options, e) {
	    // In Autoplay the focus stays on clicked button even after transition
	    // to next slide. That only goes away by click somewhere outside
	    e.preventDefault();
	    this.props.clickHandler(options);
	  };
	
	  Dots.prototype.render = function render() {
	    var _this2 = this;
	
	    var dotCount = getDotCount({
	      slideCount: this.props.slideCount,
	      slidesToScroll: this.props.slidesToScroll
	    });
	
	    // Apply join & split to Array to pre-fill it for IE8
	    //
	    // Credit: http://stackoverflow.com/a/13735425/1849458
	    var dots = Array.apply(null, Array(dotCount + 1).join('0').split('')).map(function (x, i) {
	
	      var leftBound = i * _this2.props.slidesToScroll;
	      var rightBound = i * _this2.props.slidesToScroll + (_this2.props.slidesToScroll - 1);
	      var className = (0, _classnames2.default)({
	        'slick-active': _this2.props.currentSlide >= leftBound && _this2.props.currentSlide <= rightBound
	      });
	
	      var dotOptions = {
	        message: 'dots',
	        index: i,
	        slidesToScroll: _this2.props.slidesToScroll,
	        currentSlide: _this2.props.currentSlide
	      };
	
	      var onClick = _this2.clickHandler.bind(_this2, dotOptions);
	
	      return _react2.default.createElement(
	        'li',
	        { key: i, className: className },
	        _react2.default.cloneElement(_this2.props.customPaging(i), { onClick: onClick })
	      );
	    });
	
	    return _react2.default.createElement(
	      'ul',
	      { className: this.props.dotsClass, style: { display: 'block' } },
	      dots
	    );
	  };
	
	  return Dots;
	}(_react2.default.Component);

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports.NextArrow = exports.PrevArrow = undefined;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _classnames = __webpack_require__(3);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	var _helpers = __webpack_require__(64);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var PrevArrow = exports.PrevArrow = function (_React$Component) {
	  _inherits(PrevArrow, _React$Component);
	
	  function PrevArrow() {
	    _classCallCheck(this, PrevArrow);
	
	    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
	  }
	
	  PrevArrow.prototype.clickHandler = function clickHandler(options, e) {
	    if (e) {
	      e.preventDefault();
	    }
	    this.props.clickHandler(options, e);
	  };
	
	  PrevArrow.prototype.render = function render() {
	    var prevClasses = { 'slick-arrow': true, 'slick-prev': true };
	    var prevHandler = this.clickHandler.bind(this, { message: 'previous' });
	
	    if (!this.props.infinite && (this.props.currentSlide === 0 || this.props.slideCount <= this.props.slidesToShow)) {
	      prevClasses['slick-disabled'] = true;
	      prevHandler = null;
	    }
	
	    var prevArrowProps = {
	      key: '0',
	      'data-role': 'none',
	      className: (0, _classnames2.default)(prevClasses),
	      style: { display: 'block' },
	      onClick: prevHandler
	    };
	    var customProps = {
	      currentSlide: this.props.currentSlide,
	      slideCount: this.props.slideCount
	    };
	    var prevArrow;
	
	    if (this.props.prevArrow) {
	      prevArrow = _react2.default.cloneElement(this.props.prevArrow, _extends({}, prevArrowProps, customProps));
	    } else {
	      prevArrow = _react2.default.createElement(
	        'button',
	        _extends({ key: '0', type: 'button' }, prevArrowProps),
	        ' Previous'
	      );
	    }
	
	    return prevArrow;
	  };
	
	  return PrevArrow;
	}(_react2.default.Component);
	
	var NextArrow = exports.NextArrow = function (_React$Component2) {
	  _inherits(NextArrow, _React$Component2);
	
	  function NextArrow() {
	    _classCallCheck(this, NextArrow);
	
	    return _possibleConstructorReturn(this, _React$Component2.apply(this, arguments));
	  }
	
	  NextArrow.prototype.clickHandler = function clickHandler(options, e) {
	    if (e) {
	      e.preventDefault();
	    }
	    this.props.clickHandler(options, e);
	  };
	
	  NextArrow.prototype.render = function render() {
	    var nextClasses = { 'slick-arrow': true, 'slick-next': true };
	    var nextHandler = this.clickHandler.bind(this, { message: 'next' });
	
	    if (!_helpers2.default.canGoNext(this.props)) {
	      nextClasses['slick-disabled'] = true;
	      nextHandler = null;
	    }
	
	    var nextArrowProps = {
	      key: '1',
	      'data-role': 'none',
	      className: (0, _classnames2.default)(nextClasses),
	      style: { display: 'block' },
	      onClick: nextHandler
	    };
	    var customProps = {
	      currentSlide: this.props.currentSlide,
	      slideCount: this.props.slideCount
	    };
	    var nextArrow;
	
	    if (this.props.nextArrow) {
	      nextArrow = _react2.default.cloneElement(this.props.nextArrow, _extends({}, nextArrowProps, customProps));
	    } else {
	      nextArrow = _react2.default.createElement(
	        'button',
	        _extends({ key: '1', type: 'button' }, nextArrowProps),
	        ' Next'
	      );
	    }
	
	    return nextArrow;
	  };
	
	  return NextArrow;
	}(_react2.default.Component);

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

	var camel2hyphen = __webpack_require__(78);
	
	var isDimension = function (feature) {
	  var re = /[height|width]$/;
	  return re.test(feature);
	};
	
	var obj2mq = function (obj) {
	  var mq = '';
	  var features = Object.keys(obj);
	  features.forEach(function (feature, index) {
	    var value = obj[feature];
	    feature = camel2hyphen(feature);
	    // Add px to dimension features
	    if (isDimension(feature) && typeof value === 'number') {
	      value = value + 'px';
	    }
	    if (value === true) {
	      mq += feature;
	    } else if (value === false) {
	      mq += 'not ' + feature;
	    } else {
	      mq += '(' + feature + ': ' + value + ')';
	    }
	    if (index < features.length-1) {
	      mq += ' and '
	    }
	  });
	  return mq;
	};
	
	var json2mq = function (query) {
	  var mq = '';
	  if (typeof query === 'string') {
	    return query;
	  }
	  // Handling array of media queries
	  if (query instanceof Array) {
	    query.forEach(function (q, index) {
	      mq += obj2mq(q);
	      if (index < query.length-1) {
	        mq += ', '
	      }
	    });
	    return mq;
	  }
	  // Handling single media query
	  return obj2mq(query);
	};
	
	module.exports = json2mq;

/***/ }),
/* 78 */
/***/ (function(module, exports) {

	var camel2hyphen = function (str) {
	  return str
	          .replace(/[A-Z]/g, function (match) {
	            return '-' + match.toLowerCase();
	          })
	          .toLowerCase();
	};
	
	module.exports = camel2hyphen;

/***/ }),
/* 79 */
/***/ (function(module, exports) {

	var canUseDOM = !!(
	  typeof window !== 'undefined' &&
	  window.document &&
	  window.document.createElement
	);
	
	module.exports = canUseDOM;

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

	var MediaQueryDispatch = __webpack_require__(81);
	module.exports = new MediaQueryDispatch();


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

	var MediaQuery = __webpack_require__(82);
	var Util = __webpack_require__(84);
	var each = Util.each;
	var isFunction = Util.isFunction;
	var isArray = Util.isArray;
	
	/**
	 * Allows for registration of query handlers.
	 * Manages the query handler's state and is responsible for wiring up browser events
	 *
	 * @constructor
	 */
	function MediaQueryDispatch () {
	    if(!window.matchMedia) {
	        throw new Error('matchMedia not present, legacy browsers require a polyfill');
	    }
	
	    this.queries = {};
	    this.browserIsIncapable = !window.matchMedia('only all').matches;
	}
	
	MediaQueryDispatch.prototype = {
	
	    constructor : MediaQueryDispatch,
	
	    /**
	     * Registers a handler for the given media query
	     *
	     * @param {string} q the media query
	     * @param {object || Array || Function} options either a single query handler object, a function, or an array of query handlers
	     * @param {function} options.match fired when query matched
	     * @param {function} [options.unmatch] fired when a query is no longer matched
	     * @param {function} [options.setup] fired when handler first triggered
	     * @param {boolean} [options.deferSetup=false] whether setup should be run immediately or deferred until query is first matched
	     * @param {boolean} [shouldDegrade=false] whether this particular media query should always run on incapable browsers
	     */
	    register : function(q, options, shouldDegrade) {
	        var queries         = this.queries,
	            isUnconditional = shouldDegrade && this.browserIsIncapable;
	
	        if(!queries[q]) {
	            queries[q] = new MediaQuery(q, isUnconditional);
	        }
	
	        //normalise to object in an array
	        if(isFunction(options)) {
	            options = { match : options };
	        }
	        if(!isArray(options)) {
	            options = [options];
	        }
	        each(options, function(handler) {
	            if (isFunction(handler)) {
	                handler = { match : handler };
	            }
	            queries[q].addHandler(handler);
	        });
	
	        return this;
	    },
	
	    /**
	     * unregisters a query and all it's handlers, or a specific handler for a query
	     *
	     * @param {string} q the media query to target
	     * @param {object || function} [handler] specific handler to unregister
	     */
	    unregister : function(q, handler) {
	        var query = this.queries[q];
	
	        if(query) {
	            if(handler) {
	                query.removeHandler(handler);
	            }
	            else {
	                query.clear();
	                delete this.queries[q];
	            }
	        }
	
	        return this;
	    }
	};
	
	module.exports = MediaQueryDispatch;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

	var QueryHandler = __webpack_require__(83);
	var each = __webpack_require__(84).each;
	
	/**
	 * Represents a single media query, manages it's state and registered handlers for this query
	 *
	 * @constructor
	 * @param {string} query the media query string
	 * @param {boolean} [isUnconditional=false] whether the media query should run regardless of whether the conditions are met. Primarily for helping older browsers deal with mobile-first design
	 */
	function MediaQuery(query, isUnconditional) {
	    this.query = query;
	    this.isUnconditional = isUnconditional;
	    this.handlers = [];
	    this.mql = window.matchMedia(query);
	
	    var self = this;
	    this.listener = function(mql) {
	        // Chrome passes an MediaQueryListEvent object, while other browsers pass MediaQueryList directly
	        self.mql = mql.currentTarget || mql;
	        self.assess();
	    };
	    this.mql.addListener(this.listener);
	}
	
	MediaQuery.prototype = {
	
	    constuctor : MediaQuery,
	
	    /**
	     * add a handler for this query, triggering if already active
	     *
	     * @param {object} handler
	     * @param {function} handler.match callback for when query is activated
	     * @param {function} [handler.unmatch] callback for when query is deactivated
	     * @param {function} [handler.setup] callback for immediate execution when a query handler is registered
	     * @param {boolean} [handler.deferSetup=false] should the setup callback be deferred until the first time the handler is matched?
	     */
	    addHandler : function(handler) {
	        var qh = new QueryHandler(handler);
	        this.handlers.push(qh);
	
	        this.matches() && qh.on();
	    },
	
	    /**
	     * removes the given handler from the collection, and calls it's destroy methods
	     *
	     * @param {object || function} handler the handler to remove
	     */
	    removeHandler : function(handler) {
	        var handlers = this.handlers;
	        each(handlers, function(h, i) {
	            if(h.equals(handler)) {
	                h.destroy();
	                return !handlers.splice(i,1); //remove from array and exit each early
	            }
	        });
	    },
	
	    /**
	     * Determine whether the media query should be considered a match
	     *
	     * @return {Boolean} true if media query can be considered a match, false otherwise
	     */
	    matches : function() {
	        return this.mql.matches || this.isUnconditional;
	    },
	
	    /**
	     * Clears all handlers and unbinds events
	     */
	    clear : function() {
	        each(this.handlers, function(handler) {
	            handler.destroy();
	        });
	        this.mql.removeListener(this.listener);
	        this.handlers.length = 0; //clear array
	    },
	
	    /*
	        * Assesses the query, turning on all handlers if it matches, turning them off if it doesn't match
	        */
	    assess : function() {
	        var action = this.matches() ? 'on' : 'off';
	
	        each(this.handlers, function(handler) {
	            handler[action]();
	        });
	    }
	};
	
	module.exports = MediaQuery;


/***/ }),
/* 83 */
/***/ (function(module, exports) {

	/**
	 * Delegate to handle a media query being matched and unmatched.
	 *
	 * @param {object} options
	 * @param {function} options.match callback for when the media query is matched
	 * @param {function} [options.unmatch] callback for when the media query is unmatched
	 * @param {function} [options.setup] one-time callback triggered the first time a query is matched
	 * @param {boolean} [options.deferSetup=false] should the setup callback be run immediately, rather than first time query is matched?
	 * @constructor
	 */
	function QueryHandler(options) {
	    this.options = options;
	    !options.deferSetup && this.setup();
	}
	
	QueryHandler.prototype = {
	
	    constructor : QueryHandler,
	
	    /**
	     * coordinates setup of the handler
	     *
	     * @function
	     */
	    setup : function() {
	        if(this.options.setup) {
	            this.options.setup();
	        }
	        this.initialised = true;
	    },
	
	    /**
	     * coordinates setup and triggering of the handler
	     *
	     * @function
	     */
	    on : function() {
	        !this.initialised && this.setup();
	        this.options.match && this.options.match();
	    },
	
	    /**
	     * coordinates the unmatch event for the handler
	     *
	     * @function
	     */
	    off : function() {
	        this.options.unmatch && this.options.unmatch();
	    },
	
	    /**
	     * called when a handler is to be destroyed.
	     * delegates to the destroy or unmatch callbacks, depending on availability.
	     *
	     * @function
	     */
	    destroy : function() {
	        this.options.destroy ? this.options.destroy() : this.off();
	    },
	
	    /**
	     * determines equality by reference.
	     * if object is supplied compare options, if function, compare match callback
	     *
	     * @function
	     * @param {object || function} [target] the target for comparison
	     */
	    equals : function(target) {
	        return this.options === target || this.options.match === target;
	    }
	
	};
	
	module.exports = QueryHandler;


/***/ }),
/* 84 */
/***/ (function(module, exports) {

	/**
	 * Helper function for iterating over a collection
	 *
	 * @param collection
	 * @param fn
	 */
	function each(collection, fn) {
	    var i      = 0,
	        length = collection.length,
	        cont;
	
	    for(i; i < length; i++) {
	        cont = fn(collection[i], i);
	        if(cont === false) {
	            break; //allow early exit
	        }
	    }
	}
	
	/**
	 * Helper function for determining whether target object is an array
	 *
	 * @param target the object under test
	 * @return {Boolean} true if array, false otherwise
	 */
	function isArray(target) {
	    return Object.prototype.toString.apply(target) === '[object Array]';
	}
	
	/**
	 * Helper function for determining whether target object is a function
	 *
	 * @param target the object under test
	 * @return {Boolean} true if function, false otherwise
	 */
	function isFunction(target) {
	    return typeof target === 'function';
	}
	
	module.exports = {
	    isFunction : isFunction,
	    isArray : isArray,
	    each : each
	};


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(86);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../css-loader/index.js!./slick.css", function() {
				var newContent = require("!!../../css-loader/index.js!./slick.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, "/* Slider */\n.slick-slider\n{\n    position: relative;\n\n    display: block;\n    box-sizing: border-box;\n\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n\n    -webkit-touch-callout: none;\n    -khtml-user-select: none;\n    -ms-touch-action: pan-y;\n        touch-action: pan-y;\n    -webkit-tap-highlight-color: transparent;\n}\n\n.slick-list\n{\n    position: relative;\n\n    display: block;\n    overflow: hidden;\n\n    margin: 0;\n    padding: 0;\n}\n.slick-list:focus\n{\n    outline: none;\n}\n.slick-list.dragging\n{\n    cursor: pointer;\n    cursor: hand;\n}\n\n.slick-slider .slick-track,\n.slick-slider .slick-list\n{\n    -webkit-transform: translate3d(0, 0, 0);\n       -moz-transform: translate3d(0, 0, 0);\n        -ms-transform: translate3d(0, 0, 0);\n         -o-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0);\n}\n\n.slick-track\n{\n    position: relative;\n    top: 0;\n    left: 0;\n\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n}\n.slick-track:before,\n.slick-track:after\n{\n    display: table;\n\n    content: '';\n}\n.slick-track:after\n{\n    clear: both;\n}\n.slick-loading .slick-track\n{\n    visibility: hidden;\n}\n\n.slick-slide\n{\n    display: none;\n    float: left;\n\n    height: 100%;\n    min-height: 1px;\n}\n[dir='rtl'] .slick-slide\n{\n    float: right;\n}\n.slick-slide img\n{\n    display: block;\n}\n.slick-slide.slick-loading img\n{\n    display: none;\n}\n.slick-slide.dragging img\n{\n    pointer-events: none;\n}\n.slick-initialized .slick-slide\n{\n    display: block;\n}\n.slick-loading .slick-slide\n{\n    visibility: hidden;\n}\n.slick-vertical .slick-slide\n{\n    display: block;\n\n    height: auto;\n\n    border: 1px solid transparent;\n}\n.slick-arrow.slick-hidden {\n    display: none;\n}\n", ""]);
	
	// exports


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(88);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../css-loader/index.js!./slick-theme.css", function() {
				var newContent = require("!!../../css-loader/index.js!./slick-theme.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, "@charset 'UTF-8';\n/* Slider */\n.slick-loading .slick-list\n{\n    background: #fff url(" + __webpack_require__(89) + ") center center no-repeat;\n}\n\n/* Icons */\n@font-face\n{\n    font-family: 'slick';\n    font-weight: normal;\n    font-style: normal;\n\n    src: url(" + __webpack_require__(90) + ");\n    src: url(" + __webpack_require__(90) + "?#iefix) format('embedded-opentype'), url(" + __webpack_require__(91) + ") format('woff'), url(" + __webpack_require__(92) + ") format('truetype'), url(" + __webpack_require__(93) + "#slick) format('svg');\n}\n/* Arrows */\n.slick-prev,\n.slick-next\n{\n    font-size: 0;\n    line-height: 0;\n\n    position: absolute;\n    top: 50%;\n\n    display: block;\n\n    width: 20px;\n    height: 20px;\n    padding: 0;\n    -webkit-transform: translate(0, -50%);\n    -ms-transform: translate(0, -50%);\n    transform: translate(0, -50%);\n\n    cursor: pointer;\n\n    color: transparent;\n    border: none;\n    outline: none;\n    background: transparent;\n}\n.slick-prev:hover,\n.slick-prev:focus,\n.slick-next:hover,\n.slick-next:focus\n{\n    color: transparent;\n    outline: none;\n    background: transparent;\n}\n.slick-prev:hover:before,\n.slick-prev:focus:before,\n.slick-next:hover:before,\n.slick-next:focus:before\n{\n    opacity: 1;\n}\n.slick-prev.slick-disabled:before,\n.slick-next.slick-disabled:before\n{\n    opacity: .25;\n}\n\n.slick-prev:before,\n.slick-next:before\n{\n    font-family: 'slick';\n    font-size: 20px;\n    line-height: 1;\n\n    opacity: .75;\n    color: white;\n\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n}\n\n.slick-prev\n{\n    left: -25px;\n}\n[dir='rtl'] .slick-prev\n{\n    right: -25px;\n    left: auto;\n}\n.slick-prev:before\n{\n    content: '\\2190';\n}\n[dir='rtl'] .slick-prev:before\n{\n    content: '\\2192';\n}\n\n.slick-next\n{\n    right: -25px;\n}\n[dir='rtl'] .slick-next\n{\n    right: auto;\n    left: -25px;\n}\n.slick-next:before\n{\n    content: '\\2192';\n}\n[dir='rtl'] .slick-next:before\n{\n    content: '\\2190';\n}\n\n/* Dots */\n.slick-dotted.slick-slider\n{\n    margin-bottom: 30px;\n}\n\n.slick-dots\n{\n    position: absolute;\n    bottom: -25px;\n\n    display: block;\n\n    width: 100%;\n    padding: 0;\n    margin: 0;\n\n    list-style: none;\n\n    text-align: center;\n}\n.slick-dots li\n{\n    position: relative;\n\n    display: inline-block;\n\n    width: 20px;\n    height: 20px;\n    margin: 0 5px;\n    padding: 0;\n\n    cursor: pointer;\n}\n.slick-dots li button\n{\n    font-size: 0;\n    line-height: 0;\n\n    display: block;\n\n    width: 20px;\n    height: 20px;\n    padding: 5px;\n\n    cursor: pointer;\n\n    color: transparent;\n    border: 0;\n    outline: none;\n    background: transparent;\n}\n.slick-dots li button:hover,\n.slick-dots li button:focus\n{\n    outline: none;\n}\n.slick-dots li button:hover:before,\n.slick-dots li button:focus:before\n{\n    opacity: 1;\n}\n.slick-dots li button:before\n{\n    font-family: 'slick';\n    font-size: 6px;\n    line-height: 20px;\n\n    position: absolute;\n    top: 0;\n    left: 0;\n\n    width: 20px;\n    height: 20px;\n\n    content: '\\2022';\n    text-align: center;\n\n    opacity: .25;\n    color: black;\n\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n}\n.slick-dots li.slick-active button:before\n{\n    opacity: .75;\n    color: black;\n}\n", ""]);
	
	// exports


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "68b0c4fcb84d239f261c1a5e65818142.gif";

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "../fonts/slick.eot";

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "../fonts/slick.woff";

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "../fonts/slick.ttf";

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__.p + "../fonts/slick.svg";

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = undefined;
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(2);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _style = __webpack_require__(51);
	
	var _style2 = _interopRequireDefault(_style);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var FormElement = function (_Component) {
	  _inherits(FormElement, _Component);
	
	  function FormElement(props) {
	    _classCallCheck(this, FormElement);
	
	    var _this = _possibleConstructorReturn(this, (FormElement.__proto__ || Object.getPrototypeOf(FormElement)).call(this, props));
	
	    _this.state = { hide: false };
	    return _this;
	  }
	
	  _createClass(FormElement, [{
	    key: 'changeState',
	    value: function changeState(field) {
	      var _this2 = this;
	
	      return function (e) {
	        _this2.props.parent.setState(_defineProperty({}, field, e.target.value));
	      };
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      switch (this.props.type) {
	        case 'input':
	          return _react2.default.createElement(
	            'div',
	            { className: _style2.default.formGroup },
	            _react2.default.createElement(
	              'label',
	              null,
	              this.props.label
	            ),
	            _react2.default.createElement('input', {
	              className: _style2.default.formInput,
	              type: this.props.subtype,
	              placeholder: this.props.placeholder,
	              name: this.props.name,
	              onChange: this.changeState.call(this, this.props.name),
	              maxLength: this.props.maxlength,
	              minLength: this.props.minlength,
	              required: this.props.required
	            })
	          );
	          break;
	        case 'textarea':
	          return _react2.default.createElement(
	            'div',
	            { className: _style2.default.formGroup },
	            _react2.default.createElement(
	              'label',
	              null,
	              this.props.label
	            ),
	            _react2.default.createElement('textarea', {
	              type: this.props.subtype,
	              className: _style2.default.formTextarea,
	              placeholder: this.props.placeholder,
	              name: this.props.name,
	              onChange: this.changeState.call(this, this.props.name),
	              maxLength: this.props.maxlength,
	              minLength: this.props.minlength,
	              required: this.props.required
	            })
	          );
	          break;
	        case 'select':
	          var options = this.props.options.map(function (se) {
	            return _react2.default.createElement(
	              'option',
	              { value: se.option.value },
	              se.option.label
	            );
	          });
	          var placeholder = '';
	          if (this.props.placeholder) {
	            placeholder = _react2.default.createElement(
	              'option',
	              { value: '' },
	              this.props.placeholder
	            );
	          }
	          return _react2.default.createElement(
	            'div',
	            { className: _style2.default.formGroup },
	            _react2.default.createElement(
	              'label',
	              null,
	              this.props.label
	            ),
	            _react2.default.createElement(
	              'select',
	              {
	                className: _style2.default.formInput,
	                value: this.state.value,
	                onChange: this.changeState.call(this, this.props.name),
	                name: this.props.name,
	                required: this.props.required
	              },
	              placeholder,
	              options
	            )
	          );
	          break;
	      }
	    }
	  }]);
	
	  return FormElement;
	}(_react.Component);
	
	var Form = function (_Component2) {
	  _inherits(Form, _Component2);
	
	  function Form(props) {
	    _classCallCheck(this, Form);
	
	    var _this3 = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, props));
	
	    _this3.state = {};
	    return _this3;
	  }
	
	  _createClass(Form, [{
	    key: 'handleSubmit',
	    value: function handleSubmit(event) {
	      event.preventDefault();
	      if (this.props.onFormSend) {
	        var representation = '';
	        for (var key in this.state) {
	          representation += key + ': ' + this.state[key] + '\n';
	        }this.props.onFormSend(this.state, this.props.formId, representation);
	      }
	    }
	  }, {
	    key: 'handleClose',
	    value: function handleClose(event) {
	      this.setState({ hide: true });
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this4 = this;
	
	      if (this.state.hide) return null;
	      if (!this.props.elements) return null;
	      var elements = this.props.elements.map(function (fe) {
	        return _react2.default.createElement(FormElement, _extends({ parent: _this4 }, _this4.props, fe));
	      });
	      return _react2.default.createElement(
	        'div',
	        { className: _style2.default.formOverlay },
	        _react2.default.createElement(
	          'form',
	          { className: _style2.default.formContainer, onSubmit: this.handleSubmit.bind(this) },
	          _react2.default.createElement(
	            'div',
	            { onClick: this.handleClose.bind(this), className: _style2.default.formClose },
	            _react2.default.createElement(
	              'svg',
	              { version: '1.1', width: '15', height: '15', xmlns: 'http://www.w3.org/2000/svg' },
	              _react2.default.createElement('line', { x1: '1', y1: '15', x2: '15', y2: '1', stroke: 'grey', 'stroke-width': '2' }),
	              _react2.default.createElement('line', { x1: '1', y1: '1', x2: '15', y2: '15', stroke: 'grey', 'stroke-width': '2' })
	            )
	          ),
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.formTitle },
	            this.props.title
	          ),
	          elements,
	          _react2.default.createElement(
	            'div',
	            { className: _style2.default.buttonLayer },
	            _react2.default.createElement('input', { className: _style2.default.formSubmit, type: 'submit', value: 'Submit' })
	          )
	        )
	      );
	    }
	  }]);
	
	  return Form;
	}(_react.Component);
	
	exports.default = Form;

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(96);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss", function() {
				var newContent = require("!!../../../../node_modules/css-loader/index.js?modules&importLoaders=1&localIdentName=botpress-platform-webchat__[name]__[local]___[hash:base64:5]!../../../../node_modules/sass-loader/index.js!./style.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(13)();
	// imports
	
	
	// module
	exports.push([module.id, ".botpress-platform-webchat__style__external___1BNU9 * {\n  box-sizing: border-box;\n  outline: none; }\n\n.botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K {\n  position: absolute;\n  bottom: 0;\n  right: 0;\n  width: 100%;\n  height: 100%;\n  border-left: 1px solid #ccc;\n  visibility: hidden;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K.botpress-platform-webchat__style__fullscreen___jVih9 {\n    visibility: visible; }\n  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n    -ms-flex-pack: justify;\n    justify-content: space-between;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    -ms-flex-negative: 0;\n    flex-shrink: 0;\n    height: 60px;\n    font-weight: 500;\n    border-bottom: 1px solid #ccc;\n    box-shadow: 0 1px 0.8rem rgba(0, 0, 0, 0.05);\n    padding: 0 1rem;\n    overflow: hidden; }\n    .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu {\n      overflow: hidden;\n      text-overflow: ellipsis;\n      -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n      flex: 1 1 auto; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ {\n        -webkit-box-align: center;\n        -ms-flex-align: center;\n        align-items: center;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__avatar___Fj3lF {\n          padding-left: 0;\n          overflow: hidden;\n          flex-shrink: 0;\n          width: 1.875rem;\n          height: 1.875rem; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__avatar___Fj3lF .botpress-platform-webchat__style__picture___1PvOc {\n            border-radius: 50%;\n            background-size: cover;\n            background-position: 50%;\n            width: 100%;\n            height: 100%; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__title___MJj5o {\n          padding-left: 10px;\n          overflow: hidden;\n          -webkit-box-orient: vertical;\n          -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n          flex-direction: column;\n          -webkit-box-pack: center;\n          -ms-flex-pack: center;\n          justify-content: center;\n          -webkit-box-align: start;\n          -ms-flex-align: start;\n          align-items: flex-start;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__title___MJj5o .botpress-platform-webchat__style__name___2gsld {\n            font-weight: 500;\n            overflow: hidden;\n            white-space: nowrap;\n            text-overflow: ellipsis; }\n            .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__title___MJj5o .botpress-platform-webchat__style__name___2gsld .botpress-platform-webchat__style__unread___1Ms8n {\n              display: inline-block;\n              width: 20px;\n              height: 20px;\n              margin-left: 5px;\n              text-align: center;\n              vertical-align: top;\n              border-radius: 50%;\n              line-height: 20px;\n              font-size: 12px;\n              color: #fff;\n              background-color: #ff5d5d; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__title___MJj5o .botpress-platform-webchat__style__status___J-Mqv {\n            overflow: hidden;\n            white-space: nowrap;\n            text-overflow: ellipsis;\n            font-size: .75rem;\n            font-weight: 500;\n            color: #9a9a9a;\n            vertical-align: middle;\n            margin-top: 0px; }\n            .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__title___MJj5o .botpress-platform-webchat__style__status___J-Mqv svg {\n              fill: #81d135;\n              height: 8px;\n              width: 8px;\n              margin-right: 3px; }\n    .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__icon___3-4NF {\n      color: #bbb;\n      margin-left: 22px;\n      text-align: right;\n      vertical-align: middle;\n      cursor: pointer; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__icon___3-4NF:hover {\n        color: #aaa; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__header___1urkm .botpress-platform-webchat__style__icon___3-4NF svg {\n        fill: currentColor; }\n  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj {\n    -webkit-box-flex: 1;\n    -ms-flex: 1;\n    flex: 1;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n    -ms-flex-direction: column;\n    flex-direction: column;\n    min-height: 1px; }\n    .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ {\n      border-top-width: 1px;\n      border-top-style: solid;\n      border-top-color: #eee;\n      position: relative;\n      padding: .5rem 0 0;\n      transition: border 0.3s ease; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn {\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n        flex-direction: column;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn textarea {\n          height: 43px;\n          width: 100%;\n          border: none;\n          font-size: 1rem;\n          resize: none;\n          line-height: 1.2;\n          padding: .75rem 1rem;\n          margin-bottom: .5rem;\n          background-color: transparent;\n          font: inherit; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          padding: 0 1rem .75rem;\n          -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n          justify-content: space-between;\n          -webkit-box-align: center;\n          -ms-flex-align: center;\n          align-items: center; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC {\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            list-style-type: none;\n            margin: 0;\n            padding: 0; }\n            .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC li {\n              margin-left: 0;\n              display: inline-block;\n              margin-right: 1rem; }\n              .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC li a {\n                color: #ccc; }\n                .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC li a:hover {\n                  color: #aaa; }\n                .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC li a i {\n                  -webkit-box-align: center;\n                  -ms-flex-align: center;\n                  align-items: center; }\n                  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__conversation___c85Kj .botpress-platform-webchat__style__bottom___1lsNX .botpress-platform-webchat__style__composer___IYeWQ .botpress-platform-webchat__style__flex-column___1e3hn .botpress-platform-webchat__style__line___26cWZ .botpress-platform-webchat__style__elements___vP2lC li a i svg {\n                    fill: currentColor; }\n  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU {\n    -webkit-box-flex: 1;\n    -ms-flex-positive: 1;\n    flex-grow: 1;\n    overflow-y: auto;\n    overflow-x: hidden; }\n    .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk {\n      height: 4.7rem;\n      border-bottom: 1px solid #eee;\n      cursor: pointer;\n      -webkit-box-align: center;\n      -ms-flex-align: center;\n      align-items: center;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk:hover {\n        background-color: #fafafa; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__left___3E1xu {\n        position: relative; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__avatar___Fj3lF {\n          margin: 0 .8rem;\n          width: 3.125rem;\n          height: 3.125rem;\n          -ms-flex-negative: 0;\n          flex-shrink: 0; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__left___3E1xu .botpress-platform-webchat__style__avatar___Fj3lF .botpress-platform-webchat__style__picture___1PvOc {\n            background-size: cover;\n            background-position: 50%;\n            border-radius: 50%;\n            width: 100%;\n            height: 100%; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__right___2li81 {\n        -webkit-box-flex: 1;\n        -ms-flex: 1;\n        flex: 1;\n        padding-right: 1rem;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n        flex-direction: column;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__right___2li81 .botpress-platform-webchat__style__title___MJj5o {\n          -webkit-box-align: center;\n          -ms-flex-align: center;\n          align-items: center;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          position: relative; }\n          .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__right___2li81 .botpress-platform-webchat__style__title___MJj5o .botpress-platform-webchat__style__date___HhsxQ {\n            text-align: right;\n            font-size: .875rem;\n            color: #9a9a9a;\n            position: absolute;\n            right: 0; }\n        .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__list___3AhoU .botpress-platform-webchat__style__item___3h7Uk .botpress-platform-webchat__style__right___2li81 .botpress-platform-webchat__style__text___38Uzh {\n          font-size: .875rem;\n          color: #9a9a9a;\n          max-width: 15rem;\n          overflow: hidden;\n          white-space: nowrap;\n          text-overflow: ellipsis; }\n  .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n    -ms-flex-direction: column;\n    flex-direction: column;\n    -webkit-box-align: stretch;\n    -ms-flex-align: stretch;\n    align-items: stretch;\n    overflow: hidden;\n    max-height: 230px; }\n    .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n      flex: 1 1 auto;\n      background: #f8f8f8;\n      box-shadow: inset 0 5px 9px -3px rgba(0, 0, 0, 0.3); }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y .emoji-mart {\n        width: 100% !important;\n        border: none;\n        border-radius: 0;\n        margin-top: 6px; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y .emoji-mart-anchors {\n        background-color: #f8f8f8; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y .emoji-mart-search input {\n        font-size: 14px; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y .emoji-mart-scroll {\n        height: 146px; }\n      .botpress-platform-webchat__style__external___1BNU9 .botpress-platform-webchat__style__internal___2QK5K .botpress-platform-webchat__style__emoji___1xld9 .botpress-platform-webchat__style__inside___2-82Y .emoji-mart-category-label {\n        font-size: 12px; }\n\n.botpress-platform-webchat__style__fadeIn___3ow2P {\n  animation-name: botpress-platform-webchat__style__slideInRight___3SrSu;\n  animation-duration: 0.3s;\n  animation-fill-mode: both;\n  animation-iteration-count: 1; }\n\n@keyframes botpress-platform-webchat__style__slideInRight___3SrSu {\n  0% {\n    visibility: visible;\n    transform: translateX(100%); }\n  100% {\n    transform: translateX(0);\n    visibility: visible; } }\n\n.botpress-platform-webchat__style__fadeOut___P1Cdp {\n  animation-name: botpress-platform-webchat__style__slideOutRight___3XL1L;\n  animation-duration: 0.3s;\n  animation-fill-mode: both;\n  animation-iteration-count: 1; }\n\n@-webkit-keyframes botpress-platform-webchat__style__slideOutRight___3XL1L {\n  0% {\n    transform: translateX(0);\n    visibility: visible; }\n  100% {\n    visibility: hidden;\n    transform: translateX(100%); } }\n\n@keyframes botpress-platform-webchat__style__slideOutRight___3XL1L {\n  0% {\n    transform: translateX(0);\n    visibility: visible; }\n  100% {\n    visibility: hidden;\n    transform: translateX(100%); } }\n", ""]);
	
	// exports
	exports.locals = {
		"external": "botpress-platform-webchat__style__external___1BNU9",
		"internal": "botpress-platform-webchat__style__internal___2QK5K",
		"fullscreen": "botpress-platform-webchat__style__fullscreen___jVih9",
		"header": "botpress-platform-webchat__style__header___1urkm",
		"left": "botpress-platform-webchat__style__left___3E1xu",
		"line": "botpress-platform-webchat__style__line___26cWZ",
		"avatar": "botpress-platform-webchat__style__avatar___Fj3lF",
		"picture": "botpress-platform-webchat__style__picture___1PvOc",
		"title": "botpress-platform-webchat__style__title___MJj5o",
		"name": "botpress-platform-webchat__style__name___2gsld",
		"unread": "botpress-platform-webchat__style__unread___1Ms8n",
		"status": "botpress-platform-webchat__style__status___J-Mqv",
		"icon": "botpress-platform-webchat__style__icon___3-4NF",
		"conversation": "botpress-platform-webchat__style__conversation___c85Kj",
		"bottom": "botpress-platform-webchat__style__bottom___1lsNX",
		"composer": "botpress-platform-webchat__style__composer___IYeWQ",
		"flex-column": "botpress-platform-webchat__style__flex-column___1e3hn",
		"elements": "botpress-platform-webchat__style__elements___vP2lC",
		"list": "botpress-platform-webchat__style__list___3AhoU",
		"item": "botpress-platform-webchat__style__item___3h7Uk",
		"right": "botpress-platform-webchat__style__right___2li81",
		"date": "botpress-platform-webchat__style__date___HhsxQ",
		"text": "botpress-platform-webchat__style__text___38Uzh",
		"emoji": "botpress-platform-webchat__style__emoji___1xld9",
		"inside": "botpress-platform-webchat__style__inside___2-82Y",
		"fadeIn": "botpress-platform-webchat__style__fadeIn___3ow2P",
		"slideInRight": "botpress-platform-webchat__style__slideInRight___3SrSu",
		"fadeOut": "botpress-platform-webchat__style__fadeOut___P1Cdp",
		"slideOutRight": "botpress-platform-webchat__style__slideOutRight___3XL1L"
	};

/***/ })
/******/ ]);
//# sourceMappingURL=embedded.bundle.js.map